#!/bin/bash 

cat > meta.list <<EOF
http://www.slac.stanford.edu/~timb/1000/aa_lowpt_production/aa_lowpt/E1000-B1b_ws.Paaddhad.Gwhizard-1.95.eW.pW.I39256
http://www.slac.stanford.edu/~timb/1000/aa_lowpt_production/aa_lowpt/E1000-B1b_ws.Paaddhad.Gwhizard-1.95.eW.pB.I39257
http://www.slac.stanford.edu/~timb/1000/aa_lowpt_production/aa_lowpt/E1000-B1b_ws.Paaddhad.Gwhizard-1.95.eB.pW.I39258
http://www.slac.stanford.edu/~timb/1000/aa_lowpt_production/aa_lowpt/E1000-B1b_ws.Paaddhad.Gwhizard-1.95.eB.pB.I39259
EOF

[ ! -e meta ] && mkdir meta

( cd meta
  cat ../meta.list | \
  while read url ; do 
    metaname=`basename $url`
    wget ${url}/${metaname}.txt
  done
)
