#!/bin/bash 

sedest=KEK-SRM
serep=DESY-SRM
srcdir=meta
dest=/ilc/prod/ilc/mc-dbd.log/generated/1000-B1b_ws/aa_lowpt

cmdfile=uprep.cli
rm -fv ${cmdfile}

for f in meta/*.txt ; do 
   metafile=`basename $f`
   diracpath=${dest}/${metafile}
   echo "dirac-dms-add-file ${diracpath} ${f} ${sedest}" >> ${cmdfile}
   echo "dirac-dms-replicate-lfn ${diracpath} ${serep}" >> ${cmdfile}
done

echo "Dirac command to upload and replicate meta file is created in ${cmdfile}"


