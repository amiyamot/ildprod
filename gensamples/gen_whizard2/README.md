# Tools to generate whizard2 samples for ILD production.

## Preparation of whizard2 scripts.
`mkgenscripts.py` create job scripts to generate whizard2 samples. Inputs to this script
are,

* `whizard2_template.sh` : A job script template to run whizard2.

* `sindarin/` : A directory containing whizard2 sindarin files for each process.

* `circe2/` : A directory for circe2 beam spectrum file. Place the circe2 files here.

* `param.json` : Parameters to create whizard2 job script. `mkgenscripts.py` reads parameters
for each processes to create jobs scripts from template file.

${...} string is substituted by the environment parameter, and %{...} string is substituted 
by the value defined in `oaram.json` by `mkgenscripts.py`. The parameter substitution 
aslo appilies to `whizard2_template.sh` and files in `sindarin/` file.

Job scripts are created in a directory `jobs`. Source `bsub.sh` in sub-directories to 
run whizard2 jobs.

## When whizard2 complated.

- Upload generated files to DIRAC by `source upload.sh`

- Set meta data to the upload file by `source setILCGen_Metadata.py --nodry <meta_file>`.
The meta file is created in the same directory.
