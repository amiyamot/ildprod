#!/bin/bash
# A script run Whizard jobs
# 

source %{INIT_WHIZARD2}

which whizard
which gfortran

which latex
which gcc

jobtop=`pwd`
sindir=%{SINDIR}
sindarin=%{SINDARIN}
aliases=%{ALIASES}
pythia6_parameters=%{PYTHIA6_PARAMETERS}
n_events=%{NumEvents}
circe2_file=%{CIRCE2_FILE}
sample_name=%{GenFilePrefix}
process_id=%{Process_ID}
whizard_process=%{WhizardProcess}
metafile=%{GenFilePrefix}.txt
sample_split_n_evt=%{Sample_Split_N_Evt}

# ####################################
# Whizard run of one process
# ####################################
sed -e "s|%%ALIASES%%|${aliases}|g" \
	-e "s|%%PYTHIA6_PARAMETERS%%|${pythia6_parameters}|g" \
        -e "s|%%CIRCE2_FILE%%|${circe2_file}|g" \
        -e "s|%%SAMPLE_NAME%%|${sample_name}|g" \
        -e "s|%%PROCESS_ID%%|${process_id}|g" \
        -e "s|%%PROCESS%%|${whizard_process}|g" \
        -e "s|%%N_EVENTS%%|${n_events}|g" \
        -e "s|%%SAMPLE_SPLIT_N_EVT%%|${sample_split_n_evt}|g" \
        ${sindir}/${sindarin} > ${sindarin}
      /usr/bin/time whizard -r ${sindarin} > run_whizard.log 2>&1  


# ###################################
# Collect information for the next step
# ###################################
libcv=`/lib64/libc.so.6 | head -1 | sed -e "s/ /\n/g" -e "s/,//g" | sed -n -e  "/version/,~1p" | tail -1 `
libc_version="glibc-${libcv}"
liblcio=`find ${LCIO_DIR} -type f -name "liblcio.so.*" -print`
lcio_version=`echo ${liblcio} | sed -e "s/liblcio.so.//g" -e "s/\./-/g" `
result_v1=`grep " Integral " ${whizard_process}.log | grep " = " | sed -e "s/  */ /g" | cut -d" " -f4`
result_v2=`grep " Error " ${whizard_process}.log | grep " = " | sed -e "s/  */ /g" | cut -d" " -f4`
cross_section=`echo "print float(${result_v1})" | python - `
cross_section_error=`echo "print float(${result_v2})" | python - `
number_of_files=`/bin/ls ${sample_name}.*.slcio | wc -l`
/bin/ls ${sample_name}.*.slcio > slcio.list
luminosity=`echo "print float(${n_events})/float(${result_v1})" | python - `

join_lines()
{
  inf=$1
  echo "lns=open(\"${inf}\").readlines() ; print \";\".join(lns)" | python - | tr -d "\n"
}
file_names=`join_lines slcio.list`

get_number_of_events()
{
   rm -f nevents.txt
   source /cvmfs/ilc.desy.de/sw/x86_64_gcc49_sl6/v02-00-02/init_ilcsoft.sh
   cat slcio.list | while read f ; do
     (  anajob $f | tail -4 | grep "events read from files:" | sed -e "s/  */ /g" | cut -d" " -f2 >> nevents.txt
     )
   done
   join_lines nevents.txt
}
number_of_events_in_files=`get_number_of_events`

# ####################################
# Post generation task.  Create metafile.
# ####################################
cat > ${metafile} <<EOF
process_id=%{Process_ID}
job_date_time=190418-003005-GMT-0900
process_names=%{ProcessName}
process_type=%{ProcessType}
CM_energy_in_GeV=%{Energy}
program_name_version=%{Generator}
pythia_version=6.422
stdhep_version=5.01
lcio_version=v${lcio_version}
OS_version_build=2.6.32-754.15.3.el6.x86_64
OS_version_run=2.6.32-754.15.3.el6.x86_64
libc_version=${libc_version}
fortran_version=gfortran-7.3.0
hadronisation_tune=OPAL
tau_decays=tauola
beam_particle1=e1
beam_particle2=E1
polarization1=%{ePol}
polarization2=%{pPol}
luminosity=${luminosity}
cross_section_in_fb=${cross_section}
cross_section_error_in_fb=${cross_section_error}
lumi_linker_number=18
machine_configuration=%{MachinePara}
file_type=lcio
total_number_of_events=%{NumEvents}
number_of_files=${number_of_files}
file_names=${file_names}
number_of_events_in_files=${number_of_events_in_files}
fileurl=%{DataDir}
logurl=http://www-jlc.kek.jp/~miyamoto/whizard2/
EOF

# #########################################
# Create scripts to upload to DIRAC
# #########################################

rm -f upload_files.list 
cat slcio.list | while read f ;  do
  echo "%{DataDir}/${f} ${f} %{DataSE}" >> upload_files.list 
done

echo "dirac-dms-add-file upload_files.list" > upload_files.sh 

