#!/usr/bin/env python 

'''
setILCGen_Metadata.py

Set meta data of ILC generator files. 
Generator meta data is obtained from a URL or a local file.
File meta data are set to the files defined by "file_names" field of the meta data

The script was originally written by Jan Strube and extended by Akiya Miyamoto 
to meet ILD needs, hoping it would be also useful for SiD

'''

from urllib2 import urlopen
import os.path as path
import sys
from pprint import pprint
import copy
import glob
import braceexpand

from DIRAC.Core.Base import Script
from DIRAC import gLogger, S_OK, S_ERROR

# #######################################
# 
# #######################################

class setILCGen_Metadata(object):

  def __init__(self):
    ''' initialization '''
    self.clip = self._Params()
    self.clip.registerSwitches()
    Script.parseCommandLine()

  class _Params(object):
    ''' parameter class '''
    def __init__(self):
      self.upload_path = ""
      self.metafile_path = ""
      self.dryrun = True
      self.do_prompt = True
      self.isList = False
      self.doGlob = False
      self.serialMin = 0L
      self.serialMax = 999999999L

    def setUploadPath(self, opt):
      self.upload_path = opt
      return S_OK()

    # def setMetafilePath(self, opt):
    #  self.metafile_path = opt
    #  return S_OK()

    def setDryRun(self, dummpy_opt):
      self.dryrun = True
      return S_OK()

    def setNoDryRun(self, dummy_opt):
      self.dryrun = False
      return S_OK()

    def setNoPrompt(self, dummy_opt):
      self.do_prompt = False
      return S_OK()

    def setIsList(self, dummy_opt):
      self.isList = True
      return S_OK()

    def setGlob(self, opt):
      self.doGlob = True
      return S_OK()

    def setSerialMin(self, opt):
      self.serialMin = int(opt)
      return S_OK()

    def setSerialMax(self, opt):
      self.serialMax = int(opt)
      return S_OK()

    def registerSwitches(self):
      Script.registerSwitch('','dry', 'Dry run (default), print meta to add only', self.setDryRun )
      Script.registerSwitch('','nodry', 'No dry run. Do add meta to files', self.setNoDryRun )
      Script.registerSwitch('S','no_prompt', 'Do not prompt before add meta. Valid only for nodry', self.setNoPrompt)
      Script.registerSwitch('L','list', 'input is a list of meta files.', self.setIsList )
      Script.registerSwitch('P','data_dir', 'GRID directory of generator files.', self.setUploadPath )
      Script.registerSwitch('G','glob', 'Interpret  "meta_file" as a glob string to search meta files.', self.setGlob )
      Script.registerSwitch('','min=', 'Minimum serial number to set seta data', self.setSerialMin )
      Script.registerSwitch('','max=', 'Maximum serial number to set seta data', self.setSerialMax )



      msg = '\n'.join([ '%s [options] meta_file ' % Script.scriptName , 
            'Function: add meta information of generator meta file(s)',
            '   meta information in the meta file is set to files specifield by "file_names" field.',
            '   "fileurl" field is data directory, if not "P" options is not given.',
            '   "meta_file" could be either a local file, web URL, or list of them if "L" '
            '    option is given.  ', 
            'Example: python setILCGen_Metadata.py https://web-utl/metafile.txt ', 
            '         python setILCGen_Metadata.py -P /ilc/prod/upload/dir /local/path/metafile.txt ',
            '         python setILCGen_Metadata.py -G "/local/path/*.txt" ',
            '         python setILCGen_Metadata.py -S --dry /local/path/metafile.txt # check meta file.'   ])
      
      Script.setUsageMessage(msg)

  # =========================================================================
  def load_meta_information(self, afile):
    ''' Load meta information from a metafile '''
    # generator_key to dirac directory key conversion map
    LogDirConvention = {
      "PythiaVer": "pythia_version",
      "Luminosity": "luminosity",
      "BeamParticle1": "beam_particle1",
      "BeamParticle2": "beam_particle2",
      "PolarizationB1": "polarization1",
      "PolarizationB2": "polarization2",
      "CrossSection": "cross_section_in_fb",
      "CrossSectionError": "cross_section_error_in_fb",
      "GenProcessID": "process_id",
      "GenProcessName": "process_names",
      "TauDecaysLib": "tau_decays",
      "ProgramNameVersion": "program_name_version",
      "HadTune": "hadronisation_tune"
      }
    OptConvention = ["TauDecaysLib", "HadTune"]
    # generator_key to dirac file key conversion map
    LogFileConvention = {
      "NumberOfEvents": "number_of_events_in_files",
      "FileNames": "file_names"
    }

    ret = {}
    ret["dir"]=copy.deepcopy(LogDirConvention)
    ret["file"]=copy.deepcopy(LogFileConvention)

    isLocal = True
    if "https://" in afile or "http://" in afile:
      isLocal = False
      fmetafile = urlopen(afile)
    else:
      fmetafile = open(afile)

    for line in fmetafile:
        items = line.strip().split('=')

        if items[0] == "fileurl":
            ret["FileURL"] = items[1].replace("lfn:/grid/ilc/","/ilc/").replace("lfn:/ilc/", "/ilc/")
        for c in LogDirConvention:
            if items[0] == LogDirConvention[c]:
                ret["dir"][c] = items[1]
                break
        
        if items[0] == "file_names":
           if "{" in items[1] and ".." in items[1] and "}" in items[1]:
              ret["file"]["FileNames"] = list(braceexpand.braceexpand(items[1]))
           else:
              ret["file"]["FileNames"] = items[1].split(";")
        elif items[0] == "number_of_events_in_files":
           if "*" in items[1]:
              (nevts, nfiles) = items[1].split("*")
              ret["file"]["NumberOfEvents"] = [int(nevts)]*int(nfiles)
           else:
              ret["file"]["NumberOfEvents"] = items[1].split(";")

    if self.clip.upload_path != "":
      ret["FileURL"] = self.clip.upload_path

    fmetafile.close()

    # Print error message, if any meta values are unset.
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BLUE = '\033[94m'
    ret['OK'] = True
    for c in LogDirConvention:
      if ret["dir"][c] == LogDirConvention[c]:
        if c in OptConvention:
           print( BLUE + " Warning " + ENDC +
                  "optional metakey " + BLUE + c + ENDC + " is missing." )
           ret["dir"][c] = ""
        else:
           print( FAIL + ' ERROR ' + ENDC +
                  'meta key ' + FAIL + LogDirConvention[c] + ENDC + ' is not found ' )
           ret['OK'] = False      

    return ret 

  # ==================================================================
  def upload_meta(self, afile):

    from DIRAC.Resources.Catalog.FileCatalogClient import FileCatalogClient

    meta = self.load_meta_information(afile)
    if not meta['OK']:
      print( 'Terminated due to error to load meta data.' )
      exit(2)

    fileMeta = {}

    if not self.clip.dryrun:
      fc = FileCatalogClient()

    for idx, f in enumerate(meta["file"]["FileNames"]):
      if idx < self.clip.serialMin or idx > self.clip.serialMax:
         continue 

      fileMeta["NumberOfEvents"] = meta["file"]["NumberOfEvents"][idx]
      for c, val in meta["dir"].items():
        if val != "":
           fileMeta[c] = val

      lfn=path.join(meta["FileURL"], f)
      print( "### Setting metadata for file %s" % lfn )
      pprint(fileMeta)

      if self.clip.do_prompt:  
        if raw_input("proceed (y/N)").upper() != 'Y':
          continue

      if not self.clip.dryrun:
        res = fc.setMetadata(lfn, fileMeta)
        if not res['OK']:
          gLogger.error( "Failed to set meta data %s to %s\n" %(lfn, fileMeta), res['Message'] )
          exit(1)


# ====================================================================
if __name__ == "__main__":

  metaset = setILCGen_Metadata()
  args = Script.getPositionalArgs()
  if len( args ) < 1 :
    Script.showHelp()
    exit(0)

  metafile = args[0]
  
  if metaset.clip.doGlob:
    for f in glob.glob(metafile):
      print( "### Reading a meta information from %s. " % f )
      metaset.upload_meta(f)      
  
  elif metaset.clip.isList :
    for f in open(metafile):
      print( "### Reading a meta information from %s. " % f )
      metaset.upload_meta(f.replace("\n",""))
  
  else:
    metaset.upload_meta(metafile)
  
