#!/usr/bin/env python

import os
import openpyxl
import argparse
import pprint


# =====================================================
if __name__ == "__main__":

    # Create excel file.
    parser = argparse.ArgumentParser(description="Print contents of excel file")
    parser.add_argument("excel_file", help="excel file")
    parser.add_argument("-s", "--sep", help="Separator for printout", dest="sep", action="store", default="\t")
    parser.add_argument("-o", "--only", help="Sheet name to be printed. all for all sheet data", dest="sheet", 
                       action="store", default="all")
    parser.add_argument("-n",help="Print sheet names only.", dest="names", action="store_true", default=False)
    parser.add_argument("-d", "--data_only", help="Data only mode, if True", dest="dataonly",
                       action="store_false", default=True)


    args = parser.parse_args()
    excel_file = args.excel_file
    sep = args.sep
    sheet = args.sheet
    names = args.names
    dataonly = args.dataonly
  
    wb = openpyxl.load_workbook(excel_file, data_only=dataonly)

    if names:
      #  print("Sheet names | ")
      print(sep.join(wb.sheetnames))
   
    else:
      for wsn in wb.sheetnames:
         if sheet == "all" or sheet == wsn:
            print("### worksheet "+wsn)
            ws = wb[wsn]
            values=[[cell.value if isinstance(cell.value,str) else str(cell.value) for cell in row] for row in ws]
            for y, row in enumerate(values):
                print(str(y)+" | "+sep.join(row))

