#i!/bin/bash 
#
# Create production directory using build_proddir.py scripts

prodname="rec_eennNg"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then
  mkdir -v ${proddir} || my_abort "Failed to create directory"
fi


origdirtop=/group/ilc/users/miyamoto/ildprod/v02-00-01/2f_ng-2
tdirs=`( cd ${origdirtop} && /bin/ls -d 2f_Z_* )`

for odir in ${tdirs} ; do 
  origdir=${origdirtop}/${odir}
  outdir=${proddir}/${odir}
  mkdir -vp ${outdir}
  build_prodrec.py -s s5 -o o1 -p ovl --outdir ${outdir} ${origdir}
  # build_prodrec.py -s l5:s5 -o o1 -p nobg --no_elog --outdir ${outdir} ${origdir}
done


