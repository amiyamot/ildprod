#!/bin/bash
#
# A template of init_production script, followed by detail help 
# of init_production.py arguments 
#
#############################################################################
#
#
do_one_set()
{
    prodname=$1
    procid=$2
    norder=$3
    # ${sort_key}:_${procid}_${norder}/${procid}
    split_nseq_from=$4
    split_nseq_to=$5
    prodopt=" --dstonly --recrate -1 --delfiles sim "
    if [ ${norder} -eq 0 ] ; then 
      prodopt=" --dstonly --recrate 0.1 "
    elif [ ${norder} -lt 0 ] ; then 
      echo "norder ( 2nd argument ) should be ge 0"
      exit -1
    fi
    
    ngenfile_max=$[${split_nseq_to}-${split_nseq_from}+1]
    xml_serial=${norder}

    echo "####### do_one_set ##########"
    echo "  prodname=${prodname}, procid=${procid}, norder=${norder}, nseq_from=${seplit_nseq_from}, nseq_to=${split_nseq_to}" 
    echo "  prodopt=${prodopt}, ngenfile_max=${ngenfile_max}, xml_serial=${xml_serial}"
    echo "#############################"
    
    #
    # A set of directories are created under ${proddir}/<sort_key>:<subdir>
    #
    # prodname=250-eehiq
    proddir=${PRODTASKDIR}/${prodname}
    
    if [ ! -e ${proddir} ] ; then
      mkdir -v ${proddir} || my_abort "Failed to create directory"
    fi
     
    #############################################################
    # Plan of eehiq production
    # First set : 10% of all samples. 
    #    Keep SIM&DST, REC 10%
    #    init_production argument:  --dstonly --recrate 0.10
    # Rest 
    #    Not save sim, rec, keep DST only 
    #    init_production argument:  --dstonly --recrate -1 --delfiles sim
    #############################################################
    
    
    
    ###########################################################
    # Format of xxx-list.txt file.
    # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
    # More than one line can be specified to create multiple-directories in ${proddir}.
    # Note that each line corresponding to the one set of production, one elog entry.
    #############################################################
    cat > ${prodname}-list.txt <<EOF
2f_Z_eehiq.bWW:_${procid}_${norder}/${procid}
EOF
    # 2f_Z_eehiq.bWW:I500003_1/I500003
    
    banned_sites=`joinlines banned_sites.txt`
    # banned_sites.txt is a list of banned sites name
    excel_file=prodpara/250-SetA-2f_eehiq.xlsx
    
cmd="init_production.py --workdir ${proddir} \
 --prodlist ${prodname}-list.txt \
 --excel_file ${excel_file} \
 --sim_nbtasks 1000  --rec_nbtasks 1000 \
 --gensplit_numprocs 8 \
 --ngenfile_max ${ngenfile_max} \
 --split_nseq_from ${split_nseq_from} \
 --mcprod_config mcprod_config-ilcsoft-v02-02.py \
 --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
 --xml_serial ${xml_serial} \
 ${prodopt} \
 --production_type sim:ovl  \
  
"
    
    echo ${cmd}
    
    echo " "
    ${cmd} \
      || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
      && mv -v ${prodname}-list.txt ${proddir}
}

###############################################################################
# do_one_set 250-eehiq_eR_pR I500003 0 0 6 
do_many_set(){
    # imax=67
    # nf=7
    # poltype="eR_pR"
    # procid=I500003
    poltype=$1
    procid=$2
    imax=$3
    nf_per_trans=$4
    loop_from=$5
    
    nloop=$[${imax}/${nf_per_trans}]
    for iorder in `seq ${loop_from} ${nloop}` ; do
       ifrom=$[${nf_per_trans}*${iorder}]
       ito=$[${ifrom}+${nf_per_trans}-1]
       [ ${ito} -gt ${imax} ] && ito=${imax}

       echo "####ifrom=$ifrom ito=$ito nf_per_trans=$nf_per_trans iorder=$iorder"
       do_one_set 250-eehiq_${poltype} ${procid} ${iorder} ${ifrom} ${ito} 
    done
}

###############################################################################
# Maximum nb of jobs per transformation would around 25000
# proc. PID    #Files #Jobs #Trans #F/trans. #Job/Trans.
# eL_pL I500001   67  67000  10      7        6700
# eL_pR I500002  322 322000  12     27       26833
# eR_pR I500003   67  67000  10      7        6700
# eR_pL I500004  318 318000  12     27       26500

####################################################################
# Arguments for do_one_set
# 2f_Z_eehiq.bWW:I500003_1/I500003
# prodname=$1
# procid=$2
# norder=$3
# split_nseq_from=$4
# ngenfile_max=$5

do_one_set 250-eehiq_eL_pL I500001 0 0 6 
#do_one_set 250-eehiq_eL_pR I500002 0 0 26

# do_one_set 250-eehiq_eR_pR I500003 0 0 6 
# do_one_set 250-eehiq_eR_pL I500004 0 0 26

####################################################################
# Arguments for do_many_set
#    poltype=$1
#    procid=$2
#    imax=$3  # Last serial number of generated file
#    nf_per_trans=$4  # of file per transformation
#    loop_from=$5
# do_many_set eL_pL I500001 66 7 1
# do_many_set eL_pR I500002 321 27 1

# do_many_set eR_pR I500003 66 7 1
# do_many_set eR_pL I500004 317 27 1


