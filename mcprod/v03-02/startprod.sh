#!/bin/bash 


source ${SCRIPTS_DIR}/startprod_lib.sh

# ################################################################
# Main part
# ################################################################

max_job_insys=2000
max_sim_trans=3
sleep_before_next_loop=30m

dirlist="startprod.list"
waitmin="10m"
myhost=`hostname`
dstr=`date +%Y%m%d-%H%M%S`
parent_dir=`pwd`

inargs=$1

if [ "x${inargs}" == "x-h" ] ; then 
  echo "./startprod.sh [opt] "
  echo "initiates a sequence of production defined in startprod.list."
  echo "Options"
  echo "  (no argument) : Prompt before start sequence."
  echo "  -h   : print this help"
  echo " "
  exit
fi



if [ `cat ${dirlist} | wc -l` -eq 0 ]; then
  echo "**** Need to edit ${dirlist} to initiate production"
  echo "Exit"
  exit
elif [ "x${inargs}" == "x" ] ; then 
  echo "**** Do you realy start following production?"
  list=`cat ${dirlist}`
  cat ${dirlist}
  echo -n "Continue with these production [y/n]: "
  read ANS

  case $ANS in
    [Yy]* ) echo "**** Start production"; echo ${list} ;;
    * ) echo "**** Stopped" ; exit ;;
  esac
fi

if [ -e startprod.active ] ; then 
  myhost=`hostname`
  stdecho "At ${myhost} : startprod.sh is executed as startprod.active exist. " 
  cat startprod.active 
  stdecho "Remove startprod.active before starting startprod.sh, if you know no other startprod.sh is running."
  exit
fi

stdecho "startprod.sh is active at `hostname` with PID=$$" > startprod.active
stdecho "checking proxy and waiting next be ready " >> startprod.active

check_proxy

wait_next_be_ready
if [ $? != 0 ] ; then 
  stdecho "Failed to check wait_next_be_ready for the first entry in startprod.list."
  stdecho "startprod.sh is terminated."
  exit -1
fi

while [ `cat ${dirlist} | wc -l` -ne 0 ] ; do 
  headline=`head -1 ${dirlist}`
  tail -n +2 ${dirlist} > temp-${dirlist}
  mv temp-${dirlist} ${dirlist}
  stdecho "Start ${headline} production at ${myhost} +++" "+++++"
  topdir=`dirname ${headline}`
  wdir=`basename ${headline}`
  dstr=`date +%Y%m%d-%H%M%S`
  echo ${headline} >> ${parent_dir}/startprod.done
  stdecho "startprod.sh is active at `hostname` with PID=$$" > startprod.active
  stdecho "current directory is ${headline} " >> startprod.active
  ( 
    cd ${topdir}
    stdecho "chdir to ${topdir} +++"
    . ${MYPROD_PRODPROD} 
    do_onedir ${wdir} ${waitmin} ${parent_dir}
  ) 
  stdecho "returned from do_onedir. Now at `pwd` +++" "+++++"

  if [ -e startprod.exit ] ; then
      stdecho "quit the loop of startprod.list by startprod.exit"
      break
  fi 
  while [ -e sleep.cmd ] ; do
      stdecho "found sleep.cmd and will be executed"
      . sleep.cmd
  done
  stdecho "startprod.sh is active at `hostname` with PID=$$" > startprod.active
  stdecho "waiting next be ready " >> startprod.active

  stdecho "Checking readiness of next production +++"
  wait_next_be_ready 
  if [ $? != 0 ] ; then 
    stdecho "Failed while waiting next production be ready +++"
    break
  fi

  if [ -e startprod.exit ] ; then
      stdecho "quit the loop of startprod.list by startprod.exit"
      break
  fi 
  stdecho "Completed wait_next_be_ready. Moving to next production in the queue +++"

done

rm -f startprod.active
stdecho "startprod.active was removed."
stdecho "No more entry in ${dirlist} or quit loop requested."
if [ -e post_exec.sh ] ; then 
  post_exec.sh > post_exec.log 2>&1 &
fi 

stdecho "startprod.sh reached the end of the script."
