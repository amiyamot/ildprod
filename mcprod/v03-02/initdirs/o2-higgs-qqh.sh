#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="o2-higgs"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
#testname="250-SetA-lowX"
cat > ${prodname}-list.txt <<EOF
higgs_ffh.bWW:_qqh/I402011,I402012
EOF

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk"
excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-higgs-B.xlsx"

export GENSPLIT_DEFAULT_NPROCS=4

# --noelog

cmd="init_production.py --workdir ${proddir} \
  --excel_file  ${excel_file} \
  --prodlist ${prodname}-list.txt \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --dstonly --recrate 0.10 \
  --sim_models ILD_l5_v02 --rec_options o2 \
  --se_for_sim KEK-SRM \
  --split_nseq_from 0 \
  --mcprod_config "mcprod-ilcsoft-v02-02-01.py" \
  --sim_nbtasks 100 --rec_nbtasks 100 \
  --production_type sim:ovl "

#
echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

