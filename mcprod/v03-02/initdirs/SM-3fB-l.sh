#!/bin/bash 
#
#  Create scripts for background files.
#

# ###########################################################
# Create production scripts
# Usage:
#   create_scripts [norder] [nstep] [proddir]
# Argeuments:
#   [norder] : Production sequence number
#   [nstep]  : Number of input files processed per production
#   [proddir] : Directories of production
#   [simse] : SE for SIM data output
# ###########################################################
create_scripts(){
  nseq_start=16
  norder=$1
  nstep=$2
  proddir_now=$3
  simse_now=$4
  nseq_from=$[(${norder}-2)*${nstep}+${nseq_start}]

  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
# 3f.bWB:_I500014_${norder}/I500014
# 3f.bWB:_I500016_${norder}/I500016
cat > ${prodname}-list.txt <<EOF
3f.bBW:_I500034_${norder}/I500034
3f.bBW:_I500036_${norder}/I500036
EOF
# I500034,I500036 86 files for 1000 fb^-1. 

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-3f.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir_now} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${nstep} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir_now}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir_now}
  
}


prodname="SM-3fB"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

nb_file2prod=35
for i in `seq 2 3` ; do
  simse="DESY-SRM"
  [ $[${i}/2*2] -eq ${i} ] || simse="KEK-DISK"
  create_scripts ${i} ${nb_file2prod} ${proddir} ${simse}
done

