#!/bin/bash 
#
#  Create scripts for background files.
#


# ###########################################################
# Create production scripts
# Usage:
#   create_scripts [norder] [nstep] [proddir]
# Argeuments:
#   [norder] : Production sequence number
#   [nstep]  : Number of input files processed per production
#   [proddir] : Directories of production
#   [simse] : SE for SIM data output
#   [last_seq_number] : Last sequence number
# ###########################################################
create_scripts(){
  norder=$1
  nstep=$2
  proddir_now=$3
  simse_now=$4
  nseq_from=$[(${norder}-1)*${nstep}]
  if [ "x${5}" != "x" ] ; then 
    last_seq=$[${nseq_from}+${nstep}-1]
    if [ $last_seq -gt ${5} ] ; then
       nstep=$[${5}-${nseq_from}+1]
    fi
  fi

  nsplit_nseq_from=0
  ngenfile_max=10000 # Large number to process all.

  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
6f.bWW
EOF
  # I500094: Left over files. 

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-6f.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk,OSG.UConn.us"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir_now} \
    --excel_file ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate 0.10 \
    --ngenfile_max ${ngenfile_max} \
    --split_nseq_from ${nsplit_nseq_from} \
    --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
#    --test --se_for_data KEK-DISK --se_for_gensplit KEK-DISK --se_for_logfiles KEK-DISK \
#    --nw_perfile 10 --split_nbfiles 10 \
#

  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}
}


########################################################################
prodname="SM-6f"
proddir=${PRODTASKDIR}/${prodname}
if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

simse="DESY-SRM"
#for i in `seq 11 50` ; do 
i=1
nstep=10000   # neglected in sub-script
last_seq_number=1000 # neglected in sub-script
create_scripts ${i} ${nstep} ${proddir} ${simse} ${last_seq_number}
#done

# ###########################################################
# Create production scripts
# Usage:
#   create_scripts [norder] [nstep] [proddir]
# Argeuments:
#   [norder] : Production sequence number
#   [nstep]  : Number of input files processed per production
#   [proddir] : Directories of production
#   [simse] : SE for SIM data output
#   [last_seq_number] : Last sequence number
# ###########################################################
