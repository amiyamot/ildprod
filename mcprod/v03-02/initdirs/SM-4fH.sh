#!/bin/bash 
#
#  Create scripts for background files.
#
# ###########################################################
# Create production scripts
# Usage:
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 
# ###########################################################
create_scripts(){
  procname=$1
  processID=$2
  norder=$3
  nseq_from=$4
  ngenfile=$5
  simse_now=$6

  prodname="SM-4fH"
  proddir=${PRODTASKDIR}/${prodname}

  if [ ! -e ${proddir} ] ; then 
    mkdir -v ${proddir} || my_abort "Failed to create directory" 
  fi
  # ###########################################################
  # Format of xxx-list.txt file.
  # <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
  #############################################################
cat > ${prodname}-list.txt <<EOF
${procname}:_I${processID}_${norder}/I${processID}
EOF

  excel_file="/home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-SM-midhighX.xlsx"
  banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk,LCG.LAPP.fr,LCG.QMUL.uk,LCG-UKI-LT2-IC-HEP.uk,LCG.UKI-SOUTHGRID-RALPP.uk"
  export GENSPLIT_DEFAULT_NPROCS=8
  options=" --nodry --noPrompt -N ${norder} "
  
  cmd="init_production.py --workdir ${proddir} \
    --excel_file  ${excel_file} \
    --prodlist ${prodname}-list.txt \
    --dstonly --recrate -1 \
    --split_nseq_from ${nseq_from} --ngenfile_max ${ngenfile} \
    --delfiles sim --se_for_sim ${simse_now} \
    --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
    --sim_nbtasks 1000 --rec_nbtasks 1000 \
    --production_type sim:ovl \
    --step4_options ${options} "
  
  echo ${cmd}
  
  echo " "
  ${cmd} \
    || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
    && mv -v ${prodname}-list.txt ${proddir}
  
}

###########################################################
# main part of this script
###########################################################
#  4f Large cross section processes.
#
#   create_scripts <procname> <processID> <norder> <nseq_from> <ngenfile> <simse> 

#
# #######################################################################################
# 4f_WW_hadronic.bWW
### 4f_ww_h     eL.pR I500066   504fb-1(0-74 files, 37500 jobs) ==> 5000 fb-1(0-743 total)
nseq_from0=300
num_files=50
for i in `seq 5 13` ; do
   simse="DESY-SRM"
   # [ $[${i}/2*2] -eq ${i} ] || simse="KEK-DISK"
   n0=$[${nseq_from0}+(${i}-5)*${num_files}]
   num=${num_files}
   [ $[${n0}+${num_files}-1] -gt 743 ] && num=$[743-${n0}+1]
   create_scripts 4f_WW_hadronic.bWW 500066 ${i} ${n0} ${num} ${simse}
done
#
#
