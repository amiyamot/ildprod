#!/bin/bash 
#
#  Create scripts for background files.
#

prodname="SM-aa_2fB"
proddir=${PRODTASKDIR}/${prodname}

if [ ! -e ${proddir} ] ; then 
  mkdir -v ${proddir} || my_abort "Failed to create directory" 
fi

banned_sites="LCG.Bristol.uk,LCG.Cracow.pl,LCG.JINR-LCG2.ru,LCG.RAL-LCG2.uk"

# ###########################################################
# Format of xxx-list.txt file.
# <sort_key>:<subdir>/<procID1>,<procID2>,<procID3>,,,
#############################################################
cat > ${prodname}-list.txt <<EOF
aa_2f_Z_leptonic.bBB
EOF

export GENSPLIT_DEFAULT_NPROCS=7
#
# aa_2f_leptonic 500053 : 360 files(72/set)
# 3.01 fb-1/file, 332.3 files for 1000 fb-1, 0:75 files done.
# 76:333 files next. 86 files/set. 140jobs/file. Total 12040 jobs.

# --noelog

cmd="init_production.py --workdir ${proddir} \
  --excel_file  /home/ilc/miyamoto/ILDProd/mcprod/v03-02/initdirs/prodpara/250-SetA-aa_2f_leptonic.xlsx \
  --prodlist ${prodname}-list.txt \
  --dstonly --recrate -1 \
  --split_nseq_from 76 --ngenfile_max 258 \
  --delfiles sim --se_for_sim DESY-SRM \
  --sim_banned_sites ${banned_sites} --rec_banned_sites ${banned_sites} \
  --sim_nbtasks 1000 --rec_nbtasks 1000 \
  --production_type sim:ovl "

#
echo ${cmd}

echo " "
${cmd} \
  || ( echo "Failed to produce scripts in ${proddir}" && exit 1 ) \
  && mv -v ${prodname}-list.txt ${proddir}

