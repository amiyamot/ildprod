#!/bin/bash 


echo "#Filename Nfile Nevents Nevorig" | tee evsum.txt
cat gp-split.*.log | grep -e output -e Status | tr -d '\n' | sed -e "s/\}/\n/g" | cut -d" " -f1,13,15,17 | \
  cut -d"/" -f2 | sort | tee -a evsum.txt

#  sed -e "s|out/||" | sort | tee -a evsum.txt

