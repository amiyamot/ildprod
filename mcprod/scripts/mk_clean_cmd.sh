#!/bin/bash 

eid=$1
types=$2
if [ "x${eid}" == "x" ] ; then 
  echo "Files to create removal request is created."
  echo "Usage:"
  echo "  ./mk_clean_cmd.sh [elog_id] [types]"
  echo "  [types] is a comma separated list of gensplit,SIM,DST"
  exit 0
fi

typename=`echo ${types} | sed -e "s/,/_/g"`
rmcmd="${eid}-rm-${typename}.cmd"
crlog="${eid}-rmreq-${typename}.log"

# touch ${rmcmd}
# dtype={"dst":"DST", "gensplit":"gensplit", "sim":"SIM" }




get_elog_prodid.py ${eid} | sed -e "s/,/\n/g" | while IFS="=" read itp pid ; do
  # echo "itp=${itp}"
  if [ `echo ${types} | grep -c ${itp}` -ne 0 ] ; then 
    dtype="not_allowed" 
    if [ "x${itp}" == "dst" ] ; then 
      dtype="DST"
    elif [ "x${itp}" == "sim" ] ; then 
      dtype="SIM"
    elif [ "x${itp}" == "gensplit" ] ; then 
      dtype="gensplit"
    else 
      "In mk_clean_cmd.sh : Datatype=${itp} is not allowed to use this command."
    fi
    if [ "${dtype}" != "not_allowed" ] ; then 
      rmlist="${eid}-${itp}-${pid}-remove.list"
      echo "dirac-dms-find-lfns Path=/ilc/prod/ilc/mc-2020/ild/${itp} ProdID=${pid} Datatype=${dtype} | grep "/${itp}/" > ${rmlist} " >> ${rmcmd}
      echo "( [ -s ${rmlist} ] && dirac-dms-create-removal-request All ${rmlist} ) | tee ${crlog} " >> ${rmcmd}
    fi
  fi
done
echo "source ${rmcmd} to create removal request"

