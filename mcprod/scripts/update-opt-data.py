#!/bin/env python 
#
# update elog entry so as to put a link to genmeta record
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob
import ssl
from urllib2 import urlopen
import subprocess
import shutil

import ExcelTools
import ILDLockFile
from openpyxl import Workbook
from openpyxl import load_workbook


_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "opt-data"


# ######################################################
def open_logbook():
    ''' open elog logbook '''
    global _logbook, _elog_url, _elog_subdir, _do_test
   
    if _logbook == None:
        lines = open(os.environ["MY_ELOG_KEY"]).readlines()
        ( auser, apass ) = lines[0][:-1].split(' ')

        _logbook = elog.open("%s/%s/" % (_elog_url, _elog_subdir), user=auser, password=apass)

    return 

# ######################################################
def write_optdata(optdata_json, rfrom, rto):
    ''' Write data in optdata_json file to Elog opt-data database '''

    optdata = json.load(open(optdata_json))

    outinfo = {}
    ncount = 0
    for k, v in sorted(optdata.items()):
       ncount += 1
       if ncount < rfrom or ncount > rto:
          continue
       
       rec = {} 
       rec["Status"] = "OK"
       rec["GenProcessIDNum"] = long(v["ProcID"])
       rec["Ecm"] = str(int(v["ecm"]))
       rec["Machine"] = v["machine"]
       rec["EvtType"] = v["EvtType"]
       rec["ProcName"] = v["ProcName"]
       rec["Pol"] = v["BeamPol"]
       rec["Datatype"] = v["Datatype"]
       rec["ILDConfig"] = v["ILDConfig"]
       rec["Model"] = v["DetectorModel"]
       rec["ProdID"] = long(v["RecID"])
       rec["NEvents"] = long(v["NEvents"])
       xsect = float(v["xsect"])
       intlumi = float(v["intlumi"])
       rec["Xsect"] = "%-12.5g" % xsect
       rec["IntLumi"] = "%-12.3e" % intlumi if intlumi > 10000.0 else "%-10.1f" % intlumi
       rec["ElogIDNum"] = v["elogid"]
       rec["NFiles"] = int(v["ndstm"])
       filelist = v["dstm-info"]
       rec["ElogID"] = "<a href=\"%s/dbd-prod/%s\">%s</a>" % ( _elog_url, v["elogid"], v["elogid"] ) 
       rec["GenProcessID"] = v["ProcID"]

       uploadfiles = [ str(v["dstm-info"]) ]
       # print uploadfiles
       msgtext = ""
       msg_id = _logbook.post(msgtext, attributes = rec, attachments = uploadfiles, encoding = "HTML" )
       
       del rec["unm"]
       del rec["upwd"]

       rec["elogid-opt-prod"] = msg_id

       outinfo[msg_id] = rec

       print( str(ncount) + " th record " + k + " is written to elog id " + str(msg_id) )

    return outinfo

# #########################################################
def update_entry(elogid):
    ''' Update an elog entry.  Put a link to genmeta. '''

    (message, attributes, attachements ) = _logbook.read(elogid)
    genprocid = attributes["GenProcessID"]
    weblink = "<a href=\"https://ild.ngt.ndu.ac.jp/elog/genmeta/?process_id=%s\">%s</a>" % (genprocid, genprocid) 
    attributes["GenProcessID"] = weblink
    
    msg_id = _logbook.post(message, attributes = attributes, msg_id=elogid, encoding = "HTML" )
    print( "msg_id was updated.  msg_id=" + str(msg_id) )


# =========================================================
if __name__ == "__main__":

    open_logbook()

    msgids = _logbook.get_message_ids()
    
    id_from = 2
    id_to = 1510
    for eid in range(id_from, id_to):
        if eid in msgids:
            update_entry(eid)
   
    # out = write_optdata("elogdbs.json", 1, 3)
    # out = write_optdata("elogdbs.json", 1, 1000)

    # json.dump(out, open("write-opt-prod.json", "w"))

   
