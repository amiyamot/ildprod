#!/bin/bash
#
# A script for step5 tasks
#

stfile="status.log"

echo "### dst2kek has started" 2>&1 | tee ${stfile}
( LANG=C && date && hostname ) 2>&1 | tee -a ${stfile}

./dst2kek.py
chmod +x ./dst2kek.sh
if [ "xWEBTEST" == "xNo" ] ; then
  ./dst2kek.sh 2>&1 | tee -a ${stfile} 
fi

date 2>&1 | tee -a ${stfile}

echo "### dst2kek completed" 2>&1 | tee -a ${stfile}

