#!/bin/bash

prodid=$1

echo "Reset files of production ${prodid}"

echo getFiles ${prodid} | dirac-transformation-cli > getfiles-${prodid}.log

nfile=`grep -v Processed getfiles-${prodid}.log | grep ilc | cut -d" " -f1 | wc -l`

echo "${nfile} files will be reset for production ${prodid}"
echo -n "Continue ? "
read ans

grep -v Processed getfiles-${prodid}.log | grep ilc | cut -d" " -f1 | \
  while read f 
  do 
    ( echo resetFile ${prodid} $f )  
  done > resetcmd-${prodid}.cli

cat resetcmd-${prodid}.cli | dirac-transformation-cli

