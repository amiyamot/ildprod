#!/bin/bash
#
# A script for step3 tasks
#

export LANG=C
stfile="status.log"
dstr=`date +%Y%m%d-%H%M%S`
hostname=`hostname`

if [ ! -e ${stfile} ] ; then 
   echo "### gensplit and upload started at ${hostname} with PID=$$, on ${dstr} " 2>&1 | tee -a ${stfile}
else
   echo "### gensplit and upload RE-STARTED at ${hostname} with PID=$$, on ${dstr} " 2>&1 | tee -a ${stfile}
   echo "${stfile} exists." 2>&1 | tee -a ${stfile}
fi
( LANG=C && date && hostname ) 2>&1 | tee -a ${stfile}

### Check noelog flag
noelog=`jsonread production.json __ELOG_NOELOG 2>/dev/null`
if [ "x${noelog}" == "xTRUE" ] ; then
  echo "NOELOG specified. ELOG data for task_step3.sh is not created " | tee -a ${stfile}
fi

### Update ELOG
if [ "x${noelog}" != "xTRUE" ] ; then 
  putelog.py gensplit 2>&1 | tee -a ${stfile}
fi

gensplit_id=`jsonread production.json __PRODID_FOR_GENSPLIT`
gensplit_active="${MCPROD_TOP}/gensplit.active.${gensplit_id}"
echo "+++ ${dstr} : task_step3.sh started : ${hostname}, `pwd`" > ${gensplit_active}

### Loop of gensplit upload and add meta
### for d in I* ; do 
order_pid_ser | while read d ; do 
  ( 
    dstr=`date +%Y%m%d-%H%M%S`
    echo "### in $d, ${dstr}, Start gensplit-upload-addmeta.sh " | tee -a ${stfile}
    (
      cd $d 
      # ./gensplit-upload-addmeta.sh 
      gensplit-onefile.sh 
      touch gensplit.done
    )
    if [ -e task3.opt ] ; then 
      echo "### Found task3.opt. Source it before moving to next file."
      source task3.opt
      echo "### end of sourcing task3.opt "
    fi

    dstr=`date +%Y%m%d-%H%M%S`
    timeleft=`dirac-proxy-info | grep timeleft `
    echo "### ${hostname} : ${dstr} | dirac-proxy timeleft is ${timeleft} at nloop=${nloop}"
    tl=`echo ${timeleft} | cut -d":" -f2 | sed -e "s| ||g"`

    if [ ${tl} -lt 6 ] ; then
      dstr=`date +%Y%m%d-%H%M%S`
      echo "### ${hostname} : ${dstr} | Remaining time of dirac proxy is less than 6 hours. proxy is initialized again."
      if [ ! -z ${MYPROD_PRODPROD} ] && [ -e ${MYPROD_PRODPROD} ] ; then
         source ${MYPROD_PRODPROD}
      fi
    fi
  ) 
  if [ -e task_step3.exit ] ; then 
    dstr=`date +%Y%m%d-%H%M%S`
    echo "### `hostname` : ${dstr} | task_step3.sh is terminated as task_step3.exit exists."
    rm -f ${gensplit_active}
    exit
  fi 
done 2>&1 | tee -a ${stfile}

date 2>&1 | tee -a ${stfile}


### Retry failed gensplit files
dstr=`date +%Y%m%d-%H%M%S`
echo " "
echo "### Completed upload and setmeta. Retying failed files. ${dstr}" | tee -a ${stfile}
gensplit-retry-all.sh 2>&1 | tee -a ${stfile}

### Another retry 

source ${MYPROD_PRODPROD} 2>&1 > proxy-init-${dstr}.log
gensplit-retry-2nd.sh 2>&1 | tee -a ${stfile}
recoverdir=`/bin/ls -vd recover-* | tail -1`
cmdlen=`cat ${recoverdir}/setmeta-command.cli 2>/dev/null | wc -l`
if [ ${cmdlen} -gt 0 ] ; then
  echo "### `date +%Y%m%d-%H%M%S` : Found ${cmdlen} files to set meta. set meta command is executed." | tee -a ${stfile}
  source ${recoverdir}/setmeta-command.cli 2>&1 | tee -a ${stfile}
fi

### Create a list of gensplit file and dstonly/recdst flag to them, if DSTONLY flag is set in production.json
dstonly=`jsonread production.json __DSTONLY`
recrate=`jsonread production.json __RECRATE`

if [ "x${dstonly}" == "xTRUE" -a "x${recrate}" != "x-1" ]; then 
  dstr=`date +%Y%m%d-%H%M%S`
  echo " "
  echo "### dstr : Creating a file, selected_reckeep_byID.json, for recdst/onlydst flags. ${dstr}" | tee -a ${stfile}
  set_selected_file.py | tee -a ${stfile}
fi

###############################
dstr=`date +%Y%m%d-%H%M%S`
echo " "
echo "### ${dstr} : All gensplit steps completed" 2>&1 | tee -a ${stfile}
rm -f ${gensplit_active}

