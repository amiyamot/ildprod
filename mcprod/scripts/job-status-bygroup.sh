#!/bin/bash 


echo "### Searching Dirac job group names in this directory and below."
find . -name "ddsimjob.json" -exec jsonread {} job_group_name \; | sort | uniq 2>&1 | tee jobgroup.list
echo " "
cat jobgroup.list | while read f ; do 
  echo "### Getting status of job-group $f "
  dirac-wms-job-status -g $f 2>&1 | tee -a job-status-$f.log
done

