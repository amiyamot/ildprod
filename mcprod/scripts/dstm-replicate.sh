#!/bin/bash 


put_request()
{
  echo "### Start dstm-replicate.sh "
  echo "    Date : `date` "
  echo "    host : `hostname` "
  echo "    pwd  : `pwd` "
  dstm_listfile="dstm-replicate-list.txt"
  for jinfo in dstmjobs-*/dstm.I*/jobinfo.txt ; do
    (
      . ${jinfo}
      echo ${_dstm_dirname}/${_dstm_file_name}
    )
  done > dstm-replicate-list.txt
  echo "### dstm-replicate-list.txt was created "

  # get information for transfer name from the first json file.
  ajson=`/bin/ls dstmjobs-*/dstm-jobmaker-*.json | head -1`
  echo "Getting parameters from ${ajson}"
  energy=`jsonread ${ajson} energy`
  evttype=`jsonread ${ajson} my_evttype`
  prodid=`jsonread ${ajson} dstm meta ProdID`

  # requestname="ILD-${energy}-${evttype}-${prodid}"
  requestname="ILD-P${prodid}-`date +%Y%m%d-%H%M%S`"

  selists0=`jsonread -a ${ajson} dstm replicates `
  if [ $? -ne 0 ] ; then 
    selists0='KEK-DISK'
    echo "Target SEs are not defined. Use ${selists} as default"
  fi
  selists=`echo ${selists0} | sed -e "s/,/ /g"`

  echo "### Executing following command"
  echo "dirac-dms-replicate-and-register-request ${requestname} dstm-replicate-list.txt ${selists} "
  dirac-dms-replicate-and-register-request ${requestname} dstm-replicate-list.txt ${selists} 2>&1 | tee -a create-request.log
  # grep "Request " create-request.log | grep "has been put to ReqDB for execution" | cut -d" " -f2 | cut -d"'" -f2 > rep-request.list
  grep "RequestID(s): " create-request.log | cut -d" " -f2- > rep-request.list
  echo "### Replication RequestID(s) will be found in rep-request.list"
  noelog=`jsonread ${recprod_json} conf __ELOG_NOELOG 2>/dev/null`
  if [ "x${noelog}" != "xTRUE" ] ; then
    putelog.py -c ${ajson} dstm_replicating 
  fi
  echo "### Complete dstm-replicate.sh : `date` "
  # echo "You can monitor requests' status using command: 'dirac-rms-request <requestName/ID>  "
  lenerror=`tail -1 create-request.log | grep -v "You can monitor requests' status using command" | wc -l`
  if [ "x${lenerror}" != "x0" ] ; then 
     echo "### ERROR when creating replicate-and-register-request. Log output was"
     cat create-request.log
  fi  

  echo "You can monitor requests' status using command: 'dirac-rms-request <requestISs>  "
}

export LANG=C

put_request 2>&1 | tee -a dstm-replicate.log 

