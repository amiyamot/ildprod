#!/bin/bash 
# Make sure first stdhep file on disk
#
firstfile=`head -1 input_genfiles.list | cut -d"," -f1`

if [ `echo ${firstfile} | grep -c -e "^/ilc/prod/"` -eq 0 ] ; then 
   localbase=`jsonread production.json __STDHEP_LOCALBASE`
   filepath=`echo ${localbase}/${firstfile} | sed -e "s| ||g" | sed -e "s|//|/|g"`
else
   filepath=`echo ${firstfile} | sed -e "s|^/ilc/prod/|/hsm/ilc/grid/storm/prod/|"`
fi

ghilsout=`ghils ${filepath}`
echo "### ghils output : ${ghilsout}"
ghilstest=`echo ${ghilsout} | cut -d" " -f1 | grep 'B'`
## echo "ghilstest is ${ghilstest}"
if [ "x${ghilstest}" != "xB" ] ; then  
  od -N 100 ${filepath} > /dev/null 2>&1 
  echo "od -N 100 was issued."
  ghils ${filepath}
fi

