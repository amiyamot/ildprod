#!/bin/bash 

dstr=`date +%Y%m%d-%H%M%S`
curdir=`pwd | cut -d"/" -f8-`
echo "### ${dstr} `hostname` ${curdir}, tail -5 of status.log"
tail -5 status.log


