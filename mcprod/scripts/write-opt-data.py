#!/bin/env python 
#
# Create a production summary based on the information on elog
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob
import ssl
from urllib2 import urlopen
import subprocess
import shutil

import ExcelTools
import ILDLockFile
from openpyxl import Workbook
from openpyxl import load_workbook


_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "opt-data"


# ######################################################
def open_logbook():
    ''' open elog logbook '''
    global _logbook, _elog_url, _elog_subdir, _do_test
   
    if _logbook == None:
        lines = open(os.environ["MY_ELOG_KEY"]).readlines()
        ( auser, apass ) = lines[0][:-1].split(' ')

        _logbook = elog.open("%s/%s/" % (_elog_url, _elog_subdir), user=auser, password=apass)

    return 

# ######################################################
def write_optdata(optdata_json, rfrom, rto):
    ''' Write data in optdata_json file to Elog opt-data database '''

    optdata = json.load(open(optdata_json))

    outinfo = {}
    ncount = 0
    for k, v in sorted(optdata.items()):
       ncount += 1
       if ncount < rfrom or ncount > rto:
          continue
       
       rec = {} 
       rec["Status"] = "OK"
       rec["GenProcessIDNum"] = long(v["ProcID"])
       rec["Ecm"] = str(int(v["ecm"]))
       rec["Machine"] = v["machine"]
       rec["EvtType"] = v["EvtType"]
       rec["ProcName"] = v["ProcName"]
       rec["Pol"] = v["BeamPol"]
       rec["Datatype"] = v["Datatype"]
       rec["ILDConfig"] = v["ILDConfig"]
       rec["Model"] = v["DetectorModel"]
       rec["ProdID"] = long(v["RecID"])
       rec["NEvents"] = long(v["NEvents"])
       xsect = float(v["xsect"])
       intlumi = float(v["intlumi"])
       rec["Xsect"] = "%-12.5g" % xsect
       rec["IntLumi"] = "%-12.3e" % intlumi if intlumi > 10000.0 else "%-10.1f" % intlumi
       rec["ElogIDNum"] = v["elogid"]
       rec["NFiles"] = int(v["ndstm"])
       filelist = v["dstm-info"]
       rec["ElogID"] = "<a href=\"%s/dbd-prod/%s\">%s</a>" % ( _elog_url, v["elogid"], v["elogid"] ) 

       genprocid = v["ProcID"]
       weblink = "<a href=\"https://ild.ngt.ndu.ac.jp/elog/genmeta/?process_id=%s\">%s</a>" % (genprocid, genprocid)
       rec["GenProcessID"] = weblink


       uploadfiles = [ str(v["dstm-info"]) ]
       # print uploadfiles
       msgtext = ""
       msg_id = _logbook.post(msgtext, attributes = rec, attachments = uploadfiles, encoding = "HTML" )
       
       del rec["unm"]
       del rec["upwd"]

       rec["elogid-opt-prod"] = msg_id

       outinfo[msg_id] = rec

       print( str(ncount) + " th record " + k + " is written to elog id " + str(msg_id) )

    return outinfo

# =========================================================
if __name__ == "__main__":

    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description='''\
    Insert new record in elog opt-data data base.  Input data is taken from elogdbs.json,
    which is created by web_summary.py
    Example:
        ./update-opt-data.py -i elogdbs.json --no_prompt
    ''')
    parser.add_argument("-i", help="Input data file. Default is elogdbs.json",
        dest="input", action="store", default="elogdbs.json")
    parser.add_argument("--no_prompt", help="No prompt before writting opt-data records", dest="no_prompt", action="store_true",
        default=False)
    parser.add_argument("--entries", help='''\
         First and last records in elogdbs.json, which are written in ELOG.
         Two numbers separated by \"-\" should be given.  
         If not specified, all records (10000 max.) in elogdbs.json are written to ELOG opt-data database.
         ''',
         dest="entries", action="store", default="1-10000")

    args = parser.parse_args()
    input_file = args.input
    no_prompt = args.no_prompt
    entries = args.entries.split("-")
    entry_from = long(entries[0])
    entry_to = long(entries[1]) 

    open_logbook()

    print( input_file + " entries from " + str(entry_from) + " to " + str(entry_to) +
           " are written to ELOG opt-data database." )
    if not no_prompt:
        ans = raw_input("Are you sure ? (Y to proceed) : ")
        if ans != "Y":
           print( "ELOG opt-data was NOT updated." )
           exit()

    out = write_optdata(input_file, entry_from, entry_to)
    print( "ELOG opt-data database was UPDATED." )
   
