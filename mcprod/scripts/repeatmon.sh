#!/bin/bash 

hostname=`hostname`
dstr=`date +%Y%m%d-%H%M%S`
echo "### start repeatmon.sh at ${hostname}  ${dtsr} : `pwd | cut -d"/" -f7-`"

# runtest_by_load.sh || ( echo "Too much load on this system. Repeatmon not started." ; exit 2 ; )

dirname=`basename ${PWD}`

task=""
if [ "x${dirname}" == "x6_dstmerge" ] ; then 
   task="task_step6.sh"
elif [ "x${dirname}" == "x7_logupload" ] ; then 
   task="task_step7.sh"
else
   echo "### ERROR :  executed in undefined directory, `pwd`"
   exit 31
fi

if [ -e repeat.log ] ; then 
   line0=`head -1 repeat.log | grep "repeattask of task_step"`
   if [ "x${line0}" == "x" ] ; then
     echo "### ERROR : First line of a file, repeat.log, is strange. It is not the one created by repeatmon.sh"
     exit 41
   fi

   taskdone=`grep "ALL TASKS DONE" repeat.log`
   if [ "x${taskdone}" != "x" ] ; then 
     echo "### ${task} ${dstr} "
     tail -1 repeat.log
   else
     msg1=`head -1 repeat.log | cut -d"|" -f1 | cut -d" " -f2- | sed -e "s/:/since/"`
     msg2=`head -2 repeat.log | tail -1 | cut -d" " -f2- | sed -e "s/Process ID = /pid /"`
     echo "### repeattask of ${task} at ${msg1}, ${msg2} : showing tail 5 status.log" 
     # echo "### ${task} ${hostname}  ${dtsr} : last 5 lines of status.log"
     tail -5 status.log
     echo "============"
   fi
else

   source ${SCRIPTS_DIR}/task_step6_lib.sh
   echo "Will check system load ..."
   check_status || exit 0

   echo "### #start ${task} at ${hostname}  ${dtsr} ########################"
   touch repeat.log
   repeattask.sh ${task} > repeat.log 2>&1 &
   echo "### sleep 20 sec."
   sleep 20
   tail -5 status.log
   echo "============ end of tail -5 status.log"
fi

sleep 15
exit 0

