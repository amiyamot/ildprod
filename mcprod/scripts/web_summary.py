#!/bin/env python 
#
# Create a production summary based on the information on elog
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob
import ssl
from urllib2 import urlopen
import subprocess

import ExcelTools
import ILDLockFile
from openpyxl import Workbook
from openpyxl import load_workbook


_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "dbd-prod"
# _elog_subdir = "test-prod"
_do_test = False

_genmeta = None
_prodpara_excel = None
_allsheets = None

WAKATAKE_IRO = "#66ff99"
SABISEIJI = "#d6e9ca"
LOWSTAT = "#bb5548"
LINEHEAD = "#ccff99"

# #######################################################
def build_web_data_summary(elogdata, nevplan):
    ''' '''
    by_evttype = {}
    elogid_detector = {}

    # pprint.pprint(elogdata)
    ildconfigs = []

    for elogid, an_entry in elogdata.items():        

       elogid_detector[elogid] = []

       for k, value in an_entry.items():

         if value["ILDConfig"] not in ildconfigs:
             ildconfigs.append(value["ILDConfig"])
         evttype = "%4.4d-%s.%s" % ( int(value["ecm"]), value["machine"], value["EvtType"] )
         nevents = long(value["NEvents"])
         nexists = nevents
         exceeds = False
         if nevents > value["totev_submit"]:
            nevents = long(value["totev_submit"])
            exceeds = True

         if evttype not in by_evttype:
            by_evttype[evttype] = {"NEvents":nevents, "elogid":[elogid], "exists":nexists, "exceeds":exceeds} 
         else:
            by_evttype[evttype]["NEvents"] += nevents # Number of available events, but not exceed totev_submit
            by_evttype[evttype]["exists"] += nexists  # Number of available events
            by_evttype[evttype]["exceeds"] = exceeds
            if elogid not in by_evttype[evttype]["elogid"]:
               by_evttype[evttype]["elogid"].append(elogid)
    
         if value["DetectorModel"] not in elogid_detector[elogid]:
            elogid_detector[elogid].append(value["DetectorModel"])


    webdata = []
    nevtotal = 0L
    nextotal = 0L
    totsub = 0L
    # for evttype, value in sorted(by_evttype.items()):
    subhead = ""
    linehead=[ "<td allign=\"right\">Nev plan</td>",
               "<td allign=\"right\">Nev prod.</td>",
               "<td allign=\"right\">Processed(%)</td>",
               "<td>ElogIDs</td>",
               "</tr>"] 
    for evttype, nevsub in sorted(nevplan.items()):  # evttype is like a E500-TDR_ws.2f_Z_nuNg, for example

      (ene, machine) = evttype.split(".")[0].split('-')
      totsub += nevsub
      if evttype.split(".")[0] != subhead:
          subhead = evttype.split(".")[0]
          webdata += ["<tr bgcolor=\"%s;\">" % LINEHEAD,
                      "<td>E%d-%s : EvtType</td>" % ( int(ene), machine ) ]
          webdata += linehead

      nevsub2 = long(nevsub)
      evtclass = evttype.split('.')[1]
      url = "https://ild.ngt.ndu.ac.jp/elog/opt-data/?Ecm={:d}&Machine={}&EvtType={}".format( int(ene), machine, evtclass)
      if len(ildconfigs) == 1:
         url += "&ILDConfig={}".format(ildconfigs[0])
      webdata.append("<tr><td><a href=\"{}\">{}</td><td align=\"right\">{:,d}</td>".format(url, evtclass, nevsub2 ) )
      nevprod = by_evttype[evttype]["NEvents"] if evttype in by_evttype else 0
      nexists = by_evttype[evttype]["exists"] if evttype in by_evttype else 0

      processed = float(nevprod)/float(nevsub2)*100.0
      overflow = ""
      if by_evttype[evttype]["exceeds"] == True:
          overflow = ">"
      webdata.append("<td align=\"right\">{:,d}</td>".format(nexists))
      if processed > 97.0:
         webdata.append("<td align=\"right\">%s%4.1f</td>" % (overflow, processed))
      else:
         webdata.append("<td align=\"right\"><span style=\"background-color: %s;\">%s%4.1f</span></td>" % (LOWSTAT, overflow,processed))

      weblinks = []
      nevtotal += long(nevprod)
      nextotal += long(nexists)

      if evttype in by_evttype:
         for eid in sorted(by_evttype[evttype]["elogid"]):
             detlist = []
             ovlstat = {"ovl":0, "nobg":0}

             for det in elogid_detector[eid]:
                 if "_o1_" not in det and det.split("_")[2] not in detlist:
                     detlist.append(det.split("_")[2])
                 for k in ovlstat.keys():
                     if "nobg" in det:
                         ovlstat["nobg"] += 1
                     else:
                         ovlstat["ovl"] += 1
             ovlflag = ""
             if ovlstat["nobg"] > 0:
                ovlflag = "nobg"
                if ovlstat["ovl"] > 0:
                   ovlflag = "ovl/nobg"
             if ovlflag != "" :
		   detlist.append(ovlflag)
             detopt = "" if len(detlist) == 0 else "(" + ",".join(detlist) + ")"

             weblink = "<a href=\"https://ild.ngt.ndu.ac.jp/elog/dbd-prod/%s\">%s%s</a>" % ( str(eid), str(eid), detopt )
             weblinks.append(weblink)
         webdata.append("<td>"+",".join(weblinks)+"</td>")
         webdata.append("</tr>")
      else:
         webdata.append("<td></td></tr>")


    donerate = float(nevtotal)/float(totsub)*100.0
    return {"webdata":webdata, "nevtotal":nevtotal, "nsubtotal":totsub, 
            "nextotal":nextotal, "processed":donerate, "ildconfigs":ildconfigs}

# =======================================================
if __name__ == "__main__":


    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,description='''\
    Create a web page for production summary.
    Example:
        ./web_summary.py 
    ''')
    parser.add_argument("--elogdata_dir", help="A directory of json file of production data", 
        dest="elogdata_dir", action="store", default="elog-dbd-prod")
    parser.add_argument("--webhtml", help="html file name for output", dest="webhtml", action="store", 
        default="summary-by-evttype.html")
   
    args = parser.parse_args()
    
    elogdata_dir = args.elogdata_dir
    webhtml = args.webhtml
   
    # elogdata = json.load(open(elogdata_json))

    elogdata = {}
    for jf in sorted(glob.glob(elogdata_dir+"/elog-dbd-prod-*.json")):
       (dummy, elogid) = jf.replace(".json","").rsplit('-',1)
       jdata = json.load(open(jf))
       elogdata[elogid] = jdata
       print( "loading elog " + str(elogid) + " from " + jf )

    elogids = sorted(elogdata.keys())
    elogid_from = elogids[0]
    elogid_to = elogids[-1]

    nevplan = {}

    for elogid in elogids:
        for k, data in elogdata[elogid].items():
            key = "%4.4d-%s.%s" % ( int(data["ecm"]), data["machine"], data["EvtType"] ) 
            nev_submit = long(data["totev_submit"])
            if key not in nevplan:
                nevplan[key] = nev_submit
            else:
                nevplan[key] += nev_submit
    
    aa_lowpt_rec={"0500-TDR_ws.aa_lowpt_BB":(148129+147805)*2,
                  "0500-TDR_ws.aa_lowpt_BW":(158361+157712)*2,
                  "0500-TDR_ws.aa_lowpt_WB":(158954+158009)*2,
                  "0500-TDR_ws.aa_lowpt_WW":(160790+160838)*2}
    for k,v in aa_lowpt_rec.items():
        nevplan[k] = aa_lowpt_rec[k] = v

    #
    ret = build_web_data_summary(elogdata, nevplan)
    now = datetime.datetime.now()
    nowstr = "%4.4d/%2.2d/%2.2d %2.2d:%2.2d:%2.2d" % ( now.year, now.month, now.day, now.hour, now.minute, now.second)
    ildconfig_info = ",".join(ret["ildconfigs"])
    webout = ["<html><head><title>Samples by evttype</title></head>", 
               "<body>",
               "<h1>Summary of samples of elogid from %s to %s</h1>" % (str(elogid_from), str(elogid_to)),
               "Total events: {:,d}".format(ret["nsubtotal"]) + " planned/ " + 
               "{:,d}".format(ret["nevtotal"]) + " produced/ " + 
               "{:,d}".format(ret["nextotal"]) + " available ",
               " (<span style=\"background-color: %s; font-weight: bold;\">processed %4.1f %% </span>" % (WAKATAKE_IRO, ret["processed"]) + ")",
               "<br>",
               "&bull; summarized based on the elog data for dst-merged events of ILDConfig ",
               "<span style=\"background-color: %s;\">%s</span>" % ( LINEHEAD, ildconfig_info),
               "<br>",
               "&bull; default detector model is ILD_l5/s5_o1_v02. Other model is indecated in () after elog id",
               "<br>",
               "&bull; produced/processed does not count events produced more than plan.",
               "<br>",
               "&bull;\"&gt;\" in the field \"Processed\" indicates that the nb. of produced events is larger than the plan in some processes.",
               "<br>",
               "( created on %s )" % nowstr,
               "<hr>", 
               "<table border=1 cellpadding=5 >"]

    webout += ret["webdata"]

    webout += ["</table>",
               "</body>",
               "</html>"]  
    # json.dump(res2, open("dstm-info.json","w"))
    # pprint.pprint(webout)

    fout = open(webhtml,"w")
    fout.write("\n".join(webout))
    fout.close()


      
