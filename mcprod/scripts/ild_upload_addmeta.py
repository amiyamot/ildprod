#!/usr/bin/env python 
########################################################
# Upload files and add meta
########################################################
'''
  Upload files and add meta information to them according to the input json file.
'''

import os
import pprint
import json

from DIRAC.Core.Base import Script
from DIRAC import gLogger, S_OK, S_ERROR

class _Params(object):
  def __init__(self):
    self.json_file = "upload_and_addmeta.json"

  def setInitFile(self, opt):
    self.init_file=opt
    return S_OK()

  def registerSwitches(self):
    Script.registerSwitch('I:', 'initfile=', 'A json file for file names and meta data (default=upload_and_addmeta.json)', self.setInitFile)
    msg = '%s [options]\n' % Script.scriptName
    msg += 'Function: Upload files and add meta information to them\n'
    msg += '          File name and meta data are taken from a json input file.\n'
    msg += 'json file format:\n'
    msg += "  {<local_file_name>:{\"lfn\":<upload_lfn>,\n"
    msg += "                      \"se\":<upload_se>,\n"
    msg += "                      \"filemeta\":{....meta data...}...}\n"
    Script.setUsageMessage(msg)

def upload_and_addmeta():
  ''' Main part of this script '''
  clip = _Params()
  clip.registerSwitches()
  Script.parseCommandLine( ignoreErrors = True )
  
  from DIRAC.DataManagementSystem.Client.DataManager import DataManager
  from DIRAC.Resources.Catalog.FileCatalogClient import FileCatalogClient
  from DIRAC import gLogger
  import DIRAC
  exitCode = 0

  if not os.path.exists(clip.json_file):
    gLogger.error("\n### Error ### : File %s must exist in order to get input parameters.\n" % clip.json_file)
    Script.showHelp()
    DIRAC.exit(10)

  fdict = open(clip.json_file)
  uldict = json.load(fdict)
  fdict.close()
  for local_file, param in uldict.items():
    if not os.path.exists(local_file):
       gLogger.error("\n### Error ### : File %s, requested by %s does not exist.\n" % (local_file, clip.json_file))
       pprint.pprint(uldict)
       DIRAC.exit(9)
    if "lfn" not in param:
       gLogger.error("\n### Error ### : a key, lfn, is not defined for file %s.\n" % local_file)
       DIRAC.exit(8)
    if "se" not in param:
       gLogger.error("\n### Error ### : a key, se, is not defined for file %s.\n" % local_file)
       DIRAC.exit(7)

  exitCode = 0
  
  # dm = DataManager()
  ascript = "dirac-upload.sh"
  for local_file, param in uldict.items():
    
    gLogger.notice("\nUploading %s to %s at %s\n" % (local_file, param["lfn"], param["se"]) )
    guid = None
    lfn = str(param["lfn"])
    se = str(param["se"])
    se2 = str(param["se2"]) if "se2" in param else "RAL-SRM"
    se3 = str(param["se3"]) if "se3" in param else "CERN-DST-EOS"
    error_msg = "Error: failed to upload"
    cmdline = ["#!/bin/bash", "which dirac-dms-add-file",
      "dirac-dms-add-file -ddd %s %s %s > dirac-upload1-%s.log 2>&1 " % ( lfn, local_file, se, se),
      "logout=`grep \""+error_msg+"\" dirac-upload1-%s.log`" % se,
      "if [ \"x${logout}\" == \"x\" ] ; then ",
      "  echo \"Upload succesful: 1st SE, %s\" " % se ,
      "  exit 0",
      "fi",
      "echo \"Upload to %s failed. Retry second SE, %s\" " % ( se, se2 ),
      "#",
      "dirac-dms-add-file -ddd %s %s %s > dirac-upload2-%s.log 2>&1 " % ( lfn, local_file, se2, se2),
      "logout=`grep \""+error_msg+"\" dirac-upload2-%s.log`" % se2,
      "if [ \"x${logout}\" == \"x\" ] ; then ",
      "  echo \"Upload succesful: 2nd SE, %s\" " % se2 ,
      "  exit 0",
      "fi",
      "echo \"Upload to %s failed. Retry thrid SE, %s\" " % ( se2, se3 ),
      "dirac-dms-add-file -ddd %s %s %s > dirac-upload3-%s.log 2>&1 " % ( lfn, local_file, se3, se3),
      "logout=`grep \""+error_msg+"\" dirac-upload3-%s.log`" % se3,
      "if [ \"x${logout}\" == \"x\" ] ; then ",
      "  echo \"Upload succesful: 3rd SE, %s\" " % se3 ,
      "  exit 0",
      "fi",
      "echo \"ERROR:UPLOAD file all failed. file=%s, SEs=%s,%s,%s\" " % ( lfn, se, se2, se3 ),
      "exit -1"]
      
    cmd = "\n".join(cmdline)

    fout=open(ascript,'w')
    fout.write(cmd)
    fout.close()
    import stat
    os.chmod(ascript,stat.S_IREAD|stat.S_IWRITE|stat.S_IEXEC)
    import subprocess
    exitCode=0
    try:
      output = subprocess.check_output(["./" + ascript], stderr=subprocess.STDOUT)
      for line in output:
        if "ERROT:UPLOAD file all failed." in line[:-1]:
          exitCode = 9
          print( '### Script returned with 0 but found "ERROR:UPLOAD" in logfile. return code is set to '+str(exitCode))
      print( "### output of "+ascript+" will follows.")
      print( output )
    except subprocess.CalledProcessError as e:
      print( "### Error.. "+ascript+" returned with returncode="+str(e.returncode) )
      print( "### cmd="+str(e.cmd) )
      print( "### output="+str(e.output) )
      exitCode=e.returncode

  if exitCode == 0:
    fc = FileCatalogClient()
    for local_file, param in uldict.items():
      lfn = str(param["lfn"])
      if "filemeta" in param:
        gLogger.notice("Adding meta to %s\n" % lfn)
        thismeta = param["filemeta"]    
        res = fc.setMetadata( lfn, thismeta )
        if not res['OK']:
          gLogger.error("### Error ### : Failed to set meta data to %s\n%s" % (lfn, res['Message']))          
          exitCode = 3
          continue
        res = fc.getFileUserMetadata(lfn)
        gLogger.notice("Meta data successfully added to %s" % lfn)
  
      if "ancestor" in param:
        ancestor = param["ancestor"]
        gLogger.notice("Adding ancestor %s to %s\n" % (ancestor, lfn) )
        try:
          result = fc.addFileAncestors({lfn:{'Ancestors':ancestor}})
          if not result['OK']:
            print( "Failed to add file ancestors to the catalog: " + result['Message'] )
          elif result['Value']['Failed']:
            print( "Failed to add file ancestors to the catalog: " +
                    result['Value']['Failed'][lfn] )
          else:
            print( "Added %d ancestors to file %s" % (len(ancestor),lfn) )
        except Exception as x:
          print( "Exception while adding ancestor: ", str(x) )
          exitCode = 2 
  
  print( "upload_and_addmeta will return with exitCode="+str(exitCode) )
  return exitCode
#  DIRAC.exit(exitCode)

def test_step():
  print( 'Hello world.' )
  return 123

def run_in_bash(prodcmd, usercmd):
  ''' create a script  '''
  script = 'testscript.sh'
  python_cmd = 'echo "from ild_upload_addmeta import * ; upload_and_addmeta()" '
  python_cmd += ' | python - -ddd 2>&1 | tee upload_and_addmeta.log '

  prodcmds = prodcmd.split('@')
  usercmds = usercmd.split('@')
  cmds0 = ['#!/bin/bash'] + prodcmds 
  cmds0.append(python_cmd) 
  cmds0.append('returncode=`grep exitCode upload_and_addmeta.log | cut -d"=" -f2`')
  cmds0.append('echo "### python_cmd returns with ${retuncode}"')
  cmds = cmds0 + usercmds + ['rm -f *.pyo', 'exit ${returncode}']
  cmd = '\n'.join(cmds)

  fout=open(script,'w')
  fout.write(cmd)
  fout.close()
  import stat
  os.chmod(script,stat.S_IREAD|stat.S_IWRITE|stat.S_IEXEC)

  import subprocess
  returncode=0
  try:
    output = subprocess.check_output(["./"+script], stderr=subprocess.STDOUT)
    for line in output:
      if "Error: failed to upload" in line[:-1]:
        returncode = 1
        print( '### Script returned with 0 but found "Error: failed to upload" in the log. return code is set to '+str(returncode) )

      # if "Error" in line[:-1]:
      #   returncode = 1
      #   print '### Script returned with 0 but found "Error" in the log. return code is set to '+str(returncode)
      # elif "Failed" in line[:-1]:
      #   returncode = 2
      #  print '### Script returned with 0 but found "Failed" in the log. return code is set to '+str(returncode)
    print( "### output of "+script+" will follows." )
    print( output )
  except subprocess.CalledProcessError as e:
    print( "### Error.. "+script+" returned with returncode="+str(e.returncode) )
    print( "### cmd="+str(e.cmd) )
    print( "### output="+str(e.output) )
    returncode=e.returncode
    
  os.remove(script)

  print( '### run_in_bash returns with returncode='+str(returncode) )

  return returncode    

if __name__ == '__main__':

  print( "### ild_upload_addmeta started" )
  #  returncode = upload_and_addmeta()

  import sys
  argvs = sys.argv
  arg1 = argvs[1]
  arg2 = argvs[2]

  print( "### ild_upload_addmeta started" )
  print( "### First argument = "+arg1 )
  print( "### Second argument= "+arg2 )
  returncode = run_in_bash(arg1, arg2)

  print( "### ild_upload_addmeta completed with return code ="+str(returncode) )
  exit(returncode)

