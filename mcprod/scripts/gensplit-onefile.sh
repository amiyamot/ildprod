#!/bin/bash 
#
# Do gensplit, upload, setmeta of one directory (one file).
# Name of the run directory should "I<process_id>.<serial_numer>", 
# which is found in the file name
# script for single gen. dir 

# ###########################################################
# Parse arguments
# ###########################################################

print_help(){
cat <<EOF
Usage: gensplit-onefile.sh [OPTION]

Split a generator file, then upload and setmeta to DIRAC.
The information of generator file is taken from production.json, using
genmeta json file.

Optional parameters.
  --no_split     Skip splitting.
  --no_setmeta   Do no upload and setmeta
  -h, --help     Print this help message
EOF
}


nosplit=0
nosetmeta=0

while [ $# -ne 0 ] ; do 
  case "$1" in 
    --no_split) nosplit=1 ;;
    --no_setmeta) nosetmeta=1 ;;
    -h) print_help ;; 
    --help) print_help ;;
  esac
  shift
done

# ###########################################################
# Obtain run parameters
# ###########################################################
export LANG=C
echo started `hostname` `date` 
ln -sf ../production.json . 
nw_per_file=`jsonread production.json __SPLIT_NW_PER_FILE`
maxread=`jsonread production.json __SPLIT_MAX_READ`
nskip=`jsonread production.json __SPLIT_SKIP_NEVENTS`
nbfiles=`jsonread production.json __SPLIT_NB_FILES`
datadir=`jsonread production.json __STDHEP_LOCALBASE`
recrate=`jsonread production.json __RECRATE`
numprocs=`jsonread production.json __GENSPLIT_NUMPROCS`
genmeta_json=`jsonread production.json __GENMETA_JSON 2>/dev/null`

updir=`(cd .. && /bin/pwd)`
curdir=`/bin/pwd | sed -e "s|${updir}||g" -e "s|/||g"`
pid_seq=`echo ${curdir} | sed -e "s|I||g" -e "s|\.| |g"`

echo "### running in ${curdir}, decoded as ${pid_seq}"

if [ "x${genmeta_json}" != "x" ] ; then export GENMETA_JSON=${genmeta_json} ; fi 
if [ "x" == "xundef" ] ; then datadir="undef" ; fi 
echo "gensplit.opt can be used to set nrec_per_file, maxread, nskip dependently on input file." 

###########################################################
# set optional parameters
###########################################################
if [ -e ../common.opt ] ; then . ../common.opt ; fi
if [ -e gensplit.opt ] ; then . gensplit.opt ; fi

###########################################################
# Split original generator files.
###########################################################
if [ ${nosplit} -eq 0 ] ; then
  datadir_opt="-d ${datadir}"
  if [ "x${datadir}" == "x" ] ; then 
     datadir_opt=""
  fi
  echo "A command will be"
  echo "Stdhep2LcioSplit ${pid_seq} -n ${nw_per_file} -m ${maxread} -s ${nskip} -N ${nbfiles} ${datadir_opt} 2>&1 | tee gensplit.log"

  Stdhep2LcioSplit ${pid_seq} -n ${nw_per_file} -m ${maxread} -s ${nskip} -N ${nbfiles} \
		${datadir_opt} 2>&1 | tee gensplit.log
else
  echo "No Stdhep2LcioSplit was requested."
fi

##########################################################
# Upload files and set meta 
##########################################################
if [ ${nosetmeta} -eq 0 ] ; then
  split_begin=`jsonread lciosplit.json FileSplitNumberFirst` 
  split_last=`jsonread lciosplit.json FileSplitNumberLast` 
  echo spit_begin=${split_begin}  split_last=${split_last} 
  setMetaGenSplit -i 1
  echo "Start setMetaGenSplit from ${split_begin} to ${split_last} numprocs=${numprocs}, subdir=${curdir} "
  seq ${split_begin} ${split_last} | xargs -P ${numprocs} -I{} setMetaGenSplit -d ${curdir} {} 2>&1 | tee setmeta.log 

##########################################################
# Set additional meta when not all files are processed.
##########################################################
  [ -e lciosplit.json ] && [ "x${recrate}" != "x-1" ] && ( cd .. && set_selected_file.py )
else
  echo "Neither Upload nor Setmeta were requested."

fi
echo " ### gensplit completed `hostname` `date` " 
