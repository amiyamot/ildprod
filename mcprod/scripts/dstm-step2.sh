#!/bin/bash
#
# Submit DST merge jobs
#


if [ "x${DIRAC}" == "x" ] ; then
  echo "DIRAC environment is not initialized yet."
  echo "Execute after dirac-user proxy is initialized"
  exit 10
fi

echo "### Switching to user proxy" | tee -a ${logf}
. ${MYPROD_USERPROD}

jobmakers="jobmaker-*.log"

if [ "x$1" != "x" ] ; then 
  jobmakers=$1
  echo "### Jobmaker files given by argument is ${jobmakers}" 2>&1 | tee -a ${logf}
fi

logf="dstm-step2.log"
match_string="### ILD_DSTM_Job_Maker completed with code 0"
match_string2="### ILD_DSTM_Job_Maker completed with code 2"
nfile=0
for f in ${jobmakers} ; do
  nfile=$[$nfile+1]

  echo "### jobmaker log file is ${f}" 2>&1 | tee -a ${logf}
  if [ ! -e ${f} ] ; then 
    echo "### ERROR in dstm-step2.sh : ${f} does not exist." 2>&1 | tee -a ${logf}
    exit 20
  fi
  if [ `tail -20 ${f} | grep -c "${match_string2}" ${f}` != 0 ] ; then
    echo "### Skipping ${f} because no DSTs were found." 2>&1 | tee -a ${logf}
    continue
  fi

  if [ `tail -20 ${f} | grep -c "${match_string}" ${f}` != 0 ] ; then
    echo "### dstm-step1.sh is completed. Proceed to the next step" 2>&1 | tee -a ${logf}
    cmd=`tail -10 ${f} | grep python | grep dstm_sub.py`
    jobtop=`tail -10 ${f} | grep python | grep dstm_sub.py | sed -e "s|^ *python ||" | cut -d"/" -f1`
    dstmmaker_json=`( cd ${jobtop} && ( /bin/ls dstm-jobmaker-*.json | head -1 ))`
    if [ -z ${dstmmaker_json} ] ; then 
       echo "### ERROR in dstm-step2.sh : dstm-jobmaker-*.json not found in ${jobtop}" 2>&1 | tee -a ${logf}
       exit 21
    fi

# Confirm produced jobs process all DST files.
    unused=`grep d_dst_ ${f} | grep "### Warning:" | grep slcio | wc -l`
    numdsts=`grep d_dst_ ${f} | grep -v "### Warning:" | grep slcio | wc -l`
    ndsts=$[${numdsts}-${unused}]
    tdir=${jobtop}
    jdsts=`cat $tdir/dstm.I*/dstlist.txt | wc -l`
    if [ "x${ndsts}" != "x${jdsts}" ] ; then 
       echo "### ERROR in dstm-step2.sh : # of input dsts (${ndsts}) and those used by merge jobs (${jdsts}) are not equal. jobmaker is ${f}. Job is not submitted." 2>&1 | tee -a ${logf}
       exit 12
    fi

    tosub=`/bin/ls -vd ${tdir}/dstm.I*.j* | wc -l`
    echo "### dstm_sub.py for ${f} will be executed to merge ${ndsts}/${jdsts} dsts by(to) ${tosub} jobs/dstm-files." 2>&1 | tee -a ${logf}
    if [ -e ${tdir}/repo.rep ] ; then 
       ( cd ${tdir} ; dstr=`date +%Y%m%d-%H%M%S` ; mv -v repo.rep repo.rep-${dstr}  )
    fi

    echo "### Loop over ${tdir} directory to submit job. " 2>&1 | tee -a ${logf}
    rm -f dstm-step2-submit.log
    ( cd ${tdir} && /bin/ls -d -v dstm.I*.n*.j* ) | while read subdir ; do
       if [ ! -e ${tdir}/${subdir}/submit.json ] ; then 
          job_number=`echo ${subdir} | cut -d"." -f4 | sed -e "s/^j//"`
          expr ${job_number} + 1 >&/dev/null
          if [ $? -eq 0 ] ; then 
            python ILD_DSTM_Submit.py ${tdir} ${job_number} 2>&1 | tee dstm-step2-submit.log | tee -a ${logf}
            suberror=`tail dstm-step2-submit.log | grep "Failed to submit job"`
            if [ "x${suberror}" != "x" ] ; then 
               echo "### FAILED to submit job, ${tdir} job_number ${job_number}" 2>&1 | tee -a ${logf}
               echo "### Further submission is abandoned. Retry later" | tee -a ${logf}
               exit 13
            fi
          fi 
       fi
    done
    echo "###### Completed submit loop of DST merge job of ${tdir}" | tee -a ${logf} 

    # echo "####################################" 2>&1 | tee -a ${logf}
    # echo "${cmd} will be executed." 2>&1 | tee -a ${logf}
    # echo "Current dir is ${PWD}. " 2>&1 | tee -a ${logf}
    # ls 2>&1 | tee -a ${logf}
    # echo "####################################" 2>&1 | tee -a ${logf}
    # ${cmd} 2>&1 | tee -a ${logf}
    # echo "###### Completed ${cmd} " 2>&1 | tee -a ${logf}

    # nsub=`grep Submitted ${tdir}/repo.rep | wc -l`
    nsub=`/bin/ls -v ${tdir}/dstm.I*/submit.json | wc -l`
    if [ "x${nsub}" != "x${tosub}" ] ; then 
       echo "### ERROR in dstm-step2.sh : only ${tosub} out of ${nsub} jobs were submitted for ${f}.  Something is wrong." 2>&1 | tee -a ${logf}
       exit 13
    fi

    echo "### Submission of DST merge job in ${tdir} has completed." 2>&1 | tee -a ${logf}
  else
    echo "DST merge job of ${tdir} do not completed yet." 2>&1 | tee -a ${logf}
  fi
done
echo "${nfile} jobmaker log files were processed" 2>&1 | tee -a ${logf}

echo "### Switching to prod proxy" | tee -a ${logf}
. ${MYPROD_PRODPROD}

echo "### Set meta data of output directory. " 2>&1 | tee -a ${logf}
for d in dstmjobs-* ; do
  cmd="./setdirmeta.sh"
  echo "######## ${cmd} in ${d} ########" 2>&1 | tee -a ${logf}
  ( cd $d &&  ${cmd} ) 2>&1 | tee -a ${logf}
  echo "###### Completed ${cmd} " 2>&1 | tee -a ${logf}
done

echo "### dstm-step2.sh has completed." | tee -a ${logf}
   
