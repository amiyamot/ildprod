#!/bin/bash 

export LANG=C
echo "### Logmon"

dirlist="prod-4f_1V_leptonic/*.1lep"


if [ -e sleep.log ] ; then 
  echo "+++ start production script is sleeping"
  cat sleep.log
fi

#########################################################################

echo "### startprod status ###"
echo "### `cat active-prod-dir.list | wc -l` active prod."
echo "### `cat startprod.list 2>/dev/null | wc -l` prod in queue. Next is `head -1 startprod.list 2>/dev/null`"
if [ -e startprod.active ] ; then
  cat startprod.active
  [ -e startprod.log ] && tail -5 startprod.log
  echo "  "
else
  nqueue=`cat startprod.list | wc -l`
  if [ "x${nqueue}" == "x0" ] ; then 
    echo "+++ startprod.sh is not active and not prod in queue."
  else
    echo "+++ WARNING : There are ${nqueue} prod in queue, but startprod.sh is not running"
  fi
  echo "  "
fi

#########################################################################

( [ -e active-prod-dir.list ] && grep -v -E "^#" active-prod-dir.list ) | while read d ; do
   outfile=${d}/3_gensplit/status.log
   echo ${outfile}
   if [ -e ${outfile} ] ; then
     echo "<<< gensplit $d >>>"
     iscomp=`tail -5 ${outfile} | grep "###" | grep -e "All gensplit steps completed" -e "all gensplit step completed"`
     if [ "x${iscomp}" != "x" ] ; then 
       nslcio=`tail -1000 ${outfile} | grep "###" | grep " gensplit " | grep " localfiles" | \
              grep "on dirac catalog" | tail -1 | \
              cut -d":" -f2 | cut -d" " -f3`
       endtime=`tail -5 ${outfile} | grep "All gensplit steps completed" | cut -d":" -f1 | cut -d" " -f2`
       grep " command not found" ${outfile} | grep " line " | grep "mcprod/scripts" 
       echo "+++ completed. ${nslcio} files at ${endtime}"
     else 
       lsmsg=`/bin/ls -d ${d}/3_gensplit/I* 2>&1 | grep "No such file or directory"`
       if [ "x${lsmsg}" != "x" ] ; then
         echo "+++ gensplit ${d} : no gensplit directory (I*) found "
       else
         hostname=`head -1 ${outfile} | cut -d" " -f7 | sed -e "s/\.kek\.jp//"`
         stime=`head -1 ${outfile} | cut -d" " -f9 | sed -e "s/2018//"`
         ngenfile=`/bin/ls -d ${d}/3_gensplit/I* | wc -l`
         nprocess=`/bin/ls ${d}/3_gensplit/I*/setmeta.log | wc -l`
         ndone=$[nprocess-1]
         currmetaf=`/bin/ls ${d}/3_gensplit/I*/setmeta.log | tail -1`
         curdir=`dirname ${currmetaf}`
         nspfiles=`/bin/ls ${curdir}/gensplit | wc -l`

         if [ ${ndone} -gt 0 ] ; then 
            dplast=`grep  "gensplit completed" ${outfile} | uniq | tail -1 | cut -d" " -f6-`
            dp=`grep "Start gensplit-upload"  ${outfile}  | uniq | tail -2 | head -1 | cut -d" " -f4 | cut -d"," -f1`
            dpfile=`grep "Start gensplit-upload"  ${outfile}  | uniq | tail -2 | head -1 | cut -d" " -f3 | cut -d"," -f1`
            
            dps="${dp:0:4}/${dp:4:2}/${dp:6:2} ${dp:9:2}:${dp:11:2}:${dp:13:2}"
            dpbgn=`date -d "${dps}" "+%s"`
            dplast=`date -d "${dplast}" "+%s"`
            dpused=`expr ${dplast} - ${dpbgn}`
            dpend=`echo "${dpused}*(${ngenfile}-${ndone})+${dplast}" | bc`
            dpscnd=`echo "${dpused}/60.0" | bc`
            dpend_str=`date -d "@${dpend}" "+%m/%d(%a) %H:%M"`
         else
            dpend_str="No estimate yet"
            dpscnd="not known"
            dpfile=""
         fi
         ntask_max=`echo "${ngenfile}*${nspfiles}" | bc`
         numprocs=`grep numprocs ${outfile} | tail -1`

         echo "+++ ${ngenfile} dirs, ${ndone} done, now `basename ${curdir}`/ ${nspfiles} files, ${hostname} since ${stime} " 
         echo "+++ ntask_max ${ntask_max}, ${dpscnd} min/file ${dpfile}, will end arround ${dpend_str}"
         [ "x${numprocs}" != "x" ] && echo "+++    ${numprocs}"

     # check time past last update of ${outfile}
         modlast=`date -r ${outfile} +%s`
         timenow=`date +%s`
         diffsec=$[${timenow}-${modlast}]
         if [ ${diffsec} -lt 2400 ] ; then 
           echo "+++ last 5 lines of ${outfile} modified ${diffsec} sec. ago. "
         else
           echo "??? NO UPDATE of ${outfile} in last ${diffsec} sec. Last 5 lines are ???"
         fi
         tail -5 ${outfile}
       fi
     fi  
   else
     if [ -e startprod.list ] ; then 
       if [ "x`grep ${d} startprod.list`" != "x" ] ; then 
         echo " "
         echo "+++ gensplit ${d} not started yet."
       fi
     fi 
   fi

   # 
   if [ ! -e ../6_dstmerge/prodstatus.log ] ; then
     if [ -e ${d}/4_subprod/simfiles.list ] ; then 
       lensim=`cat ${d}/4_subprod/simfiles.list | wc -l`
       echo "### New ${lensim} sim files were found for setting SelectedFile meta"
     fi 
   fi
   echo "            "

done

