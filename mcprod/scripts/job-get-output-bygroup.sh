#!/bin/bash 

echo "### Download output sandbox to jobout/ and delete jobs from DIRAC."
find . -name "ddsimjob.json" -exec jsonread {} job_group_name \; | sort | uniq 2>&1 | tee jobout-group.list
echo " "

cat jobout-group.list | while read f ; do 
  joboutdir=jobout-$f
  mkdir -p ${joboutdir}
  echo "### Download output sandbox to ${joboutdir} "
  ( 
     cd ${joboutdir}
     dirac-wms-job-get-output -g ${f} 
     dirac-wms-job-delete -g ${f}    
  ) 2>&1 | tee -a job-output-$f.log
done

echo "### Job output completed."
