'''
ILDProdUserJob.py 

  A utility class for job submission of DDSIM and Marlin production by UserJob

  Akiya Miyamoto  17 July, 2017
'''

from ILDProdUserJob import *

import json 
import datetime
import os
import subprocess
import pprint

import ILDIDTool

########################################################


if __name__ == '__main__':
  params = {}
  params["jobtype"] = "SelectedPairs"
  params["isLocal"] = False
  params["isTest"] = False
  params["noUpload"] = False

  # res=ILDIDTool.get_nextid("gensplit", logtext="ddsim of GuineaPig , 250-SetA")
  # if res["OK"] != "True":
  #    print "Failed to get next job id"
  # jobid = res["NEXTID"])
  jobgid = 500576
  params["jobgroupid"] = str(jobgid)
  params["number_of_events"] = -1 if not params["isTest"] else 10
  params["ILDConfig"] = "v02-00-02"
  params["DDSIM_version"] = "ILCSoft-02-00-02_gcc49"
  params["detectors"] = ["ILD_l5_v03", "ILD_s5_v03"]
  params["InputSandBox"] = ["LFN:/ilc/prod/ilc/ild/software/lcgeo-ild-compact.small-steplength-for-pair.tar.gz"]
  params["gendir"] = "/ilc/prod/ilc/mc-opt.dsk/ild/gensplit/250-SetA/selectedpairs/500570"
  params["list_of_genfiles"] = "input_genfiles.csv" if not params["isTest"] else "input_genfiles_test.csv"
  params["extraCLIArguments"]=" --compactFile lcgeo/ILD/compact/%detector_model%/10mm.xml "
  params["simSE"] = "KEK-SRM"
  # default value
  # params["simfile_format"] = "s%s.m%m.P%P.I%I.n%n.d_%d_%t.slcio"
  # params["simdir_format"] = "/ilc/prod/ilc/mc-opt-3/ild/sim/%E/%C/%m/%s/%t"
  # params["sleep_seconds"] = "10" # in unit of seconds. Sleep time before submitting the job.


  subCalibDDSim(params)

