# ILDProd Changelog ( only major changes. )

Master branch is tagged when some milestone is passed. Master branch used for production 
would evolve during a period between one tag to the next depending on needs for production
if the change is not significant to declare it a milestone.

## v02-00-02 (2018-10-04)
- Reorganize files and directories, such as moving scripts and ILDTools in mcprod/v02-00-01
to mcprod/scripts and python. 
- auto/ directories are created for cron tools.
- bin/setup.sh is cleaned-up. Environment parameters required to source this file is 
  described in this file. PRODKEY_FILE should points a file to define environment 
  parameters for secret information. 
- scripts/task_step6.sh, for DST-Merge task, was revised so that it can be re-executed easily in case of error.
- mcprod/testprod was created for test production tools.
- Updated files in docs


## v02-00-01-p2 
- Prepared to do 1 TeV production while makeing a major update for v02-00-02. 
  mcprod_config.py includes setting for v02-00-02 production


## v02-00-01-p1 (2018-08-29)
- Removed stale files
- In order to prepare production with v02-00-01, current state is tagged.

## v02-00-01 (2018-06-28)
- A version used for v02-00-01 production was tagged at the end of June 2018.
Most 2018 Opt production had completed but production of additional request was done 
by a version later than v02-00-01.

## v02-00 (2018-05-21)
- A tag just before switching to v02-00-01 ILCSoft.  v02-00 production was problematic.

## v01-19-06 (2018-04-20)
- A tag just before switching to v02-00 ILCSoft

