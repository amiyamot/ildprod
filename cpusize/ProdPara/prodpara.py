#!/bin/env python 
#
# Create parameters for MCProduction from genmeta.json and cpusize.json
# Output is writtten to an excel file, prodpara.xls
#
# 
#  evtclass
#  split_nw_per_file
#  generator name : whizard-1.95 or whizard-1_95
#  a list of generator file path.
#  splitdir name
#  split_prodid
#
# Input to this script is 
#  energy

import json
import os
import pprint
import ast
import sys
import xlsxwriter
import pycolor
import argparse
import braceexpand

if "GENMETA_JSON" not in os.environ:
    print("FATAL ERROR: GENMETA_JSON environment parameter is not defined.")
    exit(0)
# _genmetaByID_json="/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta/genmetaByID.json"
_genmetaByID_json=os.environ["GENMETA_JSON"]

_cpusize_json = "../Overlay/cpusize.json"
_nevgen_json = "nevgen/nevlist.json"
_omit_process_id_file = "omit_process_id.txt"

_cpusize = None
_genmeta = None
_nevgen = None
_omit_process_id = None
_options = {}
_debug = False

# _maxsize = 2000.0 # MB 
_maxsize = 1300.0 # MB 
_maxcpu = 8.0*3600.0 # sec
_maxjobs = 30000L # Expected number of max jobs
_evtgroup = ["4f"]
pyc = pycolor.pycolor()

# ####################################################################
def omit_process_id(omitfile):
    alist = []
    if not os.path.exists(omitfile):
       return alist
    for line in open(omitfile):
       alist.append(line.replace("\n",""))
    return alist


# ####################################################################
def collect_by_process_type(evtgroup):
  no_np_process_type = _options["no_np_process_type"]
  evtclass = _options["evtclass"]
  grname = {}
  thisgroup= _cpusize[evtgroup]
  for pid, cpusize in thisgroup.items():
    if pid[0:1] == "I" and pid in _nevgen:
      # print "pid is "+pid
      # metakey = cpusize["metakey"]
      metakey = pid[1:]
      genmeta = _genmeta[metakey]
      process_type = genmeta["process_type"]

      if evtclass != "":
         process_type = evtclass
      elif process_type == "500":
         process_type = evtgroup
      elif process_type[:3] == "np-" and not no_np_process_type and "_" in genmeta["process_type"]:
         process_type = genmeta["process_type"].split("_")[0]
      if process_type not in grname:
         grname[process_type] = {}
      grname[process_type][pid] = {}
      jobinfo = decide_nbevents(cpusize)
      prodinfo = find_files(pid, jobinfo, genmeta)
      grname[process_type][pid]["job"] = jobinfo
      grname[process_type][pid]["prod"] = prodinfo 
      grname[process_type][pid]["genmeta"] = genmeta

    elif pid in _omit_process_id:
      continue
    
    elif pid[0:1] == "I" and not pid in _nevgen:
      print pyc.c["red"] + "PID %s " % ( pid ) + " not in _nevgen" + pyc.c["end"]

  return grname

#######################################################################
def find_files(pid, jobinfo, genmeta):
  # print "find_files ... " + str(pid)
  nevreq = _nevgen[pid]["nev"]
  if long(nevreq) < 1: 
     print "Too small nevreq: nevreq="+str(nevreq)+" pid="+str(pid)
     print "Something is wrong."
     exit(10)

  prtype = _nevgen[pid]["prtype"]
  gentotev = genmeta['total_number_of_events']
  filenames = genmeta['file_names']
  if "*" not in genmeta["number_of_events_in_files"]:
     nbevents = genmeta["number_of_events_in_files"].replace(',',';').split(";")
  else:
     (kevents, kfiles) = genmeta["number_of_events_in_files"].split("*")
     nbevents = [int(kevents)]*int(kfiles)
  indfile = 0L  # index to event file.
  nbevjob = long(jobinfo["nbevjob"]) # Nb of events per job
  nbtot = 0L   # Total Nb of events
  nbjobs = 0L  # Nb of jobs
  # print "prtype = "+prtype + " nbevjob=" + str(jobinfo["nbevjob"])
  while nbtot < nevreq or prtype == "higgs":
    # print "indfile="+str(indfile)
    # 
    if indfile >= len(nbevents):
      # print "pid="+pid+" No more gen files. len(nbevents)="+str(len(nbevents)) + " indfile="+str(indfile)+"  nevreq="+str(nevreq)+" "+str(nbtot)
      # print nbevents
      # indfile -= 1L
      break
    nbtot += long(nbevents[indfile])
    nbjobs += long(nbevents[indfile])/nbevjob 
    indfile += 1L

  if prtype == "single" and indfile == 1L:
     nbtot = nevreq
     nbjobs = nbtot/nbevjob
     
  # nbebents -- actual number of events to produce"
   
  # print " PID="+str(pid)+" nbevents="+str(nbtot)
  ret = { "nbevents":nbtot, "nbjobs":nbjobs, "nbfiles":indfile, 
          "nevreq":nevreq} 
  ret["cpusec"] = {"sim":jobinfo["evsec"]["sim"]*float(nbtot), 
                "rec":jobinfo["evsec"]["rec"]*float(nbtot)}
  ret["sizeMB"] = {"sim":jobinfo["evMB"]["sim"]*float(nbtot),
                   "rec":jobinfo["evMB"]["rec"]*float(nbtot), 
                   "dst":jobinfo["evMB"]["dst"]*float(nbtot)}
  return ret

#####################################################################################
def decide_nbevents(cpusize):
  size_ev_MB = cpusize["rec_size_ev_MB"] if cpusize["rec_size_ev_MB"] > \
               cpusize["sim_size_ev_MB"] else cpusize["sim_size_ev_MB"]
  cpu_ev_sec = cpusize["rec_cpu_ev_sec"] if cpusize["rec_cpu_ev_sec"] > \
               cpusize["sim_cpu_ev_sec"] else cpusize["sim_cpu_ev_sec"]
  nbev_size = _maxsize/size_ev_MB 
  nbev_cpu = _maxcpu/cpu_ev_sec
  nbevts = nbev_cpu if nbev_cpu < nbev_size else nbev_size
  machinepara = cpusize["machinePara"]
  procid = cpusize["process_id"]
  # processname = genmeta["process_name"] if "process_name" in genmeta else genmeta["process_names"]

  if "calib" in machinepara:
      # if "single" in processname:
      # elif "uds" in processname:
      nevreq = int(_nevgen[procid]["nev"])
      nevtot = int(_genmeta[procid[1:]]["total_number_of_events"])
      # print "nevreq="+str(nevreq)+" nevtot="+str(nevtot)
      nbevjob = nbevts if nbevts < nevreq else nevreq
      nbevjob = nbevjob if nbevjob < nevtot else nevtot
      # print "nbevjob="+str(nbevjob)
      # print "nevreq ="+ str(nevreq) + " nbevjob=" + str(nbevjob)
      limits = [10000000, 200000, 100000, 20000, 10000, 5000, 2000, 1000, 600, 500, 400, 200, 100, 50]
      for l in range(1, len(limits)):
         if nbevjob < limits[l-1] and nbevjob >= limits[l]:
            nbevjob = limits[l]

  else:
      #   -99, -199, -299, -399, -499, -599, -699, -799, -899, -999, 1000- 
      #    100, 100,  200,  200,  400,  400,  600,  600,  600,  600, 1000   
      nbnorm = [1,1,2,2,4,  4,6,6,6,6, 10,10, 15,15,15,15,15, 20,20,20,20,20 ]
      nbnorm += 5*[25] + 5*[30] + 10*[40] + 10*[50]
      nbjind = int(nbevts/100.0) if nbevts < int(100*nbnorm[-1]) else len(nbnorm)-1 
      nbevjob = 100*nbnorm[nbjind]
      # print( " nbevts=%d, nbjind=%d" % ( nbevts, nbjind) + " nbevt_size %d, nbev_cpu=%d, nvevts=%d" % ( nbev_size, nbev_cpu, nbevts) )
  
  simcpu = float(nbevjob)*cpusize["sim_cpu_ev_sec"]
  reccpu = float(nbevjob)*cpusize["rec_cpu_ev_sec"]
  simsize = float(nbevjob)*cpusize["sim_size_ev_MB"] 
  recsize = float(nbevjob)*cpusize["rec_size_ev_MB"] 
  dstsize = float(nbevjob)*cpusize["dst_size_ev_MB"] 

  # print(" simsize="+str(simsize))
  
  return {"nbevjob":nbevjob, "nbevopt":nbevts, 
          "cpu":{"sim":simcpu, "rec":reccpu},
          "size":{"sim":simsize, "rec":recsize, "dst":dstsize},
          "evsec":{"sim":cpusize["sim_cpu_ev_sec"], "rec":cpusize["rec_cpu_ev_sec"]}, 
          "evMB":{"sim":cpusize["sim_size_ev_MB"], "rec":cpusize["rec_size_ev_MB"], 
                  "dst":cpusize["dst_size_ev_MB"]} }

########################################################################
def write_sumgrp_sheet(sumgrp, abook):
  sheet2 = abook.add_worksheet("summary")

  format_2digit = abook.add_format()
  format_2digit.set_num_format('0.00')
  sheet2.set_column('A:A',30.0)
  sheet2.set_column('B:B',10.0)
  sheet2.set_column('C:C',25.0)
  sheet2.set_column('D:H',7.0)
  sheet2.set_column('I:M',9.0, format_2digit)
  sheet2.set_column('N:N',10.0)
  sheet2.set_column('O:O',30.0)
 
  varname = ["subgroup", "group", "process_type", "nbevjob", "nbjobs", "nbprocid", "nbinputs",
             "totev_submit",
             "totday_sim", "totday_rec", "totGB_sim", "totGB_rec", "totGB_dst", 
             "whizard_info", "pids", "nrow"]
  for ic in range(0, len(varname)):
    sheet2.write(0, ic, varname[ic])

  row = 1
  groupsum = {}
  grkeys = ["group", "nbprocid","nbjobs","total_nb_inputs","totev_submit", 
            "totday_sim", "totday_rec", 
            "totGB_sim", "totGB_rec", "totGB_dst"] 
  for key in sorted(sumgrp.keys(), reverse=True):
    v = sumgrp[key]
    pids = ""
    for pidk, pidv in v["filesum"].items():
      pids += pidk + ","

    values = [ key, v["group"], v["process_type"], v["nbevjob"], v["nbjobs"], 
             v["nbprocid"], v["total_nb_inputs"], v["totev_submit"],
             v["totday_sim"], v["totday_rec"], v["totGB_sim"], v["totGB_rec"],
             v["totGB_dst"], v["whizardinfo"], pids[:-1], row ]

    for col in range(0, len(values)):
       sheet2.write(row, col, values[col])
 
    row += 1

    if v["group"] not in groupsum:
      groupsum[v["group"]] = {}
      groupsum[v["group"]]["group"] = v["group"]
      for i in range(1, 5):
        groupsum[v["group"]][grkeys[i]] = 0
      for i in range(5, 10):
        groupsum[v["group"]][grkeys[i]] = 0.0

    for ksum in grkeys:
      if ksum != "group":
        groupsum[v["group"]][ksum] += v[ksum]     

  # sheet3 : Summary by group
  sheet3 = abook.add_worksheet("groupsum")
  sheet3.set_column('A:A',5.0)
  sheet3.set_column('E:E',14.0)
  sheet3.set_column('F:J',9.0, format_2digit)
  row = 0
  for col in range(0, len(grkeys)):
    sheet3.write(row, col, grkeys[col])
  for gr, v in groupsum.items():
    row += 1
    sheet3.write(row, 0, v["group"])
    for col in range(1, len(grkeys)):
      sheet3.write(row, col, v[grkeys[col]])

  rowlast = row+1
  rowtotal = row+3
  sheet3.write("A"+str(rowtotal), "Total")
  for col in "B C D E F G H I J".split(' '):
    sheet3.write(col+str(rowtotal), "=sum("+col+"2:"+col+str(rowlast)+")")

  rowall = rowtotal+2
  sheet3.write("B"+str(rowall), "2 sim + 2 rec")
  sheet3.write("F"+str(rowall), "CPU days")
  sheet3.write("G"+str(rowall), "=2*(F"+str(rowtotal)+"+G"+str(rowtotal)+")")
  sheet3.write("H"+str(rowall), "Storage TB")
  sheet3.write("I"+str(rowall), "=(2*H"+str(rowtotal)+"+2*I"+str(rowtotal)+"+2*J"+str(rowtotal)+")/1000.0")

  rowall += 1
  sheet3.write("B"+str(rowall), "2 sim + 4 rec")
  sheet3.write("F"+str(rowall), "CPU days")
  sheet3.write("G"+str(rowall), "=2*F"+str(rowtotal)+"+4*G"+str(rowtotal))
  sheet3.write("H"+str(rowall), "Storage TB")
  sheet3.write("I"+str(rowall), "=(2*H"+str(rowtotal)+"+4*I"+str(rowtotal)+"+4*J"+str(rowtotal)+")/1000.0")

  rowall += 1
  sheet3.write("B"+str(rowall), "2 sim + 6 rec")
  sheet3.write("F"+str(rowall), "CPU days")
  sheet3.write("G"+str(rowall), "=2*F"+str(rowtotal)+"+6*G"+str(rowtotal))
  sheet3.write("H"+str(rowall), "Storage TB")
  sheet3.write("I"+str(rowall), "=(2*H"+str(rowtotal)+"+6*I"+str(rowtotal)+"+6*J"+str(rowtotal)+")/1000.0")

########################################################################
def write_xlsx(prodpara, excelbook):
  # Write prodpara into excel xlsx file.
  # Format
  # process_group, process_type, process_id, process_name, pol, genname, totev gen., totev plan, totev submit,
  #   nbevts_perjob, nbjobs,
  #   jobhour_sim, jobhour_rec, jobMB_sim, jobMB_rec,
  #   totday_sim, totday_rec,
  #   totGB_sim, totGB_rec, totGB_dst,
  #   xsect, intlumi, nbfile_genuse,
  # filename, filedir, evsec, evMB
  # import xlsxwriter

  sheet1 = excelbook.add_worksheet("byProcID")

  format_2digit = excelbook.add_format()
  format_2digit.set_num_format('0.00')

  sheet1.set_column('A:A',8.0)
  sheet1.set_column('B:B',20.0)
  sheet1.set_column('C:C',8.0)
  sheet1.set_column('D:D',20.0)
  sheet1.set_column('E:E',8.0)
  sheet1.set_column('F:F',10.0)
  sheet1.set_column('G:G',30.0)
  sheet1.set_column('H:H',8.0)
  sheet1.set_column('I:I',12.0)
  sheet1.set_column('J:O',8.0)
  sheet1.set_column('P:AA',9.0, format_2digit)
  sheet1.set_column('AB:AB',60.0)
  sheet1.set_column('AC:AC',50.0)
  sheet1.set_column('AD:AI',9.0, format_2digit)

  atable = []

  varname = ["group", "subgroup", "evtclass", "evttype", "process_id", "process_name",
             "process_type", "pol", "genname", "totev_gen", "totev_plan", "totev_submit", 
             "nbevts_perjob", "nbjobs", "jobhour_sim", "jobhour_rec", "jobMB_sim",
             "jobMB_rec", "jobMB_dst", "totday_sim", "totday_rec", "totGB_sim", 
             "totGB_rec", "totGB_dst", "xsect", "intlumi", "nbfile_genuse", 
             "filename", "filedir",
             "evsec_sim","evsec_rec","evMB_sim","evMB_rec", "evMB_dst"]

  for ic in range(0, len(varname)):
    sheet1.write(0,ic,varname[ic])

  # beam type to be used for VTX smearing. L and R are equivalent to W
  # Different beam type combination can not be produced in one production.
  beamtype = {"L":"W", "R":"W", "W":"W", "B":"B", "0":"0"} 

  row = 1
  for grp, grdata in prodpara.items():
    for prtype, prdata  in grdata.items():
      # print "==== %s === %s ====================" % (grp, prtype)
      for pid, da in prdata.items():
        # process_group, process_type, process_id,
        job = da["job"]
        prod = da["prod"]
        genmeta = da["genmeta"]
        # process_name, pol, genname, totev gen.,
        genname = "none"
        if "{" in genmeta["file_names"] and ".." in genmeta["file_names"] and "}" in genmeta["file_names"]:
          filenames = list(braceexpand.braceexpand(genmeta["file_names"]))
        else:
          filenames = genmeta["file_names"].split(';')
        for meta in filenames[0].split('.'):
          if meta[0:1] == 'G':
            genname = meta[1:]
        if genname == "whizard-1":
          genname = "whizard-1.95"
        polstr = "e%s.p%s" % ( genmeta["polarization1"], genmeta["polarization2"])
        beampoltype = beamtype[genmeta["polarization1"]]+beamtype[genmeta["polarization2"]]

        processname = genmeta["process_name"] if "process_name" in genmeta else genmeta["process_names"]
        if float(genmeta["cross_section_in_fb"]) > 0.0: 
            intlumi = float(prod["nbevents"])/float(genmeta["cross_section_in_fb"])
        else:
            intlumi = 0.0

        subgroup = prtype + ".b"+beampoltype
        modprtype = prtype
        fileurl = genmeta["fileurl"].replace('lfn:/grid','') 
        processtype = genmeta["process_type"]
        evttype = processtype
        if grp == "higgs":
          modprtype = "higgs_ffh"
          subgroup = modprtype + ".b"+beampoltype
          if genmeta["fileurl"] == "":
             fileurl = "/ilc/prod/ilc/mc-dbd/generated/500-TDR_ws/higgs"
          else:
             fileurl = genmeta["fileurl"].replace("lfn:/grid","").replace("generated/-TDR_ws",
                    "generated/%d-TDR_ws" % int(genmeta["CM_energy_in_GeV"])) 
          # print fileurl
        elif grp in ["aa_4f", "3f", "5f"]:
          subgroup = grp + ".b"+beampoltype
          evttype = grp

        elif grp == "single":
          modprtype = "single"
          subgroup = modprtype + ".b"+beampoltype


# Fill values of this row.

        fileurl1 = fileurl.split(':')[1] if ':' in fileurl else fileurl
        fileurl2 = fileurl1[:-1] if fileurl1.endswith('/') else fileurl1
        evtclass = fileurl2.split('/')[-1]

        if [ _debug ]:
          print evtclass+":"+str(pid)+":"+str(processname),
          print " #TotEv="+str(prod["nbevents"])+" #ev/job="+str(job["nbevjob"]),
          print " #Jobs="+str(prod["nbjobs"]),
          print " simcpu="+str(job["cpu"]["sim"])+" simMB="+str(job["size"]["sim"]),
          print " recMB="+str(job["size"]["rec"])
         
        values = [ grp, subgroup, evtclass, evttype, pid, processname, processtype, 
                   polstr, genname, 
                   int(genmeta["total_number_of_events"]),
                   prod["nevreq"], prod["nbevents"],
                   job["nbevjob"], prod["nbjobs"],
                   job["cpu"]["sim"]/3600.0, 
                   job["cpu"]["rec"]/3600.0,
                   job["size"]["sim"],
                   job["size"]["rec"], 
                   job["size"]["dst"],
                   prod["cpusec"]["sim"]/3600.0/24.0, 
                   prod["cpusec"]["rec"]/3600.0/24.0,
                   prod["sizeMB"]["sim"]*0.001, 
                   prod["sizeMB"]["rec"]*0.001, 
                   prod["sizeMB"]["dst"]*0.001,
                   float(genmeta["cross_section_in_fb"]), 
                   intlumi, 
                   prod["nbfiles"],
                   filenames[0], 
                   fileurl,
                   job["evsec"]["sim"],
                   job["evsec"]["rec"],
                   job["evMB"]["sim"],
                   job["evMB"]["rec"],
                   job["evMB"]["dst"]
                 ]

        col = 0
        arec = {}
        for var in values:
          sheet1.write(row, col, var)
          arec[varname[col]] = var
          col += 1

        atable.append(arec)
        row += 1

  return atable
  # book.close()


# ###########################################################
def cr_prgroup(asheet):
  ''' Sum up rows if same subgroup and create information for summary sheet '''

  prgrp = {}
  for line in asheet:
    akey = line["subgroup"]
    if akey in prgrp:
      prgrp[akey].append(line)
    else:
      prgrp[akey] = [line]

  sumgrp = {}
  for key, lines in prgrp.items():
    # print key + " " + str(len(lines))
    suminfo = {"totday_sim":0.0, "totday_rec":0.0, "totGB_sim":0.0,
               "totGB_rec":0.0, "totGB_dst":0.0, "nbjobs":0, "nbprocid":0, 
               "totev_submit":0 }
    sumkeys = suminfo.keys()
    suminfo["group"] = lines[0]["group"]
    # print(" group="+suminfo["group"])
    # suminfo["nbevjob"] = lines[0]["nbevts_perjob"]
    suminfo["nbevjob"] = "void"
    suminfo["process_type"] = lines[0]["process_type"]
    suminfo["whizardinfo"] = lines[0]["genname"]
    filesum = {}
    nballfile = 0
    for line in lines:
      # print(" Processing "+line["process_type"])
      if line["process_type"] != suminfo["process_type"]:
          print pyc.c["blue"]+"WARNING : " + line["process_type"] + " ",
          print line["process_id"] + pyc.c["end"], 
          print " is included in summary of "+suminfo["group"] + " ",
          print suminfo["process_type"]
          
#          print line["process_type"] + "  " + line["process_id"] + \
#                " is not same as the first one, " + suminfo["process_type"] 
#          print pyc.c["blue"]+"WARNING : process_type "+pyc.c["end"],
#          print line["process_type"] + "  " + line["process_id"] + \
#                " is not same as the first one, " + suminfo["process_type"] 
#          print "Probably wrong subgroup was used for summary of " + suminfo["group"]
#          exit(-1)

      afile = {"procid":line["process_id"], "dirname":line["filedir"],
               "filename":line["filename"], "nbgenfiles":int(line["nbfile_genuse"])}
      filesum[line["process_id"]] = afile
      nballfile += int(line["nbfile_genuse"])
      for sumkey in sumkeys:
        if sumkey != "nbprocid":
          suminfo[sumkey] += line[sumkey]
        else:
          suminfo["nbprocid"] += 1
    suminfo["filesum"] = filesum
    suminfo["total_nb_inputs"] = nballfile

    sumgrp[key] = suminfo

  return sumgrp



# ###################################################################################
def print_prodpara(prodpara):
  # Print prodpara
  # Format
  # process_group, process_type, process_id, process_name, pol, genname, totev gen., totev plan, totev submit, 
  #   nbevts_perjob, nbjobs,
  #   jobhour_sim, jobhour_rec, jobMB_sim, jobMB_rec,
  #   totday_sim, totday_rec,
  #   totGB_sim, totGB_rec, totGB_dst, 
  #   xsect, intlumi, nbfile_genuse, 
  # filename, filedir, evsec, evMB
  
  origout = sys.stdout
  sys.stdout = open("prodpara.csv", "w")

  print "group\tprocess_type\tprocess_id\tprocess_name\tpol\tgenname",
  print "\ttotev_gen\ttotev_plan\ttotev_submit\tnbevts_perjob\tnbjobs",
  print "\tjobhour_sim\tjobour_rec\tjobMB_sim\tjobMB_rec\tjobMB_dst",
  print "\ttotday_sim\ttotday_rec\ttotGB_sim\ttotGB_rec\ttotGB_dst",
  print "\txsect\tintlumi\tnbfile_genuse",
  print "\tfilename\tfiledir\tevsec\tevMB"

  
  for grp, grdata in prodpara.items():
    for prtype, prdata  in grdata.items():
      print "==== %s === %s ====================" % (grp, prtype)
      for pid, da in prdata.items():
        process_group, process_type, process_id, 
        print "%s\t%s\t%s" % (grp, prtype, pid), 
        job = da["job"]
        prod = da["prod"]
        genmeta = da["genmeta"]
        # process_name, pol, genname, totev gen., 
        genname = "none"

        if "{" in genmeta["file_names"] and ".." in genmeta["file_names"] and "}" in genmeta["file_names"]:
           filenames = list(braceexpand.braceexpand(genmeta["file_names"]))
        else:
           filenames = genmeta["file_names"].split(';')
        for meta in filenames[0]:
          if meta[0:1] == 'G':
            genname = meta[1:]
        if genname == "whizard-1":
          genname = "whizard-1_95"
        polstr = "e%s.p%s" % ( genmeta["polarization1"], genmeta["polarization2"])
        processname = genmeta["process_name"] if "process_name" in genmeta else genmeta["process_names"]
        print "\t%s\t%s\t%s\t%s" % ( processname, polstr, genname, genmeta["total_number_of_events"]),
        # totev plan, totev submit, 
        print "\t%s\t%s" % ( prod["nevreq"], prod["nbevents"]),
        #   nbevts_perjob, nbjobs,
        print "\t%s\t%s" % ( job["nbevjob"], prod["nbjobs"]),

        # jobhour_sim, jobhour_rec, jobMB_sim, jobMB_rec,
        print "\t%5.2f\t%5.2f\t%7.1f\t%7.1f\t%7.3f" % ( job["cpu"]["sim"]/3600.0, job["cpu"]["rec"]/3600.0, 
              job["size"]["sim"],job["size"]["rec"], job["size"]["dst"]),
        #   totday_sim, totday_rec,
        print "\t%5.2f\t%5.2f" % ( prod["cpusec"]["sim"]/3600.0/24.0, prod["cpusec"]["rec"]/3600.0/24.0 ),
        #   totGB_sim, totGB_rec, totGB_dst, 
	print "\t%6.1f\t%6.1f\t%6.1f" % ( prod["sizeMB"]["sim"]*0.001, prod["sizeMB"]["rec"]*0.001, prod["sizeMB"]["dst"]*0.001),
        #   xsect, intlumi, nbfile_genuse, file_names, file_url, evsec, evMB
        intlumi = float(prod["nbevents"])/float(genmeta["cross_section_in_fb"])
        print "\t%s\t%s\t%d" % (genmeta["cross_section_in_fb"], intlumi, prod["nbfiles"]),
            
        # file_names, file_url, evsec, evMB
        print "\t%s\t%s\t%s\t%s" % ( filenames[0], genmeta["fileurl"].replace('lfn:/grid',''), 
                      str(job["evsec"]), str(job["evMB"]))
  sys.stdout = origout

# #########################################################
if __name__ == "__main__":
  evttype = ["6f", "higgs",
             "2f", "4f", "5f", "aa_4f", "uds", "single", "flavortag"]

  helpmsgs = ["Create an excel file, prodpara.xlsx, for production parameters.",
              "Usage:",
              "./prodpara.py  6f,higgs,2f,4f,5f,aa_4f,uds,flavortag "]
  parser = argparse.ArgumentParser(
      prog = "prodpara.py",
      formatter_class = argparse.RawTextHelpFormatter, description="\n".join(helpmsgs) )

  parser.add_argument("evttypes",help="comma separated list of evttype to be created.")
  parser.add_argument("--evtclass", help="\n".join(["EvtClass which is used for production disregarding process_type of generator type",
                      "Number of evtclass should be same evttype each being separated by \",\""]),
                      dest="evtclass", action="store", default="")
  parser.add_argument("-s", "--size",
      help="max size allowed in MB for files produced by production. Default=%s" % str(_maxsize), 
      dest="MAXSIZE", action="store", default=str(_maxsize))
  parser.add_argument("-c", "--cpu", 
      help="max cpu time allowed in sec for production jobs. Default=%s" % str(_maxcpu), 
      dest="MAXCPU", action="store", default=str(_maxcpu))
  parser.add_argument("-d", help="print debug message", dest="DEBUG", action="store_true", default=False)
  parser.add_argument("--genmeta_json", 
      help="\n".join(["genmetaByID json file name. Default is taken from GENMETA_JSON env. parameter.", 
                      "GENMETA_JSON=%s" % _genmetaByID_json]), 
                      dest="GENMETA_JSON", action="store",default=_genmetaByID_json)
  parser.add_argument("--cpusize_json", help="cpusize json file name. Default=%s" % _cpusize_json,
                      dest="CPUSIZE_JSON", action="store", default=_cpusize_json)
  parser.add_argument("--nevgen_json", 
         help="nevgen json file name ( containing # of events to produce for each process ID\nDefault=%s" % _nevgen_json, 
                      dest="NEVGEN_JSON", action="store", default=_nevgen_json)
  helparg = ["If given, whole process_type value is used for process grouping",
             "Otherwize, string before first \"_\" is used if first 3 characters of ",
             "process type is \"np-\"" ] 

  parser.add_argument("--no_np_process_type", help="\n".join(helparg), dest="no_np_process_type", action="store_true", default=False)
  parser.add_argument("--no_lepton_aware", help="group separetely if nb. of leptons in the process is different.", 
                          dest="no_lepton_aware", action="store_true", default=False)
  parser.add_argument("--omit_process_id_file", help="Process ID's which are omitted from output excel file.",
                          dest="omit_process_id", action="store", default="omit_process_id.txt")
  parser.add_argument("-f", "--prodpara_xlsx", help="Output excel file name. Default=prodpara.xlsx", dest="prodpara_xlsx", action="store", default="prodpara.xlsx")

  #
  args = parser.parse_args()
  _maxsize = float(args.MAXSIZE)
  _maxcpu = float(args.MAXCPU)
  _debug = args.DEBUG
  _cpusize = json.load(open(args.CPUSIZE_JSON))
  _genmeta = json.load(open(args.GENMETA_JSON))
  _nevgen = json.load(open(args.NEVGEN_JSON))
  print "Following parameters are used for deciding number of events of each process ID."
  print "maxsize = " + str(_maxsize) + " MB/file"
  print "maxcpu  = " + str(_maxcpu) + " sec/job"

  prodpara_xlsx = args.prodpara_xlsx

  evttypes = args.evttypes
  if len(evttypes) <= 0:
      print "ERROR: evttypes is not given.  Provided it using -t option."
      exit(0)
  evttype = evttypes.split(',')

  evtclass = []
  if args.evtclass != "":
     evtclass = args.evtclass.split(",")
     if len(evtclass) != len(evttype):
        print "ERROR: --evtclass argument exists, but Nb of evtclass("+str(len(evtclass)),
        print ") specified is not same as evttype(" + str(len(evttype)) + ")."
        exit(0)

  _options={"no_np_process_type":args.no_np_process_type, 
            "no_lepton_aware":args.no_lepton_aware}

  _omit_process_id = omit_process_id(args.omit_process_id)

  prodpara = {}
  for ip in range(0, len(evttype)):
    grp = evttype[ip]
    print "### " + grp,
    _options["evtclass"] = "" if len(evtclass) == 0 else evtclass[ip]
    if len(evtclass) == 0:
        print ""
    else:
        grouping_options["evtclass"] = evtclass[ip]
        print " as evtclass " + grouping_options["evtclass"]
    res = collect_by_process_type(grp)
    # pprint.pprint(res)
    prodpara[grp] = res


  # print_prodpara(prodpara)
  book = xlsxwriter.Workbook(prodpara_xlsx)
  asheet = write_xlsx(prodpara, book)

  sumgrp = cr_prgroup(asheet)
  # pprint.pprint(sumgrp)  
  write_sumgrp_sheet(sumgrp, book)

  book.close()
  # pprint.pprint(asheet)

  # json.dump(prodpara, open("prodpara.json","w"))

