#!/bin/bash 
#
# Make a rundir to run ddsim from an input file.
#

if [ "x${DIRAC}" == "x" ] ; then 
  echo "Initialize ILCDirac environment first."
  exit 127
fi

templ="`pwd`/runddsim.templ"
inputfiles="bkgfiles.list"

mkdir -p jobs
rm -f suball.sh

declare -A polconv
polconv=( ["L"]="W"  ["R"]="W" ["W"]="W" ["B"]="B" )
declare -A Offset
ZOffset=( ["WW"]=0.0  ["WB"]=-0.0352 ["BW"]=0.0357 ["BB"]=0.0 )
ZSmear=( ["WW"]=0.1468 ["WB"]=0.1389 ["BW"]=0.1394 ["BB"]=0.1260 )


cat ${inputfiles} | while read f ; do 
  bsname=`basename $f`
  ene=`getMetaFromFilename $bsname E`
  proc=`getMetaFromFilename $bsname P`
  epol=`getMetaFromFilename $bsname e`
  ppol=`getMetaFromFilename $bsname p`
  pid=`getMetaFromFilename $bsname I`
  nser=`getMetaFromFilename $bsname n`
  bmtype=${polconv[$epol]}${polconf[$ppol]}
  zoffset=${ZOffset[$bmtype]}
  zsmear=${ZSmear[$bmtype]}

  for detector in "ILD_l5_v02" "ILD_s5_v02" ; do 

    outdir="jobs/${ene}/${detector}/I${pid}"
    mkdir -p ${outdir}
    outfile="sv02-00-01.m${detector}.E${ene}.I${pid}.P${proc}.e${epol}.p${ppol}.n${nser}_1.d_sim_000.slcio"
  
    sed -e "s|%%INPUT_FILE%%|$f|" \
       -e "s|%%OUTPUT_FILE%%|${outfile}|" \
       -e "s|%%IP_SIGMA%%|${zsmear}|g" \
       -e "s|%%DETECTOR%%|${detector}|g" \
       -e "s|%%IP_OFFSET%%|${zoffset}|g" ${templ} > \
       ${outdir}/runddsim.sh 
    chmod +x ${outdir}/runddsim.sh 
  
    echo "bsub -o runddsim.log -J I${pid} \"( ./runddsim.sh > runddsim.log 2>&1 )\" " > ${outdir}/bsub.sh 
  
    echo "( cd ${outdir} && . bsub.sh )" >> suball.sh 
  
  done
done


