#
# Create directory structure and files for dummy background file.

# 91-nobeam with 250-SetA beam BG
#newecm=91-nobeam
#orgecm=250-SetA
#ildconf=v02-02

# 550-Test with 500-TDR_ws beam BG
newecm=550-Test
orgecm=500-TDR_ws
ildconf=v02-02-03

detector=ILD_l5_v02

srcdir="/group/ilc/grid/storm/prod/ilc/mc-2020/ild/sim/${orgecm}"

for d in aa_lowpt_BB  aa_lowpt_BW  aa_lowpt_WB  aa_lowpt_WW  seeablepairs; do
  (
  tdir="${newecm}/$d/${detector}/${ildconf}"
  mkdir -pv ${tdir}
   (
    cd ${tdir}
    origfile=`/bin/ls ${srcdir}/$d/${detector}/${ildconf}/*/000/*.slcio | head -1`
    ln -s ${origfile} .
    srcfile=`basename ${origfile}`
    newfile=`echo ${srcfile} | sed -e "s/E${orgecm}/E${newecm}/"`
#    if [ ${d} != "seeablepairs" ] ; then
#      polmain=${d/aa_lowpt_/}
#      oldpol=e${polmain:0:1}.p${polmain:1:1}
#      newfile=`echo ${newfile} | sed -e "s|${oldpol}|e0.p0|"`
#    fi
    cp -v ${srcfile} ${newfile}
   )
  )
done
