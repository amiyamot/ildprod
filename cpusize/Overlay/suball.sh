
#./runsimrec.py --dolist stdhep_lists/500-TDR_ws.4f.list --numevents 50 --outdir jobs-4f 2>&1 | tee 4f.log
#./runsimrec.py --dolist stdhep_lists/500-TDR_ws.6f.list --numevents 50 --outdir jobs-6f 2>&1 | tee 6f.log
# ./runsimrec.py --dolist stdhep_lists/500-TDR_ws.235f.list --numevents 50 --outdir jobs-235f 2>&1 | tee 235f.log
#./runsimrec.py --dolist stdhep_lists/500-TDR_ws.1f.list --numevents 50 --outdir jobs-1f 2>&1 | tee 1f.log
#./runsimrec.py --dolist stdhep_lists/500-TDR_ws.higgs.list --numevents 50 --outdir jobs-higgs 2>&1 | tee higgs.log

# ./runsimrec.py --dolist stdhep_lists/500-TDR_ws.higgs_XX.list --numevents 50 --outdir jobs-higgs_XX 2>&1 | tee higgs_XX.log

# ./runsimrec.py --dolist stdhep_lists/500-TDR_ws.ng.list --numevents 50 --outdir jobs-ng 2>&1 | tee ng.log
# ./runsimrec.py --dolist stdhep_lists/500-TDR_ws.4f_lowmee.list --numevents 50 --outdir jobs-4f_lowmee 2>&1 | tee 4f_lowmee.log

#./runsimrec.py --dolist stdhep_lists/500-TDR_ws.flavortag.list --numevents 50 --outdir jobs-flavortag 2>&1 | tee flavortag.log
#./runsimrec.py --dolist stdhep_lists/1-calib.uds.list --numevents 50 --outdir jobs-uds 2>&1 | tee uds.log
#./runsimrec.py --dolist stdhep_lists/1-calib.single.list --numevents 50 --outdir jobs-single 2>&1 | tee singles.log

#./runsimrec.py --dolist stdhep_lists/1000-B1b_ws.6f_vvqqqq.list --numevents 50 --outdir jobs1000-6f_vvqqqq 2>&1 | tee 1000-6f_vvqqqq.log

# evtclass=np-susy-higgsinos-verylowdm
# enepara=500-TDR_ws

#evtclass=2f
#enepara=250-TDR_ws
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

#evtclass=4f
#enepara=250-TDR_ws
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

#enepara=250-TDR_ws
#for evtclass in 1f3f aa; do
#    ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log
#done

# evtclass=2f_highM
# enepara=250-TDR_ws
# ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

# evtclass=flavortag
# enepara=250-SetA
# ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

# evtclass=3f
# enepara=250-SetA
# ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}-lowX.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

enepara=250-SetA
#for evtclass in 4f 5f aa_4f ; do 
#  ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}-lowX.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log
#done

#evtclass=flavortag-4f
#enepara=250-SetA
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

#evtclass=flavortag
#enepara=91-nobeam
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

#evtclass=higgs
#enepara=250-SetA
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

enepara=250-SetA
#for evtclass in 3f 4f aa_4f ; do 
#  ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}-midX.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log
#done

#evtclass=4f
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}-highX.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}-highX.log

#evtclass=2f
#./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log

enepara=250-SetA
# for evtclass in higgs-excl SM-rest; do
#     ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log
# 
# done

enepara=500-TDR_ws
#for evtclass in hh; do
#     ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log
# 
#done
for evtclass in 2f.disk 4f.disk; do
     ./runsimrec.py --dolist stdhep_lists/${enepara}.${evtclass}.list --numevents 50 --outdir jobs-${evtclass} 2>&1 | tee ${enepara}.${evtclass}.log
 
done
