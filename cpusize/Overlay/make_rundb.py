#!/bin/env python

import json
import glob
import subprocess
import pprint
import os
import sys

DATADIR = {"250-SetA": "/group/ilc/grid/storm/prod/ilc/mc-2020/ild/sim",
           "91-nobeam": "/group/ilc/users/miyamoto/mcprod/201006-cpusize/cpusize/BGSim",
           "250-TDR_ws": "/group/ilc/users/miyamoto/mcprod/200915-cpusize/cpusize/BGSim",
           "500-TDR_ws": "/hsm/ilc/grid/storm/prod/ilc/mc-opt-3/ild/sim",
           "550-Test"  : "/group/ilc/users/ono/ILDProd_2020/cpusize/BGSim",
           "1000-B1b_ws":"/group/ilc/users/miyamoto/mcprod/180620-cpusize/cpusize/BGSim/runddsim/jobs"}

DATADIR["250-SetA"]   = "/group/ilc/grid/storm/prod/ilc/mc-2020/ild/sim"
DATADIR["500-TDR_ws"] = "/group/ilc/grid/storm/prod/ilc/mc-2020/ild/sim"

# _BGFILES=None
if sys.version_info.major == 2:
    execfile("version.py")
    execfile("mcprod_config.py")
else:
    exec(open("version.py").read())
    exec(open("mcprod_config.py").read())

# ###################################################################
def bkgfiles(ildconfig, detector, enepara, evttype, polstr, simdata_dir=""):
  # /hsm/ilc/grid/storm/prod/ilc/mc-opt/ild/sim/500-TDR_ws/aa_lowpt_BB/ILD_l5_v02/v01-19-05-p01/00009089/000/

  dirsearch = simdata_dir + "/%s" % str(enepara)

  filesearch = "s%s.m%s.E%s.I*%s.%s.*.d_sim_*.slcio " % \
             ( ildconfig, detector, enepara, evttype, polstr )
  cmd = "find %s -type f -name %s -print" % ( dirsearch, filesearch )

  print cmd
  cmdout = subprocess.check_output(cmd, shell=True, stderr = subprocess.STDOUT).split('\n')
  return cmdout

# ###################################################################
def vtxsmear(beampara):
   
  # pprint.pprint(db)

  if not os.path.exists("mcprod_config.py"):
     print("A file, mcprod_config.py, not found")
     print("Create a symbolic link to ${MCPROD_TOP}/mcprod_config.py in this directory")
     exist(16)


  # MCProd_ZOffset_And_ZSigma
  print("IP ZOffset and sigma for 250-TDR_ws are same as 250-SetA")
  MCProd_ZOffset_And_ZSigma["250-TDR_ws"] = MCProd_ZOffset_And_ZSigma["250-SetA"]

  if beampara not in MCProd_ZOffset_And_ZSigma:
      print "ERROR : vtx smear parameters for beam %s is not defined." % beampara
      exit(16)

  vsmear = MCProd_ZOffset_And_ZSigma[beampara]
  
  return vsmear


# ###################################################################
if __name__ == "__main__":
  runcond = {}
  bktype = [ [ "aaddhad", ["eW.pW", "eW.pB", "eB.pW", "eB.pB"] ], 
             [ "seeablepairs", ["e0.p0"] ] ]  
  ildconfig = ILDCONFIG_FOR_OVERLAY
    
  datadir = DATADIR

  for enepara in ["250-TDR_ws", "250-SetA", "500-TDR_ws", "550-Test", "1000-B1b_ws", "91-nobeam"]:
      runcond[enepara] = {}
      runcond[enepara]["vtx_smear"] = vtxsmear(enepara)
      db = {}
      (energy, mpara ) = enepara.split('-')

      # if "91-nobeam" in enepara:
      #   bktype[0][0] = ["e0.p0", "e0.p0", "e0.p0", "e0.p0"]
      detlist = ["ILD_l5_v02", "ILD_s5_v02"]
      if enepara in ["250-TDR_ws", "91-nobeam"]:
         detlist = ["ILD_l5_v02"]      

      for det in detlist :
        
          for bgproc in bktype:
              procname = bgproc[0]
              for poltype in bgproc[1]:
                  flist = bkgfiles(ildconfig, det, enepara, procname, poltype, simdata_dir=datadir[enepara])
                  # pprint.pprint(flist)       
  
                  if len(flist) > 0 and "slcio" in flist[0]:
                    arec = {}
                    arec["energy"] = energy
                    arec["ildconfig"] = ildconfig
                    arec["detector"] = det
                    arec["procname"] = procname
                    arec["poltype"] = poltype
                    arec["files"] = [flist[0]]
                    key = "%s.%s.%s.%s.%s" % ( enepara, ildconfig, det, procname, poltype)
         
                    db[key] = arec
       
      runcond[enepara]["param"] = db 
  
  json.dump(runcond, open("runcond.json","w"))


