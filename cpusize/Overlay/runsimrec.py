#!/usr/bin/env python 
# 
#  Run ddsim and marlin using python 
#

import os
import sys
import subprocess
import datetime
import pprint
import glob
import argparse
import json

if sys.version_info.major == 2:
    execfile("version.py")
else:
    exec(open("version.py").read())

def execSimRec(inputs):
  ''' run ddsim and marlin '''
  sysout = open("sysout.log","w",0) # 0 no buffering
  logpref = inputs["logpref"]
  detector = inputs["detector"]
  numevents = inputs["numevents"]
  genpath = inputs["genpath"]
  vsmear = inputs["vsmear"]
  bgfiles = inputs["bgfiles"]
  marlin_detector = inputs["marlindetector"]
  energy = inputs["energy"]


  os.environ["MARLIN_DLL"] = ""
  softdir = SOFTDIR
  version = SOFT_VERSION
  init_ilcsoft = "/".join([softdir, version, "init_ilcsoft.sh"])
  ildconfig_version = ILDCONFIG_SIM
  ildconfig = "/".join([ILDCONFIG_DIR, ildconfig_version, "StandardConfig/production"])
  my_marlin_stdreco = ildconfig + "/MarlinStdReco.xml"      

  lcgeo_DIR = subprocess.check_output(". "+init_ilcsoft+" 2>&1 > init_ilcsoft.log && echo ${lcgeo_DIR}", 
              shell=True, stderr=subprocess.STDOUT)
  compact_file = "/".join([lcgeo_DIR[:-1], "ILD/compact", detector, detector+".xml"])

  genfile = os.path.basename(genpath)
  (genpref, gentype) = genfile.rsplit(".",1)
  simfile = ".".join(["s"+ILDCONFIG_SIM, "m"+detector, genpref, "slcio"])
  mdetector = marlin_detector
  machinePara = ""
  for token in genpref.split("."):
      if token[0:1] == "E":
         (enekey, machinekey) = token[1:].split("-",1)
         machinePara = machinekey

  if bgfiles == "":
     mdetector += "_nobg"
  recfile = ".".join(["r"+ILDCONFIG_REC, "s"+ILDCONFIG_SIM, "m"+mdetector, genpref, "d_rec", "slcio"])
  dstfile = ".".join(["r"+ILDCONFIG_REC, "s"+ILDCONFIG_SIM, "m"+mdetector, genpref, "d_dst", "slcio"])

  ddsim_run = " ".join(["/usr/bin/time -o time-ddsim.log ", 
		 "--format='### ddsim time : elapsed %e sec, user %U sec, system %S sec.' ", 
		 "ddsim", "--inputFiles", genpath, 
                 "--outputFile", simfile, "--numberOfEvents", str(numevents), 
                 vsmear,
                 "--compactFile", compact_file, "--steeringFile", ildconfig+"/ddsim_steer.py"])
  if "calib" in machinePara:
      ddsim_run += " --crossingAngleBoost 0.0 "
  ddsim_exec = " ".join([". " + init_ilcsoft, "&&", ddsim_run, 
                         " > "+logpref+"_ddsim.log 2>&1 "])
  print >> sysout, "### ddsim_exec command :"
  print >> sysout, ddsim_exec
  print >> sysout, "### ddsim_exec starts : "+str(datetime.datetime.now())
  ret = subprocess.call(ddsim_exec, shell=True)
  print >> sysout, "### ddsim_exec ends ret="+str(ret) + " : "+str(datetime.datetime.now())
  if ret != 0:
    print >> sysout, "ddsim_exec ends with non 0 return code."
    exit(2)


  marlin_run = " ".join(["/usr/bin/time -o time-marlin.log",
	 "--format='### marlin time : elapsed %e sec, user %U sec, system %S sec.' ", 
	 "Marlin", my_marlin_stdreco,
         "--constant.lcgeo_DIR=%s" % lcgeo_DIR[:-1], 
         "--constant.DetectorModel=%s" % marlin_detector,
         "--global.LCIOInputFiles="+simfile,
         "--MyLCIOOutputProcessor.LCIOOutputFile="+recfile,
         "--DSTOutput.LCIOOutputFile="+dstfile] )
  if bgfiles != "":
    bgdata = [ " --constant.RunOverlay=true "]
    if int(energy) != 91:
      bgdata.append(" --constant.CMSEnergy=%s " % energy )
    for bgkey, bgname in bgfiles.items():
      bgdata.append(" --%s.InputFileNames=%s " % ( bgkey, bgname ) )
    marlin_run += " ".join(bgdata)
   
  if "calib" in machinePara or "nobeam" in machinePara:
    marlin_run += " --constant.RunBeamCalReco=false "

  marlin_exec = " ".join([". "+init_ilcsoft, "&&", marlin_run, 
                          " > "+logpref+"_marlin.log 2>&1 "])
   
  print >> sysout, "### marlin_exec command :"
  print >> sysout, marlin_exec
  print >> sysout, "### marlin_exec starts : "+str(datetime.datetime.now())
  ret = subprocess.call(marlin_exec, shell=True )
  print >> sysout, "### marlin_exec ends ret="+str(ret) + " : "+str(datetime.datetime.now())
  if ret != 0:
    print >> sysout, "marlin_exec ends with non 0 return code."
    exit(1)

# =======================================================================================
def get_genProcessID(genfile):
  for key in genfile.split('.'):
    if key[0:1] == 'I':
      genprocid=key[1:]
      return genprocid
  return 'No genProcID key found'

# =======================================================================================
def do_one_file(genpath, outdir="jobs", detector="ILD_l5_v02", numevents=3, 
                genmeta="genmetaByID.json", chdir=True, recopt="o1"):
  import json
    
  dirname=os.path.dirname(genpath)
  genfile=os.path.basename(genpath)  
  genprocid = get_genProcessID(genfile)
  (metakey, nseq, ftype) = genfile.rsplit('.',2)
  mkeys = {}
  for key in metakey.split('.'):
    mkeys[key[0:1]] = key[1:]
 
  enepara = mkeys["E"][1:] if mkeys["E"][0:1] == "0" else mkeys["E"]
  print(" mkeys="+mkeys["E"]+" enepara="+enepara)
  ( strenergy, machinePara )  = mkeys["E"].split('-')
  energy = str(int(strenergy))
  runcond_all = json.load(open("runcond.json"))
  runcond = runcond_all[enepara]
  procid = str(int(mkeys['I']))
  metakey = procid

  metainfo = json.load(open(genmeta))
  if procid not in metainfo:
    print  "meta key, %s, not found in %s" % ( metakey, genmeta)
    exit(1)
  xsect = metainfo[metakey]["cross_section_in_fb"]
  total_number_of_events = metainfo[metakey]["total_number_of_events"]
  number_of_files = metainfo[metakey]["number_of_files"]

  curdir = os.getcwd()
  if chdir: 
    # ======== chdir to rundir and do ddsim & marlin
    (temp, energy_dir, proctype) = dirname.rsplit('/',2)
    rundir = '/'.join([outdir, energy_dir, proctype, "I"+str(genprocid)])

    print "rundir is "+rundir
    if not os.path.exists(rundir):
      os.makedirs(rundir)
    os.chdir(rundir)

  # === exec ddsim and marlin
  rundata = { "detector":detector, "numevents":numevents, 
              "logpref":"I"+str(genprocid), "genpath":genpath }

  rundata["energy"] = energy
  if "calib" in machinePara:
      vspara = [ 0.0, 0.0 ]
  elif mkeys["e"] == "0" and mkeys["p"] == "0":
      vspara = [ 0.0, 0.0 ]
  else: 
      vspara = runcond["vtx_smear"][mkeys["e"]+mkeys["p"]]
  rundata["vsmear"] = " --vertexSigma 0.0 0.0 %s 0.0 --vertexOffset 0.0 0.0 %s 0.0 " \
                  % ( vspara[1] , vspara[0] )

  bgkeys = {"BgOverlayWW":["aaddhad","eW.pW"], "BgOverlayWB":["aaddhad","eW.pB"], 
            "BgOverlayBW":["aaddhad", "eB.pW"], "BgOverlayBB":["aaddhad","eB.pB"],
            "PairBgOverlay":["seeablepairs","e0.p0"]}
  bgfiles = {}
  ildconfig4ovl = ILDCONFIG_FOR_OVERLAY
  rundata["marlindetector"] = detector.replace("_v02","_%s_v02" % recopt)
  if "calib" not in machinePara:
    for key, value in bgkeys.items():
      skey = "%s.%s.%s.%s.%s" % ( enepara, ildconfig4ovl, detector, value[0], value[1] )
      bgfile = runcond["param"][skey]["files"][0]
      bgfiles[key] = bgfile
    rundata["bgfiles"] = bgfiles
  else:
    rundata["bgfiles"] = ""


  execSimRec(rundata)  
    
  # ======== analize run result ================
  ddsimtime=open("time-ddsim.log").readlines()[0].split()
  marlintime=open("time-marlin.log").readlines()[0].split()
  
  sim={"elaspsed":ddsimtime[5], "user":ddsimtime[8], "sys":ddsimtime[11]}
  rec={"elaspsed":marlintime[5], "user":marlintime[8], "sys":marlintime[11]}

#  pprint.pprint(sim)
#  pprint.pprint(rec)
  fres = open("resource.log","w")

  print >> fres, "numevents="+str(numevents)
  for key, value in sim.items():
    print >> fres, "ddsim_"+key+"="+value
  for key, value in rec.items():
    print >> fres, "marlin_"+key+"="+value

  for slcio_file in glob.glob("*.slcio"):
    if slcio_file.find("aa_lowpt_-80e-_+30e+_000") < 0 \
    and slcio_file.find("gpigs-filtered-pairs-mod") < 0:
      fsize = os.path.getsize(slcio_file)
      ftype = "sim"
      if slcio_file.find("d_rec") > 0:
        ftype = "rec"
      if slcio_file.find("d_dst") > 0:
        ftype = "dst"

      print >> fres, ftype+"="+str(fsize)

  print >> fres, "xsect="+str(xsect)
  print >> fres, "#events=" + str(total_number_of_events)
  print >> fres, "#files=" + str(number_of_files)
  print >> fres, "metakey="+ metakey

  fres.close()

# completed

  if chdir:
    os.chdir(curdir)


# =======================================================================================
if __name__ == "__main__":

#  genpath="/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/500-TDR_ws/1f/E0500-TDR_ws.Pea_ea.Gwhizard-1.95.eR.pW.I37467.01.stdhep"

#  do_one_file(genpath)

  if sys.version_info[0] == 2 and sys.version_info[1] < 7:
    print "ERROR: Python version greater or equal to 2.7 is required."
    print "Your python version is "+str(sys.version_info)
    exit(15)

  if "GENMETA_JSON" not in os.environ:
    print("FATAL ERROR: GENMETA_JSON environment parameter is not defined.")
    exit(0)
  GENMETA_BY_ID_JSON=os.environ["GENMETA_JSON"]
  
  helpmsg="\n".join(["Do submit and run jobs to estimate CPU time and file size",
        "Examples:",
	"  ./runsimrec.py --dolist stdhep_lists/500-TDR_ws.1f.list --outdir jobs-1f 2>&1 | tee bsub-1f.log",
	"  ./runsimrec.py --dofile sample_generator_file.stdhep --outdir jobs-test ",
        "If --dry is not given, submit job immedieately.",
        "Note:",
        "  500 GeV IP position smearing and overlay of aa_lowpt and selected_pairs included."])

  parser = argparse.ArgumentParser(description=helpmsg, formatter_class=argparse.RawTextHelpFormatter)
  parser.add_argument("--dofile", help="Just do one stdhep file of ddsim and marlin", dest="afile", action="store", default="")
  parser.add_argument("--dolist", help="Sub many jobs for many files.", dest="alist", action="store", default="")
  parser.add_argument("--outdir", help="Top level job exec directory.", dest="outdir", action="store", default="jobs")
  parser.add_argument("--numevents", help="number of events of each jobs.", dest="numevents", action="store", default="50")
  parser.add_argument("--dry", help="just create a bsub script.", dest="dry", action="store_true", default=False)
  parser.add_argument("--recopt", help="Option for reconstruction.", dest="recopt", action="store", default="o1")

  args = parser.parse_args()
  afile = args.afile
  alist = args.alist
  numevents = int(args.numevents)
  outdir = args.outdir
  dryrun = args.dry
  recopt = args.recopt

  print "### runsimrec.py : start "
  print "  afile="+afile
  print "  alist="+alist
  print "  numevents="+str(numevents)
  print "  outdir="+outdir
  print "  dryrun="+str(dryrun)
  print "  recopt="+str(recopt)
  print " "
  

  if alist != "":
    for rgenpath in open(alist): 
      genpath=rgenpath.replace("\n","")
      dirname=os.path.dirname(genpath)
      genfile=os.path.basename(genpath)
      genprocid = get_genProcessID(genfile)
      if ".stdhep" not in genpath and ".slcio" not in genpath:
          print "ERROR: generator file name does not end with .stdhep nor .slcio"
          print "file name was " + genpath
          print "read in from " + alist
          exit(1)

      # print "genpath="+genpath
      # print "genfile="+genfile
      (metakey, nseq, ftype) = genfile.rsplit('.',2)
      # ======== chdir to rundir and do ddsim & marlin
      (temp, energy_dir, proctype) = dirname.rsplit('/',2)
      rundir = '/'.join([outdir, energy_dir, proctype, "I"+str(genprocid)])

      print "rundir is "+rundir+" genfile="+genfile
      curdir = os.getcwd()
      if not os.path.exists(rundir):
        os.makedirs(rundir)
      os.chdir(rundir)

      os.symlink(HighLevelReco, os.path.basename(HighLevelReco))
      os.symlink(PandoraSettings, os.path.basename(PandoraSettings))
      os.symlink("../../../../runsimrec.py", "runsimrec.py")
      os.symlink("../../../../runcond.json", "runcond.json")
      os.symlink("../../../../version.py", "version.py")


      lcmd = subprocess.check_output("ln -sf ../../../../runsimrec.py .", shell=True, stderr=subprocess.STDOUT)
      lcmd = subprocess.check_output("ln -sf ../../../../runcond.json .", shell=True, stderr=subprocess.STDOUT)

      bsubcmd = " ".join(["bsub -o bsub.log -J ", "I"+str(genprocid), 
                    " \"( . "+INIT_PYTHON+" && ./runsimrec.py --numevents ",
                    str(numevents), " --dofile ",
                    genpath, " --recopt ", recopt,  " > bsub.log 2>&1 )\" "])
      fbsub = open("bsubcmd.sh","w")
      fbsub.write(bsubcmd)
      fbsub.close()
      
      if dryrun:
        print >> sys.stderr, "bsub script was created in "+rundir+"/"+bsubcmd
      else:
        lcmd = subprocess.check_output(bsubcmd, shell=True)
        print >> sys.stderr, lcmd
 
      os.chdir(curdir)

  elif afile != "":
    do_one_file(afile, numevents=numevents, genmeta=GENMETA_BY_ID_JSON, chdir=False, recopt=recopt)

  else:
    print "Error: neither --dofile nor --dolist was given"

  
