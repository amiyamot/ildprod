#!/bin/bash 
#
# Get space token of KEK-SRM and KEK-DISK
#
# Basic command to get space information of SRM
# 
###  ccw01:~% clientSRM GetSpaceTokens -v -e
###  httpg://kek2-se01.cc.kek.jp:8444 -d DISK_ILC_TOKEN
###  ============================================================
###  Sending GetSpaceTokens request to: httpg://kek2-se01.cc.kek.jp:8444
###  Before execute:
###  Afer execute:
###  Request Status Code 0
###  Poll Flag 0
###  ============================================================
###  Request status:
###    statusCode="SRM_SUCCESS"(0)
###    explanation=""
###  ============================================================
###  SRM Response:
###    arrayOfSpaceTokens(size=1)
###    [0] "AA0C75BC-7A01-4C83-CA0D-CFD28E27AF05"
###  
###  
###  ccw01:~% clientSRM GetSpaceMetadata -v -e
###  httpg://kek2-se01.cc.kek.jp:8444 -s
###  AA0C75BC-7A01-4C83-CA0D-CFD28E27AF05
###  ============================================================
###  Sending GetSpaceMetaData request to: httpg://kek2-se01.cc.kek.jp:8444
###  Before execute:
###  Afer execute:
###  Request Status Code 0
###  Poll Flag 0
###  ============================================================
###  Request status:
###    statusCode="SRM_SUCCESS"(0)
###    explanation=""
###  ============================================================
###  SRM Response:
###    arrayOfSpaceDetails (size=1)
###        [0] spaceToken="AA0C75BC-7A01-4C83-CA0D-CFD28E27AF05"
###        [0] status: statusCode="SRM_SUCCESS"(0)
###                    explanation="Valid space token"
###        [0] owner="/DC=it/DC=infngrid/OU=Services/CN=storm"
###        [0] totalSize=300000000000000
###        [0] guaranteedSize=300000000000000
###        [0] unusedSize=299999999995904
###        [0] lifetimeAssigned=-1
###        [0] lifetimeLeft=-1
###  ============================================================
###  
###  
# Actual command
# Tape quota

gfal-xattr srm://kek2-se01.cc.kek.jp:8444/srm/managerv2?SFN=/ spacetoken.description?ILC_TOKEN | jq .

# Disk quota 
gfal-xattr srm://kek2-se01.cc.kek.jp:8444/srm/managerv2?SFN=/ spacetoken.description?DISK_ILC_TOKEN | jq .
