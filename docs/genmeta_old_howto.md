# Prepare generator meta data ( old method )
a version in 20 August, 2018

## Overview
**A method described here is an old method used till August 2018.** 

**A newer method since August 2018 will be found in [ProductionManual.md](ProductionManual.md).**

## Step 1. Preparation of generator data on dirac, etc.
ILD production relies on the following generator data, meta information, etc. 
- Generator meta data and log files on DIRAC.  
-- Data files in `/ilc/prod/ilc/mc-dbd/generated`  
-- Meta file and log file in `/ilc/prod/ilc/mc-dbd.log/generated`. ( log file is not mandatory )  
-- The generator data on DIRAC should have the meta information consistent with the information of the meta file. 
- The meta information should be registered in the [ELOG genmeta data base](https://ild.ngt.ndu.ac.jp/elog/genmeta/). 
- The collection of the meta file should be prepared as json files(`genmetaByID.json` and `genmetaByFile.json`) and placed at KEKCC. 

### 1.1 Upload generator data and meta data prepared by generator group. 
Samples produced at SLAC are initially placed in SLAC web site. Generator files are added to DIRAC by 
Tim, someone in generator group, or by ourselves.  Meta data and job logs of generator samples 
produced at SLAC are prepared at SLAC web. They should be copied to the standard location on 
ILCDIRAC catalog with DESY-SRM as SE.  As long as KEKCC is concerned, `robot.txt` at SLAC web site 
disbles a recursive downloading of whizard job outputs, thus following tool downloads only the meta file. 

To help these task, following scripts are prepared in `ILDProd/gensamples`
- `get_meta_from_slac.sh` : Download meta files from Tim''s web site. 
- `meta_ul_rep.sh` : Upload downloaded meta files to DIRAC ( and replicate to other sites )

Samples produced at DESY are stored in DIRAC by the generatorWG. Meta data and job logs as well. 
If not ask Mikael or copy by ourselves.
Samples produced at KEK are stored in DIRAC by the generator WG. Meta data and job logs should be as well.

### 1.2 Replicate generator data to KEK-SRM 
Generator data (`stdhep` or `slcio`) should be replicated to KEK-SRM ( or DESY-SRM )
Meta data and log files are only at DESY-SRM.

`meta_ul_rep.sh` would be a template for replication and/or uploading of meta files on local disk.

### 1.3 Download meta data file to a localhost
`ILDProd/gensamples/get_newmeta.sh` is a script to download meta files registered in DIRAC.
Meta files newer than date given by argument are downloaded.


### 1.4 Check downloaded data for correctness. 
A command to do this task is in `ILDProd/gensamples/genmeta-check.py` 

The list of known sub-directories is at `ild01.ngt.ndu.ac.jp:/home/www/html/mc-prod/files2017/genmeta/known-evttype.txt`.
Update ths file when new sub-directory is created, otherwize you will see the waning message.

### 1.5 Set meta information to generator files on DIRAC catalog.
If this is a new sub-directory, set meta data to the generator sub-directory. A command would be 
```
dirac-dms-filecatalog-cli 
FC:/> cd <generator_directory>
FC:/> meta set . PythiaVer 6.422 HadTune OPAL EvtClass sub_directory_name
```

`setILCGen_Metadata.py` in `ILDProd/bin` is a script to set the generator meta data to files on DIRAC catalog.
A typical command usage is 
```
setILCGen_Metadata.py -G <path_to_the_meta_file>
```
For this option to work, the fileurl filed of `metafile.txt` should have the correct value.
&lowast; could be used in <path_to_the_meta_file>.
Other command options could be displaied by `setILCGen_Metadata.py -h`.

### 1.6 Register meta data to ELOG
The script, `elog-add-genmeta.py`, in `ILDProd/bin` could be used to create an Elog entry from a meta file.
The command usage is
```
   elog-add-genmeta.py <meta_file>
```
`elog-add-genmeta.py -h` displays more help information.

### 1.7 Update web information at [https://ild.ngt.ndu.ac.jp/CDS/mc-dbd.log/generated](https://ild.ngt.ndu.ac.jp/CDS/mc-dbd.log/generated)
by copying meta data to this host and/or updating html files. 

### 1.8 Update genmeta json files for use by sub-sequent steps.
`genmetaByID.json` and `genmetaByFile.json` are in **`/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta`**
and used to determine parameters for the production.

`make_json.py` is a command to read meta data in `/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta`
and creates `genmetaByID.json` and `genmetaByFile.json` in the current directory.
It is recommended to save new meta data files in `/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta`
if created locally. 

`find_newmeta_and_do_make_json.sh` creates `genmetaByID.json` and `genmetaByFile.json`
It downloads meta file on DIRAC, newer than a date set in the script, to a directory, `metadir`.
Then copy them to `/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta/newMeta`, if not 
exists there, then run `make_json.py` script to create `genmetaByID.json` and `genmetaByFile.json`
in the current directory. If produced json files are good, place then in 
`/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta` after backupping existing files.



