# Useful tools for production work

Examples to setup useful tools for production works are described here.

## git prompt

It is possible to include git branch name in bash prompt. To do this, 

```
wget https://github.com/git/git/raw/master/contrib/completion/git-prompt.sh

```
This version is slightly different from what I have now, but should work similarly.

Then include following lines to your .bashrc
```
source < download directory >/.bash-prompt.sh 
export PS1="\[\e[1;35m\](MC2)@\h \[\e[33m\]\w\[\e[0m\]$(__git_ps1 " (%s)")\n\$"
```

## git command alias

It is useful to define command alias in ~/.gitconfig.  An example of [alias] section 
in git config would be, 
```
[alias]
        st = status
        ci = commit
        co = checkout
        lola = log --graph --decorate=full\n--pretty=oneline --abbrev-commit --all
        lola2 = log --graph --decorate=full
        logf = "!echo \"Remember to add -S<string>\" ; git log --color -p --source --all"
        date = log --graph --pretty=format:'%C(yellow)%h%Creset %C(cyan)%ad%Creset%C(green)%d%Creset %s' --date=format:'%Y/%m/%d %H:%M:%S'

```


## Jq, a popular tool to browse json file

ILDProd include various tools to read/convert json files, such as jsonread, jsoncsv, etc.
They are being used extensively in ILDProd. Jq is a popular tool on internet, used 
by many other people. Among them, jq can be called with in a vim session for display/edit
json file with an easy to read format.

### Jq can be installed as follows.
```
wget  https://github.com/stedolan/jq/releases/download/jq-1.5/jq-1.5.tar.gz

mkdir build
cd build/

tar zxf ../download/jq-1.5.tar.gz
cd jq-1.5/

./configure --prefix=<install_dir> --disable-maintainer-mode
make
make install

```
jq-1.5 was the latest at this writting.

### vim configuration

Include following lines in ```~/.vimrc```

```
command! -nargs=? Jq call s:Jq(<f-args>)
function! s:Jq(...)
    if 0 == a:0
        let l:arg = "."
    else
        let l:arg = a:1
    endif
    execute "%! jq \"" . l:arg . "\""
endfunction

command! -nargs=? Jqc call s:Jqc(<f-args>)
function! s:Jqc(...)
    if 0 == a:0
        let l:arg = "."
    else
        let l:arg = a:1
    endif
    execute "%! jq -c \"" . l:arg . "\""
endfunction

```

To view a json file, open it by vim, then
```
:Jq
```
Or 
```
:Jq .key1.key2
```
to view a content of key1:key2

```
:Jqc
```
To revert a buffer to the original format.


