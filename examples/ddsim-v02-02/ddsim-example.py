from DIRAC.Core.Base import Script
Script.parseCommandLine()
from ILCDIRAC.Interfaces.API.NewInterface.UserJob import UserJob
from ILCDIRAC.Interfaces.API.NewInterface.Applications import DDSim
from ILCDIRAC.Interfaces.API.DiracILC import DiracILC
from datetime import datetime


# localjob=True
localjob=False

if localjob :
  genfile="/hsm/ilc/grid/storm/prod/ilc/mc-dbd/generated/1-calib/single/E1-calib.Ps_pm13_prnd.Glcio_gun.e0.p0.I110082.n1.slcio"

else:
  genfile="LFN:/ilc/prod/ilc/mc-dbd/generated/1-calib/single/E1-calib.Ps_pm13_prnd.Glcio_gun.e0.p0.I110082.n1.slcio"

now = datetime.now()
# simfile="ddsim-data-%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
simfile="sv02-01.mILD_l5_v05.E1-calib.I1110082.n1.dsim_%s.slcio" % now.strftime("%Y%m%d-%H%M%S")
simdir="myprod2/test3"


d= DiracILC(True,"repo.rep")

###In case one wants a loop: comment the folowing.
#for i in range(2):
j = UserJob()
j.setJobGroup("Tutorial")
j.setName("DDSim-example")  #%i)
j.setILDConfig("v02-02")
j.setOutputSandbox(["*.log","*.xml","*.sh","*.root"])
j.dontPromptMe()

# j.setInputSandbox(steeringfile)
# j.setDestination("OSG.PNNL.us")

ddsim = DDSim()
ddsim.setVersion("ILCSoft-02-02_cc7")
ddsim.setDetectorModel("ILD_l5_v02")
ddsim.setInputFile(genfile)
ddsim.setRandomSeed(12345)
ddsim.setStartFrom(10)
ddsim.setNumberOfEvents(5)  # Number of events should not exceed number of events in file.
                            # Otherwise, G4exception is thrown
# ddsim.setDebug()
# ddsim.setOutputFile(simfile)

extraCLIArguments = " --steeringFile ddsim_steer.py"
extraCLIArguments += " --outputFile %s " % simfile
ddsim.setExtraCLIArguments( extraCLIArguments )

res = j.append(ddsim)
if not res['OK']:
    print( res['Message'] )
    exit(1)
  

if localjob :
  j.submit(d, mode="local")

else:
  simdir="testjob"
  j.setOutputData(simfile,simdir,"KEK-DISK")
  res=j.submit(d)
  if res['OK']:
    print( "Dirac job, "+str(res["Value"])+", was submitted." )
  else:
    print( "Failed to submit dirac job. " )
    print( res )
