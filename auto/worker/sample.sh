#!/bin/bash

export LANG=C
echo "Hello world."
echo "Login to `hostname` "
pwd

cd /home/ilc/miyamoto/ILDProd/mcprod/v03-01/ProdTask/flavortag/flavortag.bWW_250zh2
( cd 3_gensplit 
  echo "##############################################################"
  pwd
  tail -500 status.log | grep -v "': " | tail -20 
  echo "  "
  echo "### Now is `date` ############################################"
  /bin/ls -l --time-style=full-iso status.log
)
(
  cd 4_subprod
  echo "###############################################################"
  pwd
  tail -20 setselected.log | grep -v Akiya | grep -v username | grep -v properties | grep -v VOMS 
  echo " "
  echo "### Now is `date` ############################################"
  /bin/ls -l --time-style=full-iso setselected.log
)

