#!/bin/env python 

import os

if __name__ == "__main__":
  htmldir = "/home/www/html/mc-prod/prodmon/testdir"
  logname = os.environ["LOGNAME"]
  # logfile = "/".join([htmldir, "cronlog_%s.log" % logname ])
  logfile = "logmon.log"
  htmlfile = "/".join([htmldir, "cronlog_%s.html" % logname ])

  out = ["<html><head><title>ILDProd log</title></head>",
         "<body>",
         "<h1>ILD production log</h1>",
         "<pre>"]
   
  lines = []
  for line in open(logfile):
    lines.append(line.replace("\n",""))

  ip = 0
  trstats = []
  trstats_on = 0
  while ip < len(lines):
    if " since PID=" in lines[ip]:
      out.append("<b><font color=\"green\">"+lines[ip])
      out.append(lines[ip+1]+"</font></b>")
      ip += 1
    elif "WARNING" in lines[ip].upper():
      out += ["<b><font color=\"#dd0000\">"+lines[ip]+"</font></b>"]
    elif "ERROR" in lines[ip].upper():
      out += ["<b><font color=\"#ff0000\">"+lines[ip]+"</font></b>"]
    elif "Adding " in lines[ip] and "file(s)) to production" in lines[ip]:
      out += ["<b><font color=\"green\">"+lines[ip]+"</font></b>"] 
    elif "==== Start to print production status" in lines[ip]:
      trstats_on = 1 
    elif "++ Updating nbtasks" in lines[ip]:
      trstats_on = 2
      out.append(lines[ip])
    elif trstats_on == 0:
      out.append(lines[ip])

    # output a table of production status
    maxcol=13
    if trstats_on == 1:
      if len(lines[ip].replace(" ","")) > 0:
         trstats.append(lines[ip])
    elif trstats_on == 2:
      out += ["<table border=1>"]
      line0  = "<td colspan=%d>%s</td>" % (maxcol,trstats[0])
      out += ["<tr bgcolor=\"#dddddd\" border=\"#000000\">"+line0+"</tr>"]
      for ir in range(1, len(trstats)):
        if trstats[ir].startswith("** "):
          out += ["<tr><td colspan=%d>" % maxcol + trstats[ir][3:] + "</td></tr>"]
        else:
          cols = trstats[ir].replace(">","").replace("|","").split()
          line0 = ""
          tropt = ""
          if cols[0].startswith("Tr.ID"):
             cols.append("nbtasks")          
             tropt=" bgcolor=\"#aaaaff\" "
          for ic in cols:
             line0 += "<td align=\"right\">"+ic+"</td>"
          out += ["<tr %s>" % tropt +line0+"</tr>"]
      out += ["</table>"]
      trstats_on = 0
      trstats = []  

    ip += 1

  out.append("</pre>")
  out.append("</body></html>")

  open(htmlfile,"w").write("\n".join(out))

  # print("\n".join(out))
  # print(out)

  
  
