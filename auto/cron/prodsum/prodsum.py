#!/bin/env python 

from __future__ import print_function

import elog
import os
from urllib2 import urlopen
import pprint
import json
import datetime

# ==================================================================
ELOGDBD=os.environ["PRODSUM_ELOGDBD"]
ELOGGENMETA=os.environ["PRODSUM_ELOGGENMETA"]

DBDPROD_FIRST_ID=int(os.environ["PRODSUM_DBDPROD_FIRST_ID"])
GENMETA_FIRST_ID=int(os.environ["PRODSUM_GENMETA_FIRST_ID"])
ELOG_GENMETA_JSON=os.environ["PRODSUM_ELOG_GENMETA_JSON"]
WEBFILE=os.environ["PRODSUM_WEBFILE"]
VERB=os.environ["PRODSUM_VERB"]

# ==================================================================
def load_elog_to_json(elogurl, first_eid, jsonfile ):

  last_id=0
  elogdata = {}
  if os.path.exists(jsonfile):
    elogdata=json.load(open(jsonfile))
    ids = sorted(elogdata.keys())
    last_id = ids[-1]

  if "dbd-prod" in elogurl:
    last_id = 0
    for eid in sorted(elogdata.keys()):
       if elogdata[eid]["attrib"]["JobStatus"] != "Done":
          break;
       else:
          last_id = eid

  print("last_id is "+str(last_id))
  abook = elog.open(elogurl)
  elog_last_id = abook.get_last_message_id()
  
  if elog_last_id > last_id:
    print("Reading elog %s. IDs are" % elogurl)
    id_start=first_eid if last_id == 0 else last_id+1
    id_last = elog_last_id
    for eid in range(id_start, elog_last_id+1):
      print( " "+str(eid), end="")
      ( message, attrib, attach ) = abook.read(eid)
      # if "JobStatus" not in attrib or attrib["JobStatus"] == "Done":
      elogdata[str(eid)] = {"message":message, "attrib":attrib, "attach":attach}

    print("")
    json.dump(elogdata, open(jsonfile,"w"))

  return elogdata
    
# =================================================================
def create_html(params):
  edata = params["edata"]
  class_stat = params["class_stat"]
  ecm_select = int(params["Energy"])

  out=[]

  out+=[ "<hr>",
         "<h2><a id=\"%s-%s\">" % (params["Energy"], params["MachineParams"]),
         "%s-%s</a>, %s</h2>" % (params["Energy"], params["MachineParams"],params["Model"]),
         "<hr>",
         "<table border=1 cellpadding=5>", 
         "<tr><td>process_type</td>" + "<td align=\"right\">pol</td>" + \
              "<td>processID</td>" + "<td align=\"right\">NbEvents</td>" + \
              "<td align=\"right\">int.lumi(1/fb)</td>" + \
              "<td align=\"right\">Done %</td>" + "<td>ElogID(s)</td>" + \
              "<td>ProdIDs of DST and REC files</td></tr>"]
  process_names = ""
  last_class = ""
  last_name = ""
  sortkey = {}
  outorder={"higgs_inclusive":"00", "higgs_exclusive":"01", "2f":"02", 
            "4f":"03", "6f":"04", "3f":"05", "5f":"06", "aa_2f":"07", "aa_4f":"08", 
            "flavortag":"09", "double_higgs":"10", "2f-JER":"11", "single":"12"}
  for procid in edata.keys():
    ed = edata[procid]
    if len(ed) < 4:
       print(" len(ed) < 0  procid="+str(procid))
    edgen = ed[4]
    polstr = edgen["polstr"]
    process_class = edgen["process_class"]
    process_names = edgen["process_names"]
    if process_class not in outorder:
        print("Process_class " +process_class+" not in outoder, " + str(outorder) + ", Forced as flavortag.")
        print("process_names="+process_names+" procid="+str(procid))
        outorder[process_class] = "%2.2d" % ( int(outorder["flavortag"])+1)
    class_order = outorder[process_class]
    energy = edgen["Energy"]
    sortkey["%4.4d "%energy + class_order+" "+process_class+" "+process_names+" "+polstr] = procid

  # for procid in sorted(edata.keys()):
  for skey in sorted(sortkey.keys()):
    if int(skey[0:4]) != ecm_select:
       continue
    procid = sortkey[skey]
    eloglink = []
    ed = edata[procid]
    for elogid in ed[2]:
       eloglink.append("<a href=\"%s%s\">%s</a>" % ( ELOGDBD, elogid, elogid))

    temp = ""
    for k in range(0, len(eloglink), 10):
       if temp != "":
          temp = temp + "<br>"
       temp = temp + ",".join(eloglink[k:k+10])
    elogids = temp
    # elogids = ",".join(eloglink)
    # print(" elogids = " + str(elogids) )
    procid_url="<a href=\"%s?process_id=%s\">%s</a>" % (ELOGGENMETA, str(procid), str(procid))
    iout=1
    tempdstvec = ed[5].replace("(ovl)","").replace("(nobg)","").replace("[dst]","[dst]").replace("[recdst]","[dst]").replace("<br>",",").split(",")
    temprecvec = ed[5].replace("(ovl)","").replace("(nobg)","").replace("[dst]","").replace("[recdst]","[rec]").replace("<br>",",").split(",")
    recid_str = "DST="
    # pprint.pprint(tempdstvec)
    for ip in range(0, len(tempdstvec)):
       if "dst" in tempdstvec[ip]:
          recid_str += tempdstvec[ip].replace("[dst]","")
          if iout > 7:
             recid_str += "<br>\n"
             iout=0
          elif ip < len(tempdstvec)-1:
             recid_str += ","
          iout = iout + 1
          # print(tempdstvec[ip]+"  "+recid_str)

    iout = 0 if iout > 7 else iout+1
    recid_str += ";REC="
    for ip in range(0, len(temprecvec)):
       if "rec" in temprecvec[ip]:
          recid_str += temprecvec[ip].replace("[rec]","")
          if iout > 7:
             recid_str += "<br>\n"
             iout = 0
          elif ip < len(temprecvec)-1:
             recid_str += ","
          iout = iout + 1


    edgen = ed[4]
    polstr = edgen["polstr"]
    process_class = edgen["process_class"]
    process_names = edgen["process_names"]
    if last_class != process_class:
       if VERB > 0:
          print(procid+" Done="+str(class_stat[process_class]["N_Done"]) + " Input="+str(class_stat[process_class]["N_Total"]))
       prodratio = float(class_stat[process_class]["N_Done"])/float(class_stat[process_class]["N_Total"]) * 100.0
       out.append("<tr bgcolor=\"#ccff99;\"><td colspan=8>%s :  " % process_class + \
                  " Produced rate %6.2f%% (N_Prod/N_Gen = %d/%d)" % \
                   (prodratio, class_stat[process_class]["N_Done"], class_stat[process_class]["N_Total"] ))
       last_class = process_class
       last_name = ""
       if VERB > 0:
           print(last_class+" "+out[-1])
 
    temp_name = process_names if last_name != process_names else "<font color=\"#D0D0D0\">"+process_names+"</font>" 

    outrec = "<tr><td>%s</td>" % (temp_name) + \
               "<td align=\"right\">%s</td>" % (polstr) + \
               "<td align=\"right\">%s</td>" % (procid_url) + \
               "<td align=\"right\">%s</td>" % (str(ed[1])) + \
               "<td align=\"right\">%s</td>" % (str(int(edgen["int_lumi"]))) + \
               "<td align=\"right\">%s</td>" % (edgen["done_fraction"]) + \
               "<td>  %s</td>" % (elogids)
      
    outrec += "<td>%s</td>" % recid_str if not recid_str.endswith(",") else "<td>%s</td>" % recid_str[:-1] 
    outrec += "</tr>\n"

    out.append(outrec)
    last_name = process_names

  out += ["</table>"]
  return out   


# ==================================================================
if __name__ == "__main__":

  dstmdir = "dst-merged"
  htmlsets = [{"Energy":"250", "MachineParams":"SetA", "Model":"ILD_l5_o1_v02"}, 
              {"Energy":"500", "MachineParams":"TDR_ws", "Model":"ILD_l5_o1_v02"}, 
              {"Energy":"91", "MachineParams":"nobeam", "Model":"ILD_l5_o1_v02"},
              {"Energy":"1", "MachineParams":"calib", "Model":"ILD_l5_o1_v02"}]

  # genmeta_file = get_genmetaByID()
  genmeta_file = os.environ["PRODSUM_GENMETA_JSON"]
 
  genmeta = json.load(open(genmeta_file))
  elogdbd = load_elog_to_json(ELOGDBD, DBDPROD_FIRST_ID, "elog_dbdprod.json")
  eloggenmeta = load_elog_to_json(ELOGGENMETA, GENMETA_FIRST_ID, "elog_genmeta.json")

  edata = {}
  recids = {}
  for eid in sorted(elogdbd.keys()):
     message = elogdbd[eid]["message"]
     attrib = elogdbd[eid]["attrib"]
     attach = elogdbd[eid]["attach"]
      
     if attrib["JobStatus"] != "Done":
        print(" eid=%s : JobStatus not done " % str(eid) )
        continue
     if attrib["ILDConfig"] != "v02-02" and attrib["ILDConfig"] != "v02-02-03":
        print(" eid=%s : ILDConfig is %s, neither v02-02 nor v02-02-03" % (str(eid), str(attrib["ILDConfig"])) )
        continue
 
     if VERB > 0:
         print( ">>>> elog id="+str(eid) )
     recids[eid] = attrib["RecID"]

     for url in attach:
        if "dstm-nevents-summary" in url:
            dstmnevts = urlopen(url)
            directory = ""
            for line in dstmnevts:
               if "Directory" in line:
                 directories = line.replace("\n","").split()
                 directory = directories[3] 
               #   pprint.pprint(directory)
               elif "slcio" in line:
                   ( procids, nfile, nevents, filename ) = line.replace("\n","").split()
                   procid = str(procids)
                   if procid not in edata:
                      edata[procid] = [int(nfile), int(nevents), [str(eid)], [directory, filename] ]
                   else:
                      edata[procid][0] += int(nfile)
                      edata[procid][1] += int(nevents)
                      if str(eid) not in edata[procid][2]:
                        edata[procid][2].append(str(eid))
                      edata[procid][3].append( [directory, filename ] )
    
# Add data of unprocessed generator files
  for key in eloggenmeta:
      attrib = eloggenmeta[key]["attrib"]
      procid_url = attrib["process_id"]
      procid = procid_url.split(">")[2].split("<")[0]
      if procid in edata:
          continue 
      if VERB > 0:
          print(" missing procid="+str(procid))
      edata[procid] = [0, 0, [], [] ]    
  
  class_stat={}
  for procid in edata.keys():
      x_section = float(genmeta[procid]["cross_section_in_fb"])
      total_nbevents = int(genmeta[procid]["total_number_of_events"])
      int_lumi = float(edata[procid][1])/x_section if x_section > 0.0 else 0.0
      done_fraction = "%5.1f" % (float(edata[procid][1])/float(total_nbevents)*100.0) if total_nbevents > 0 else 0.0
      process_type = genmeta[procid]["process_type"]
      process_names = genmeta[procid]["process_names"]
      polstr = "e%s.p%s" % ( genmeta[procid]["polarization1"], genmeta[procid]["polarization2"])
      energy = int(genmeta[procid]["CM_energy_in_GeV"])
  
      process_class = ""
      begin_str = {"2f_":"2f", "4f_":"4f", "6f":"6f", "ae_3f":"3f", "ea_3f":"3f",
                   "ea_5f":"5f", "ae_5f":"5f", "aa_2f":"aa_2f", "aa_4f":"aa_4f",  
                   "flavortag":"flavortag","2f-JER":"2f-JER", "single":"single"}
      for k in begin_str.keys():
         if process_type[:len(k)] == k:
            process_class = begin_str[k]
         # print(process_type+" procss_class = "+process_class)
  
      if process_class == "" and process_type.endswith("h"):
         if "h_" in process_names:
           process_class = "higgs_exclusive"
         else:
           process_class = "higgs_inclusive"
      if process_type == "hh":
         process_class = "double_higgs"
  
      # print(process_type+" process_class = "+process_class)
      edata[procid].append( {"x_section":x_section, "total_nbevents":total_nbevents, 
                             "int_lumi":int_lumi, "done_fraction":done_fraction, 
                             "process_names":process_names, "process_type":process_type,
                             "polstr":polstr, "process_class":process_class, 
                             "Energy":energy} )
  
      recvec = []
      if len(edata[procid][2]) > 0:
         for eid in edata[procid][2]:
           recvec.append(recids[str(eid)])
  
      temp = ""
      for k in range(0, len(recvec), 8):
         if temp != "":
            temp = temp + "<br>"
         temp = temp + ",".join(recvec[k:k+8])
  
      edata[procid].append(temp)
      if process_class not in class_stat:
         class_stat[process_class]={"N_Total":total_nbevents, "N_Done":long(edata[procid][1])}
      else:
         class_stat[process_class]["N_Total"] += long(total_nbevents)
         class_stat[process_class]["N_Done"] += long(edata[procid][1])
  
#  pprint.pprint(edata)

  now = datetime.datetime.now()
  nowstr ="%4.4d-%2.2d-%2.2d %2.2d:%2.2d:%2.2d (JST)" % \
         ( now.year, now.month, now.day, now.hour, now.minute, now.second)
  OUT = []
  if not os.path.exists(dstmdir):
     os.mkdir(dstmdir)

  hslinks = []
  webaddress = os.environ["WEB_ADDRESS"]
  for hs in htmlsets:
    hslinks.append("<a href=#%s-%s>%s-%s</a>" % (hs["Energy"], hs["MachineParams"],hs["Energy"],hs["MachineParams"]))

  OUT += ["<html><head><title>Production Summary</title></head><body>",
         "<h1>Summary of production mc-2020</h1>",
         "<a href=\"%s/mc-prod/prodmon/prodsum-mc2020-o2.html\">Link to the summary of ILD_l5_o2_v02 samples</a><br>" % os.environ["WEB_ADDRESS"],
         "( created on %s )" % nowstr, 
         "<hr>",
         "<ul>",
         "<li>mc-2020 production are being carried out using iLCsoft/ILDConfig <b><font color=\"RED\">v02-02 and/or v02-02-03</font></b></li>",
         "<li>This production aims to provide high statistics samples ( 1 to 5 ab<sup>-1</sup>) for physics study</li>",
         "<li>This page shows process type summary of produced data using ILD_l5_o1 model.</li>",
         "<li>Energy points of data:%s</li>" % " ".join(hslinks) ,
         "<li>Meanings of each columns of the summary table:",
         "<ul>",
         "<li><i>process_type</i> : process type of generator sample</li>",
         "<li><i>pol</i> : beam polarization</li>",
         "<li><i>processID</i>: process ID of generator sample, linked to <a href=\"%s/elog/genmeta\"><b>elog/genmeta</b></a> record.</li>" % webaddress,
         "<li><i>NbEvents</i> : Number of events in DST-merged files.</li>",
         "<li><i>int.lumi</i> : Integrated luminosity of events in DST-merged files.</li>", 
         "<li><i>Done %</i> : Fraction of produced events with respect to generated events in %</li>",  
         "<li><i>ElogID</i> : Elog ID of <a href=\"%s/elog/dbd-prod/\"><b>elog dbd-prod</b></a>, linked to the corresponding record.</li>" % webaddress,
         "<li><i>RecIDs</i> : IDs of reconstruction production</li>",
         "</ul></li>",
         "<li>File names and directories of dst-merged files could be found in an attachment file, dstm-nevents-summary.txt, of",
         " the corresponding elog record.</li>", 
         "<li>DST-merged files can be found in DIRAC file catalog. ",
         "<ul><li>For example, Higgs e2e2h_cc sample can be found by<br>",
         "<i>\"dirac-dms-find-lfns Path=/ilc/prod/ilc/mc-2020 Datatype=DST-MERGED GenProcessName=e2e2h_cc\"</i>.</li>",
         "<li>Meta keys for file search could be found by <i>\"meta get &lt;filename&gt;\"</i> sub-command of <i>dirac-dms-filecatalog-cli</i>. <i>&lt;filename&gt;</i> is any file in dst-merged directory.</li>",
         "</li>",
         "<li>Replicas are at DESY and KEK. Local path of them are",
         "<ul><li>DESY : /pnfs/desy.de/ilc/prod/ilc/mc-2020/ild/dst-merged</li>",
         "<li>KEK  : /group/ilc/grid/storm/prod/ilc/mc-2020/ild/dst-merged</li>",
         "</ul></ul>",
         "</ul>"]

  params = { "edata":edata, "class_stat":class_stat}

  

  for tbset in htmlsets:
     for pkey in tbset.keys():
        params[pkey] = tbset[pkey]
     OUT +=create_html(params)

  OUT.append("</body></html>")
  with open(WEBFILE,"w") as fout:
    fout.write("\n".join(OUT))
  print("### prodsum.py write html in %s" % WEBFILE)
  exit(0)


