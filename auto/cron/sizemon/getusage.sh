#!/bin/bash 

get_space()
{
  token=$1
  gfal-xattr srm://kek2-se01.cc.kek.jp:8444/srm/managerv2?SFN=/ spacetoken.description?${token} > ${token}.json
  total=`jq '.[0]["totalsize"]' ${token}.json`
  unused=`jq '.[0]["unusedsize"]' ${token}.json`
  totalstr=`echo "print(\"{:.2f}\".format(${total}/1E12))" | python - `
  unusedstr=`echo "print(\"{:.2f}\".format(${unused}/1E12))" | python - `

  usage=`echo "print(\"{:.2f}\".format((${totalstr}-${unusedstr})/${totalstr}*100.0))" | python - `
  echo "KEK ${token} : ${usage}% usage ( ${unusedstr}TB/${totalstr}TB (unused/allocated))."

}

get_space ILC_TOKEN
get_space DISK_ILC_TOKEN




