#!/bin/env python 

import json
# import xlsxwriter
import pprint
import os
import datetime
import ROOT
ROOT.gROOT.SetBatch()

_fsize = json.load(open("../fsize.json"))
_dayfrom = long(os.environ["SIZEMON_DAYFROM"])
_dayto = long(os.environ["SIZEMON_DAYTO"])

# =================================================================
def make_summary(dayfrom, dayto, fsize):

  dates = sorted(fsize.keys())
  data = {}
  desysrm = {}
  keksrm = {}
  desysrm0 = 0L
  keksrm0 = 0L
  srmsize = {}
  srmlist = ["DESY-SRM", "KEK-DISK", "KEK-SRM"]
  srmsize0 = {"DESY-SRM":0L, "KEK-DISK":0L, "KEK-SRM":0L}
  for daytime in dates:
    (days, time) = daytime.split('-')
    day = long(days)
    # print(" day="+str(day))
    if day >= dayfrom and day <= dayto:
      # print day
      # mcopt = {"DESY-SRM":{"size":0L, "replicas":0L}, "KEK-SRM":{"size":0L, "replicas":0L}}
      mcopt = {"DESY-SRM":{"size":0L, "replicas":0L}, "KEK-DISK":{"size":0L, "replicas":0L},
               "KEK-SRM":{"size":0L, "replicas":0L} }

      for mcdir in ["mc-2020"]:
        subdir = "/ilc/prod/ilc/%s" % mcdir
        print("## subdir="+subdir+" daytime="+daytime)
        if subdir in fsize[daytime]:           
           for se in ["DESY-SRM", "KEK-DISK", "KEK-SRM"]:
              if se in fsize[daytime][subdir]["sesize"]:
                mcopt[se]["size"] += long(fsize[daytime][subdir]["sesize"][se]["size"])
                mcopt[se]["replicas"] += long(fsize[daytime][subdir]["sesize"][se]["replicas"])

      srmsize[day] = {}
      for se in srmlist:
         srmsize[day][se] = {"size":mcopt[se]["size"], "replicas":mcopt[se]["replicas"]}


  ildprod = {}
  for se in srmlist:
    srmsize0[se] = {"replicas":srmsize[dayfrom][se]["replicas"], "size":srmsize[dayfrom][se]["size"]}


  # lastsize = {"DESY-SRM":0L, "KEK-SRM":0L}
  lastsize = {"DESY-SRM":0L, "KEK-DISK":0L, "KEK-SRM":0L}
  for day in sorted(srmsize.keys()):
    ildprod[day] = {"size":{}, "replicas":{}}
    for se in srmlist:
      asize = srmsize[day][se]["size"] - srmsize0[se]["size"] if srmsize[day][se]["size"] > 0 else 0
      ildprod[day]["size"][se] = asize 
      ildprod[day]["replicas"][se] = srmsize[day][se]["replicas"] - srmsize0[se]["replicas"] if srmsize[day][se]["replicas"] > 0 else 0

  return ildprod

# =========================================================================
def make_plot(datype, dayfrom, dayto):

  rootfile = datype + ".root"
  rf = ROOT.TFile(rootfile, "recreate")
  dafrom = ROOT.TDatime(dayfrom/10000, (dayfrom%10000)/100, dayfrom%100, 0, 0, 0)
  dato   = ROOT.TDatime(dayto/10000, (dayto%10000)/100, dayto%100, 0, 0, 0)

  ayear = dayto/10000
  amonth = ( dayto%10000)/100
  aday = ( dayto%100 )

  dabins = (dato.Convert() - dafrom.Convert())/3600
  c1 = ROOT.TCanvas("c1", datype, 1200, 900)
  hf1= ROOT.TH1F("hf1", datype, dabins, float(dafrom.Convert()), float(dato.Convert()))
  hf1.GetXaxis().SetTimeDisplay(1)
  hf1.GetXaxis().SetTimeFormat("%m-%d")
  hf1.GetXaxis().SetTitle("From %d to %d" % (dayfrom, dayto) )
  scale = 1.0
  if datype == "size":
    hf1.GetYaxis().SetTitle("Data size(TB)")
    scale = 1.E12
  elif datype == "replicas":
    hf1.GetYaxis().SetTitle("k replicas ")
    scale = 1.E3

  grs1 = []
  grsprop = [ {"name":"desy", "color":38}, {"name":"kek-disk", "color":2}, {"name":"kek-tape", "color":3} ]
  for gid in range(0, len(grsprop)):
    gr = ROOT.TGraph()
    gr.SetName(grsprop[gid]["name"])
    gr.SetMarkerColor(grsprop[gid]["color"])
    gr.SetMarkerStyle(20)
    gr.SetFillColor(grsprop[gid]["color"])
    grs1.append(gr)

  ip = -1
  datakey = ["DESY-SRM", "KEK-DISK", "KEK-SRM"]
  maxsize = 0
  for day in sorted(ildprod.keys()):
    ip += 1
    ayear = day/10000
    amonth = (day%10000)/100
    aday = day%100
    dax = ROOT.TDatime(ayear, amonth, aday, 0, 0, 0)

    dsize = 0.0
    for gid in range(0, len(grs1)):
      # pprint.pprint(ildprod[day])

      dsize += float(ildprod[day][datype][datakey[gid]])/scale
      # if dsize > 0.0:
      # print(str(day)+" "+str(datype)+" "+str(datakey[gid])+" "+str(ildprod[day][datype][datakey[gid]]))
      grs1[gid].SetPoint(ip, dax.Convert(), dsize)
      # print dsize
      maxsize = dsize if dsize > maxsize else maxsize

  # for gid in range(0, len(grs1)):
  #    grs1[gid].SetPoint(ip, dax.Convert(), 0.0)
       
  hf1.SetMaximum(1.2*maxsize)
  hf1.SetStats(0)
  hf1.Draw()
  ROOT.gPad.SetGridx(1)
  ROOT.gPad.SetGridy(1)
  # grs1[1].Draw("B")
  for gid in range(len(grs1), 0, -1):
    grs1[gid-1].Draw("B")


  ypos=0.6
  txlist = []
  for gid in range(0, len(grs1)):
    tx = ROOT.TText(0.2, ypos, datakey[gid])
    txlist.append(tx)
    tx.SetNDC(1)
    tx.SetX(0.2)
    tx.SetY(ypos)
    tx.SetTextColor(grsprop[gid]["color"])
    tx.SetTextSize(0.03)
    tx.Draw()
    ypos += 0.04

  c1.Update()

  # c1.Print("datasize.pdf")
  c1.Print(datype+".png")
  c1.Print(datype+".C")
  c1.Print(datype+".root")
  rf.Write()
  rf.Close()

# =========================================================================
if __name__ == "__main__":
  if _dayto <= 0L:
    now = datetime.datetime.now()
    after = now + datetime.timedelta(days=7)
    _dayto = after.year * 10000L + after.month*100 + after.day

  ildprod = make_summary(_dayfrom, _dayto, _fsize)
  #
  make_plot("size", _dayfrom, _dayto)
  make_plot("replicas", _dayfrom, _dayto)


 
