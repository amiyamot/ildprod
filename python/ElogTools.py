#!/bin/env python 
#
# Register elog info
#

import elog
import os
import argparse
import json
import datetime
import pprint
import glob
import shutil
import time

from DSTM_NEvents_Summary import *

_logbook = None
_elog_url = "https://ild.ngt.ndu.ac.jp/elog"
_elog_subdir = "dbd-prod" if "ELOG_SUBDIR" not in os.environ else os.environ["ELOG_SUBDIR"]
# use export ELOG_SUBDIR=test-prod to test production
_do_test = False

# ######################################################
def open_logbook():
    ''' open elog logbook '''
    global _logbook, _elog_url, _elog_subdir, _do_test
   
    
    if _logbook == None:
        lines = open(os.environ["MY_ELOG_KEY"]).readlines()
        ( auser, apass ) = lines[0][:-1].split(' ')
        _logbook = elog.open("%s/%s/" % (_elog_url, _elog_subdir), user=auser, password=apass)

    return 



# ######################################################
def putelog_prepProd(param):
    ''' Create elog entry and register first entry of prepProd step. '''
    global _logbook, _elog_url, _elog_subdir, _do_test


    prconf = json.load(open(param["config_file"]))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day ) 
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    msglist = ["<ul>", "<li>%s %s : Production for %s has registered.</li>" % (dstr, tstr, prconf["__ELOG_PRODUCTION_SETID"]) ]

    edata = {}
    edata["Worker"] = prconf["__ELOG_WORKER"]
    edata["SplitID"] = prconf["__PRODID_FOR_GENSPLIT"]
    edata["Ecm"] = prconf["__ENERGY"]
    edata["ProcName"] = prconf["__ELOG_PROCNAME"] 
    edata["JobStatus"] = "InPreparation"
    edata["NEvents"] = str(prconf["__ELOG_NEVENTS"]) + " events will be submitted"

    procids = prconf["__ELOG_PROCIDS"].split(',')
    numprocs = len(procids)
    if numprocs < 3:
        edata["ProcID"] = prconf["__ELOG_PROCIDS"]
    else:
        edata["ProcID"] = procids[0]+"-"+procids[-1]
        msglist += ["<ul>", "<li>Process IDs of this production : %s</li>" % prconf["__ELOG_PROCIDS"] ]

    pol_list = prconf["__ELOG_POL"].split(',')
    if len(pol_list) < 2:
        edata["Pol"] = prconf["__ELOG_POL"]
    else:
        if len(msglist) < 3:
            msglist += ["<ul>"]
        edata["Pol"] = "mixed-pol"
        msglist += ["<li>Beam polarizations of input samples : %s</li>" % prconf["__ELOG_POL"] ]

    if len(param["message"]) > 0:
        if len(msglist) < 3:
            msglist += ["<ul>"]
        for msg in param["message"]:
            msglist += ["<li>"+msg+"</li>"]

    if len(msglist) < 3:
        msglist += ["</ul>"]
    else:
        msglist += ["</ul>", "</ul>"]
    msgtext = '\n'.join(msglist)

    upload_files = ["input_genfiles.list"] 

    if _do_test:
        print( "### msgtext " )
        print( msgtext )
        print( "### elog_record" )
        pprint.pprint(edata)
        print( "### Upload files." )
        pprint.pprint(upload_files)
        msg_id = 98765
    else: 
        open_logbook()
        msg_id = _logbook.post(msgtext, attributes = edata, attachments = upload_files, encoding = "HTML" )

    prconf["__ELOG_ID"] = msg_id
    json.dump(prconf, open(param["config_file"], "w"))

    print( "Elog entry for this production was created in elogID=%d in ELOG %s " % (int(msg_id), _elog_subdir) )

    return 

# ################################################################################
def putelog_upload(param):
    ''' Update elog information for gensplit '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    if "elogid" in param:
       elogid = param["elogid"]
    else:
       prconf = json.load(open(param["config_file"]))
       elogid = prconf["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)

    # Upload config file.
    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    uploadfiles = str(param["upload"])
    elogmsg = str(param["message"]) if len(param["message"]) > 0 else "File, %s, was uploaded." % uploadfiles 
    msglist += [ "<li>%s %s : %s</li>" % (dstr, tstr, elogmsg), "</ul>" ]
    msgtext = "\n".join(msglist)
    if ":" in uploadfiles:
       upload_files = uploadfiles.split(":") 
    else:
       upload_files = [ uploadfiles ]

    # newid = _logbook.post(msgtext, attributes = edata, msg_id=elogid, attachments = upload_files, encoding = "HTML" )
    newid = _logbook.post(message, msg_id=elogid, attachments = upload_files, encoding = "HTML" )

    print( "%s was uploaded to elogID=%d in ELOG %s " % (uploadfiles, int(newid), _elog_subdir) )

    return 

# ######################################################
def putelog_gensplit(param):
    ''' Update elog information for gensplit '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    prconf = json.load(open(param["config_file"]))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day ) 
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["__ELOG_ID"]
    open_logbook()
    (message, attributes, attachements ) = _logbook.read(elogid)

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += [ "<li>%s %s : gen data splitting has initiated and sim/rec production will start/has started already.</li>" % (dstr, tstr),
               "</ul>"]
    message = "\n".join(msglist)
    msg_id = _logbook.post(message, msg_id=elogid, encoding="HTML")

    print( "gensplit information was appended to ELOG ID "+str(elogid) )

    return

# ##############################################################
def putelog_by_attrib(message, attributes):
    ''' Create a new Elog entry with message and attributes given '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    open_logbook()
    elogid = _logbook.post(message, attributes=attributes, encoding="HTML")

    return elogid
  

# ##############################################################
def getelog(elogid):
    ''' get Elog data from given elog_id '''
    open_logbook()
    (message, attributes, attachements ) = _logbook.read(elogid)
    res = {"message":message, "attributes":attributes, "attachements":attachements}

    return res

# ######################################################
def putelog_loguploaded(param):
    ''' Update elog information after logs are uploaded '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    prconf = json.load(open(param["config_file"]))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += ["<li>%s %s : log files were uploaded. List of log files will be found in the attachement.</li>" % (dstr, tstr)]
    edata["JobStatus"] = edata["JobStatus"].replace("Logging","Logging_Done").replace("logging","Logging_Done")

    if "MergeDST_Done" in edata["JobStatus"] and "Logging_Done" in edata["JobStatus"]:
       edata["JobStatus"] = "Done"
       msglist.append("<li>%s %s : All step completed. JobStatus is changed to Done </li>" % (dstr, tstr) )

    msglist.append("</ul>")
    message = "\n".join(msglist)

    # msg_id = _logbook.post(message, msg_id=elogid, encoding="HTML")
    # edata["JobStatus"] = edata["JobStatus"].replace("logging","Done_Logging")
    edata["Log_SimJob"] = "see attached file, log_upload_list.txt"
    edata["Log_RecJob"] = "see attached file, log_upload_list.txt"       
    upload_files = ["log_upload_list.txt"]

    if elogid > 0:
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachments = upload_files, encoding = "HTML" )
       print( "logupload information was appended to ELOG ID "+str(elogid) )
    else:
       print( "logupload information was not uploaded. ELOG ID was "+str(elogid) )

    return

# ######################################################
def putelog_dstm_start(param):
    ''' Update elog information after dstmerge completes '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    conffile = param["config_file"]
    if "*" in param["config_file"]:
        conffile = glob.glob(param["config_file"])[0]  # Takes 1st json file.

    print( "Will open "+conffile )
    prconf = json.load(open(conffile))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["conf"]["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)

    if "MergeDST" not in edata["JobStatus"]:
      msglist = ["<ul>",
                 "<li>%s %s : Production completed and dst-merge job has started. </li>" % (dstr, tstr),
                 "</ul>"]
      message += "\n".join(msglist)
      # msg_id = _logbook.post(message, msg_id=elogid, encoding="HTML")
      tempst = edata["JobStatus"].replace("Production_Done","").replace("Production","").replace(" ","")
      if len(tempst) > 0:
        edata["JobStatus"] += ":MergeDST"
      else:
        edata["JobStatus"] = "MergeDST"
    else:
      msglist = ["<ul>",
                 "<li>%s %s : Restart the complete chain of dst-merge file creation task. </li>" % (dstr, tstr),
                 "</ul>"]
      message += "\n".join(msglist)
      

    if elogid > 0:
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, encoding = "HTML" )
       print( "MergeDST start information was appended to ELOG ID "+str(elogid) )
    else:
       print( "MergeDST start information was not uploaded. ELOG ID was "+str(elogid) )

    return

# ######################################################
def putelog_dstm_nevents(param):
    ''' Update elog information after dstmerge completes '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    # Create a summary file to "dstm-nevents-summary.txt"
    ret = create_dstm_nevents_summary()
    if not ret["OK"]:
       print( ret["message"] )
       return
    print( ret["message"] )
    summary_file = ret["summary_file"]

    conffile = param["config_file"]
    if "*" in param["config_file"]:
        conffile = glob.glob(param["config_file"])[0]  # Takes 1st json file.

    print( "Will open "+conffile )
    prconf = json.load(open(conffile))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["conf"]["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += [ "<li>%s %s : Number of events of DST-Merged files is uploaded to %s. </li>" % (dstr, tstr, ret["summary_file"]),
               "</ul>"]
    message = "\n".join(msglist)
    # msg_id = _logbook.post(message, msg_id=elogid, encoding="HTML")
    # edata["JobStatus"] = "Done"
    edata["NEvents"] = ret["nevtotal"]
    # edata["JobStatus"] = edata["JobStatus"].replace("Merging-DST","Merged-DST")
    upload_files = [ summary_file ]
    print( upload_files )

    if elogid > 0:
    #    msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachements = upload_files, encoding = "HTML" )
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachments = upload_files, encoding = "HTML" )
       print( "DSTM nevent information was appended to ELOG ID "+str(elogid) )
    else:
       print( "DSTM nevent information was not appended. ELOG ID was "+str(elogid) )

    return

# ######################################################
def putelog_dstm_replicating(param):
    ''' Update elog information after dstmerge completes '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    conffile = param["config_file"]
    if "*" in param["config_file"]:
        conffile = glob.glob(param["config_file"])[0]  # Takes 1st json file.

    print( "Will open "+conffile )
    prconf = json.load(open(conffile))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    # replicate_log = "dstm-replicate.log"
    replicate_log = "rep-request.list"
    if not os.path.exists( replicate_log ):
       print( "Error: %s not exists. Something is wrong." % replicate_log )
       print( "Elog ID %s is not updated." % str(elogid) )
       return
    #reqname = ""
    #reqid = ""
    reqidv = []
    for line in open(replicate_log):
    #   if line[0:len("Request ")] == "Request ":
    #      reqname = line[:-1].split()[1]
    #   if line[0:len("RequestID(s): ")] == "RequestID(s): ":
    #      reqid = line[:-1].split(' ',1)[1]
       reqidv.append(line.replace("\n",""))

    if len(reqidv) < 1:
       print("No replication ID found in rep-request.list")
       return

    elogid = prconf["conf"]["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)
    dstm_replicates = prconf["dstm"]["replicates"]

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += [ "<li>%s %s : Start replicating DST-Merged files to %s. <br>" % (dstr, tstr, dstm_replicates),
               "  RequestID(s): %s </li>" % ",".join(reqidv), 
               "</ul>"]
    message = "\n".join(msglist)
    #                "  Request %s, RequestID(s): %s </li>" % (reqname, reqid), 

    if elogid > 0:
    #    msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachements = upload_files, encoding = "HTML" )
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, encoding = "HTML" )
       print( "ELOG ID %s was updated for replication of DST-Merged files to %s" % (str(elogid), dstm_replicates ) )
    else:
       print( "ELOG ID %s was NOT updated for DST-Merged replication"+str(elogid) )

    return


# ######################################################
def putelog_dstm_replicated(param):
    ''' Update elog information after dstmerge completes '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    conffile = param["config_file"]
    if "*" in param["config_file"]:
        conffile = glob.glob(param["config_file"])[0]  # Takes 1st json file.

    print( "Will open "+conffile )
    prconf = json.load(open(conffile))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["conf"]["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)
    dstm_replicates = prconf["dstm"]["replicates"]

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += ["<li>%s %s : DST-Merged file replicaton has completed. Replicated to %s. </li>" % (dstr, tstr, dstm_replicates), 
                "</ul>" ]
    message = "\n".join(msglist)

    if elogid > 0:
    #    msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachements = upload_files, encoding = "HTML" )
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, encoding = "HTML" )
       print( "ELOG ID %s was updated for replication of DST-Merged files to %s" % (str(elogid), dstm_replicates ) )
    else:
       print( "ELOG ID %s was NOT updated for DST-Merged replication"+str(elogid) )

    return

# ######################################################
def putelog_dstm_loguploaded(param):
    ''' Update elog information after dstmerge completes '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    conffile = param["config_file"]
    if "*" in param["config_file"]:
        conffile = glob.glob(param["config_file"])[0]  # Takes 1st json file.

    print( "Will open "+conffile )
    prconf = json.load(open(conffile))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["conf"]["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += ["<li>%s %s : DST-Merged job log files were uploaded. </li>" % (dstr, tstr) ]
    edata["JobStatus"] = edata["JobStatus"].replace("MergeDST","MergeDST_Done")

    if "MergeDST_Done" in edata["JobStatus"] and "Logging_Done" in edata["JobStatus"]:
       edata["JobStatus"] = "Done"
       msglist.append("<li>%s %s : All steps have completed. JobStatus is changed to Done </li>" % (dstr, tstr) )

    msglist.append("</ul>")
    message = "\n".join(msglist)

    if elogid > 0:
    #    msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachements = upload_files, encoding = "HTML" )
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, encoding = "HTML" )
       print( "ELOG ID %s was updated for DSTM-Job-Log-Update " % (str(elogid) ) )
    else:
       print( "ELOG ID %s was NOT updated for DSTM-Job-Log update"+str(elogid) )

    return


# ######################################################
def putelog_logging(param):
    ''' Update elog information after logs are uploaded '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    prconf = json.load(open(param["config_file"]))
    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    elogid = prconf["__ELOG_ID"]
    open_logbook()
    (message, edata, attachements ) = _logbook.read(elogid)

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += ["<li>%s %s : download/upload log files have started.</li>" % (dstr, tstr),
               "</ul>"]
    message = "\n".join(msglist)
    # msg_id = _logbook.post(message, msg_id=elogid, encoding="HTML")
    tempst = edata["JobStatus"].replace("Production_Done","").replace("Production","").replace(" ","")
    if len(tempst) > 0:
      edata["JobStatus"] += ":logging"
    else:
      edata["JobStatus"] = "logging"

    if elogid > 0:
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, encoding = "HTML" )
       print( "Information about start logging of production was appended to ELOG ID "+str(elogid) )
    else:
       print( "Information about start logging of production was NOT appended. ELOG ID was "+str(elogid) )

    return

# ######################################################
def putelog_text(elogid, param):
    ''' Update elog information after dstmerge completes '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    open_logbook()
    msgids = _logbook.get_message_ids()
    if elogid not in msgids:
      print( "ERROR : Elog ID "+str(elogid)+" not found in putelog_text, mesage=" + str(params["message"]) )
      return

    (message, edata, attachements ) = _logbook.read(elogid)

    msgtext=param["message"]
    if param["msg_file"] != "":
       msgbuf=[]
       for line in open(param["msg_file"]):
         msgbuf.append(line[:-1])
       msgtext = "<br>\n".join(msgbuf)

    msglist = message.split("\n")
    if msglist[-1] == "</ul>":
       msglist.pop(-1)
    else:
       msglist.append("<ul>")
    msglist += ["<li>%s %s : %s</li>" % (dstr, tstr, msgtext), "</ul>"]
    message = "\n".join(msglist)

    if elogid > 0:
    #    msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, attachements = upload_files, encoding = "HTML" )
       msg_id = _logbook.post(message, msg_id=elogid, attributes=edata, encoding = "HTML" )
       msginfo = param["message"] if param["msg_file"] == "" else "texts from a file "+param["msg_file"]
       print( "ELOG ID %s was updated to include %s" % (str(elogid), msginfo ) )
    else:
       print( "ELOG ID %s was NOT updated in putelog_text, ElogID"+str(elogid) )

    return


# ######################################################
def putelog_prod_reconly(param):
    ''' Register elog entry for rec-only production '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    # print param
    jsonfile = param["jsonfile"]
    (ptype, prodid, pmodel) = jsonfile.split('.')[0].split('_',2)
    if int(procid) == 12345:
       print( "Invalid ProcID=12345 is skipped." )
       return
    prconf = json.load(open(jsonfile))

    sim_elogid = prconf["__ELOG_ID"]
    (sim_message, sim_attrib, sim_attach ) = _logbook.read(sim_elogid)

    edata["Worker"] = prconf["__ELOG_WORKER"]
    edata["SplitID"] = prconf["__PRODID_FOR_GENSPLIT"]
    edata["Ecm"] = prconf["__ENERGY"]
    edata["ProcName"] = prconf["__ELOG_PROCNAME"] 
    edata["NEvents"] = str(prconf["__ELOG_NEVENTS"]) + " events will be submitted"
    edata["SimID"] = sim_attrib["SimID"]
    edata["ProcID"] = sim_attrib["SimID"]

    edata["JobStatus"] = "InPreparation"

    return



# ######################################################
def putelog_subprod(param):
    ''' Update elog information for subprod '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    now = datetime.datetime.now()
    dstr = "%4.4d-%2.2d-%2.2d" % (now.year, now.month, now.day )
    tstr = "%2.2d:%2.2d:%2.2d" % (now.hour, now.minute, now.second)

    # print param
    prodlog = param["prodlog"]
    jsonlog = []
    for line in open(prodlog):
      if " meta were written to " in line:
         jsonfile = line[:-1].rsplit(' ',1)[1].replace('.json.', '.json')
         jsonlog.append(jsonfile)

    simid=[]
    nobgid=[]
    ovlid=[]
    nodry = False
    models = []
    detectors = []
    recdst = {}
    for jfile in jsonlog:
       print( jfile )
       ( ptype, prodid, pmodel ) = jfile.split('.')[0].split('_',2)
       if int(prodid) == 12345:
          continue
       models.append(pmodel)
       prconf = json.load(open(jfile))
       elogid = prconf['conf']["__ELOG_ID"] if "__ELOG_ID" in prconf['conf'] else -1
       dstonly = "FALSE" if "__DSTONLY" not in prconf["conf"] else prconf["conf"]["__DSTONLY"]
       if ptype == "simprod":
          simid.append({"ProdID":prodid, "detectorModel":prconf["detectorModel"], "json":prconf})
          detectors.append(prconf["detectorModel"])
       elif ptype == "recprod":
          recinfo = {"ProdID":prodid, "detectorModel":prconf["detectorModel"], "json":prconf}
          recinfo["recdst"] = "recdst"
          if dstonly == "TRUE":
             if prconf["conf"]["__RECRATE"] == "-1":
               recinfo["recdst"] = "dst"
             else:
               if int(prconf["rec"]["meta"]["SelectedFile"]) == 3:
                  recinfo["recdst"] = "dst"
               elif int(prconf["rec"]["meta"]["SelectedFile"]) == 2:
                  recinfo["recdst"] = "recdst"

          if "nobg" in pmodel:
             nobgid.append(recinfo)
             detectors.append(prconf["detectorModel"])
          else:
             ovlid.append(recinfo)
             detectors.append(prconf["detectorModel"])
       else:
          print( "### ERROR : Undefined prod json file " + jfile + " was found" )
          exit(10) 

    if len(simid) + len(nobgid) + len(ovlid) < 1:
      print( "### Non dry production was found. Skip ELOG update." )
      return

    

    if "__ELOG_ID" in prconf['conf']:
      elogid = prconf['conf']["__ELOG_ID"]
      open_logbook()
      (message, edata, attachements ) = _logbook.read(elogid)
      msgold = message.split('\n')
      for i in range(len(msgold), 0, -1):
         if "</ul>" in msgold[i-1]:
            msgold[i-1] = msgold[i-1].replace("</ul>", "")
            break
      msgold[-1] = msgold[-1].replace("</ul>","")
    else:
      print( "### WARNING : __ELOG_ID is not defined in the input json file." )
      elogid = -1
      edata = {}
      message = ""
        
    msgnew = msgold
    if "_ls5_" in edata["Detector"] :
      detectors += [edata["Detector"].replace("_ls5_","_l5_"), edata["Detector"].replace("_ls5_","_s5_")]
    else:
      if len(edata["Detector"].replace(" ","")):
        for det in edata["Detector"].split(','):
          detectors.append(det)

    print( detectors )

    removelist = []
    addlist = []
    hasrec = False
    for det in detectors:
       if "_o" in det:
          hasrec = True
       if "_l5_" in det and det.replace("_l5_","_s5_") in detectors:
          removelist += [det, det.replace("_l5_","_s5_")]
          addlist += [det.replace("_l5_","_ls5_")]
    for dadd in addlist:
       detectors.append(dadd)
    for ddel in removelist:
       detectors.remove(ddel)
    removelist = []
    if hasrec:
       for det in detectors:
         if "_o" not in det:
            removelist.append(det)
       for det in removelist:
         detectors.remove(det)

    detectors = sorted(set(detectors), key=detectors.index) 

    if len(simid) > 0:
       simids = ""
       simmsg = ""
       for v in simid:
         simids += ",%s" % str(v["ProdID"])
         simmsg += ",%s(%s)" % ( str(v["ProdID"]), str(v["detectorModel"]) )
       edata["SimID"] = simids[1:] if edata["SimID"] == "" else edata["SimID"] + simids
       edata["ILDConfig"] = v["json"]["sim"]["ILDConfig"]
       msgnew.append("<li>%s %s : sim production, %s, was submitted.</li>" % ( dstr, tstr, simmsg[1:] ))
    if len(nobgid) > 0:
       nobgids = ""
       nobgmsg = ""
       for v in nobgid:
           if "recdst" not in v:
               nobgids += ",%d" % int(v["ProdID"]) 
               nobgmsg += ",%s(%s)" % ( str(v["ProdID"]), str(v["detectorModel"]) )
           else:
               nobgids += ",%d[%s]" % (int(v["ProdID"]), v["recdst"])
               nobgmsg += ",%d(%s,%s)" % ( int(v["ProdID"]), v["detectorModel"], v["recdst"] )
       edata["RecID"] = nobgids[1:]+"(nobg)" if edata["RecID"] == "" else edata["RecID"] + nobgids+"(nobg)"
       edata["ILDConfig"] = v["json"]["rec"]["ILDConfig"]
       msgnew.append("<li>%s %s : rec nobg production, %s, was submitted.</li>" % ( dstr, tstr, nobgmsg[1:] ))
    if len(ovlid) > 0:
       ovlids = ""
       ovlmsg = ""
       for v in ovlid:
           if "recdst" not in v:
               ovlids += ",%s" % str(v["ProdID"])
               ovlmsg += ",%s(%s)" % ( str(v["ProdID"]), str(v["detectorModel"]) )
           else:
               ovlids += ",%d[%s]" % (int(v["ProdID"]), v["recdst"])
               ovlmsg += ",%d(%s,%s)" % ( int(v["ProdID"]), v["detectorModel"], v["recdst"] )

       edata["RecID"] = ovlids[1:]+"(ovl)" if edata["RecID"] == "" else edata["RecID"] + ovlids+"(ovl)"
       edata["ILDConfig"] = v["json"]["rec"]["ILDConfig"]
       msgnew.append("<li>%s %s : rec ovl production, %s, was submitted.</li>" % ( dstr, tstr, ovlmsg[1:]))

    if "," in edata["RecID"] :
       recvec = edata["RecID"].split(",")  
       recvec = sorted(set(recvec),key=recvec.index)
       edata["RecID"] = ",".join(recvec)

    print( "detectors .. "+str(detectors) )
    edata["Detector"] = ",".join(detectors) if len(detectors) > 1 else detectors[0]
    edata["JobStatus"] = "Production"       
    msgnew.append("</ul>")
    message = "\n".join(msgnew)

    print( "Data for elog" )
    print( message )
    pprint.pprint(edata)

    if elogid > 0:
       msg_id = _logbook.post(message, attributes = edata, msg_id=elogid, encoding = "HTML" )
       print( "subprod information was appended to ELOG ID "+str(elogid) )
    else:
       print( "subprod information was not uploaded. ELOG ID was "+str(elogid) )

    return

# ######################################################
def putelog_main():
    ''' get command line argument '''
    global _logbook, _elog_url, _elog_subdir, _do_test

    stepdef = ["prepProd", "gensplit", "subprod", "dstm_start", "logging", "prod_reconly", 
               "dstm_nevents", "dstm_replicating", "dstm_replicated", "loguploaded", "complete",
               "dstm_loguploaded", "text", "upload"]

    helpmsg = "\n".join(["Type of information to register. Following words are recognized.", ",".join(stepdef),
                         "[Examples:]", 
                         "putelog.py --prodlog submit-XXX-XXXX.log subprod",
                         "putelog.py -c recprod_... dstm_start  ( when Production completes) ",
                         "putelog.py -c recprod_... dstm_nevents ( After dstm-job end and job out are retrieved. )",
                         "putelog.py -c dstmjobs-../dstm-jobmaker.. dstm_replicated ( After dstm-replicated. )",
                         "putelog.py -i <elogid> -m text_message ( add text_message to the given ELOG ID ",
                         "putelog.py -i <elogid> -f file_containg_text_message ( add text_message to the given ELOG ID ",
                         ""])
    parser = argparse.ArgumentParser(description="Register production information in elog",
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument("step", help=helpmsg, nargs='?', default="")
    # parser.add_argument("step", help="Type of information to register. Either prepProd, gensplit, subprod, dstm_start, logging, complete", nargs='?', default="")
    parser.add_argument("-c","--config_file", help="production config file(json), where information is read/wrrite", 
                        nargs="?", action="store", default="production.json")
    parser.add_argument("-m", "--message", help="Message text to be added in the elog text area", nargs="?", dest="message", default=[])
    parser.add_argument("-u", "--upload", help="additional files to be uploaded to the elog", nargs="?", dest="upload", default=[])
    parser.add_argument("-p", "--prodlog", help="submit production log file", nargs="?", 
                        dest="prodlog", type=str, default="")
    parser.add_argument("-j", "--json", help="submit production log file (for rec only production)", nargs="?", 
                        dest="jsonfile", type=str, default="")
    parser.add_argument("-i", "--id", help="ELOG ID, whose record is updated.", nargs="?", dest="theID", default="0")
    parser.add_argument("-f", "--file", help="Text file containing message. Used instead of -m argument.", nargs="?", dest="msg_file", default="")

    args = parser.parse_args()
    step = args.step
    if step not in stepdef:
        print( "ERROR: "+str(step)+" is neither of the followings." )
        print( stepdef )
        exit(10)

    addmsg = args.message
    prodlog = args.prodlog
    jsonfile = args.jsonfile
    msg_file = args.msg_file

    ans = {"step":step, "config_file":args.config_file, "message":addmsg, "upload":args.upload, 
           "prodlog":prodlog, "jsonfile":jsonfile, "msg_file":msg_file, "elogid": args.theID }

    # 
    ret = 0
    gfunct = globals()
    if step == "text":
       if args.theID == "0":
          print( "ELOGID was not given." )
          return 1
       if len(addmsg) < 1 and msg_file == "":
          print( "Both message text and test file name was empty." )
          return 1  
       elogid=int(args.theID)
       putelog_text(elogid, ans)

    elif step in stepdef:
       afunct = gfunct["putelog_"+step]
       afunct(ans)
    else:
       print( "ERROR: Undefined step "+step+ "was called." )
       ret = -1

    return ret

