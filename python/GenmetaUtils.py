#!/usr/bin/env python 
# Add a record to elog genmeta
#
# Warning 
# #Events and xsect-error fields are not filled. Not know why.
#
# 8-March-2018 A.Miyamoto
#

import os
import argparse
from urllib2 import urlopen
import ssl
import pprint
import glob
import braceexpand


PASSINFO = "MY_ELOG_KEY"
LOGBOOK = "https://ild.ngt.ndu.ac.jp/elog/genmeta/"
# LOGBOOK = "https://ndubel01.ngt.ndu.ac.jp/elog/genmeta/"
WORKER = "A.Miyamoto"
G2EMAP_URL = "https://ild.ngt.ndu.ac.jp/mc-prod/files2017/genmeta/genmeta-to-elog.map"
GKEY_NOTFOR_ELOG_URL = "https://ild.ngt.ndu.ac.jp/mc-prod/files2017/genmeta/genmeta-notfor-elog.txt"
KNOWN_EVTTYPE_URL = "https://ild.ngt.ndu.ac.jp/mc-prod/files2017/genmeta/known-evttype.txt"

ISCGI = True if os.getcwd().startswith("/home/www/cgi-bin") else False

# ISCGI = True
OUTBUF = []

if ISCGI:
   import pycolor_html
   pyc = pycolor_html.pycolor()
else:   
   import elog
   import pycolor
   pyc = pycolor.pycolor()


##################################################################
def colmsg(color_code, text):
    if color_code == "":
       return text
    else:
       return pyc.c[color_code] + text + pyc.c["end"]

###################################################################
def myprint(msg, rc=0):
    if ISCGI:
       OUTBUF.append(msg)
       if rc != 0:
          return {"OK":False, "out":OUTBUF}
    else:
       print( msg )
       if rc != 0:
          exit(rc)

###################################################################

ERROR = colmsg("red", "ERROR : ")
FATAL = colmsg("purple", "FATAL")
WARNING = colmsg("light red", "WARNING : ")
MESSAGE = colmsg("light blue", "MESSAGE : ")
OK = colmsg("green", "OK : ") + " "
PASSED = colmsg("green", "PASSED : ")
ENDC = pyc.c["end"] + " "


###################################################################
def getWebData():
    ''' Get a map to convert generator meta data to elog attributes field '''
    if hasattr(ssl, '_create_unverified_context'):
            ssl._create_default_https_context = ssl._create_unverified_context

    g2econv = {}
    for line in urlopen(G2EMAP_URL):
        if line[0:1] != "#":
            (gen, elog) = line.replace("\n","").split('=')
            g2econv[gen] = elog
  
    evttype = []
    for line in urlopen(KNOWN_EVTTYPE_URL):
        evttype.append(line.replace("\n",""))

    gkey_notfor_elog = []
    for line in urlopen(GKEY_NOTFOR_ELOG_URL):
        gkey_notfor_elog.append(line.replace("\n",""))

    res = {"g2emap":g2econv, "evttype":evttype, "gkey_notfor_elog":gkey_notfor_elog}

    return res

###################################################################

WEBDATA = getWebData()
G2EMAP = WEBDATA["g2emap"]
KNOWN_EVTTYPE = WEBDATA["evttype"] 
GKEY_NOTFOR_ELOG = WEBDATA["gkey_notfor_elog"]

# pprint.pprint(G2EMAP)


###################################################################
def getAttributes(metafile):
    ''' Get ELOG attributes from generator meta file '''
    global OUTBUF
    # pprint.pprint(G2EMAP)
    fmeta = None 
    if metafile[0:len("http://")] == "http://" or metafile[0:len("https://")] == "https://":
        fmeta = urlopen(metafile)
    else:
        if not os.path.exists(metafile):
            return myprint(ERROR + colmsg("red", "ERROR : ") + "meta file, %s does not exist." % metafile,
                           rc = -1)
        fmeta = open(metafile)

    attrib = {}
    genmeta = {}
    nline = 0
    for inline in fmeta:
        line = inline.replace("\n","").replace("\r","")
        nline += 1
        if len(line.replace(" ","").replace("\n","")) == 0 :
           ret = myprint( MESSAGE + "Empty line found in line#" + str(nline))
           continue
        if "=" not in line:
           ret = myprint( ERROR + "No = in " + line )
        ( key, value ) = line.replace("\n","").split("=",1)
        if value != "":
            genmeta[key] = value

    # A special treatment for Mikael's calibration samples
    if genmeta["machine_configuration"] == "1-calib_IDR":
       ret = myprint( MESSAGE + "A Mikael's machine_configuration data (" + \
                      genmeta["machine_configuration"] + ") is found." )
       ret = myprint( "genmeta's CM_energy_in_GeV and machine_configuration are re-written." )
       ( genmeta["CM_energy_in_GeV"], temp ) = genmeta["machine_configuration"].split("-",1) 
       genmeta["machine_configuration"] = temp

    messages = []
    for key, value in genmeta.items():
        if key in GKEY_NOTFOR_ELOG :
            ret = myprint( MESSAGE + key + " is not recognized as elog key. Will be saved in text area of elog")
            messages.append("=".join([key, value]))
        elif key not in G2EMAP:
            ret = myprint( WARNING + "generator meta key, %s, is not defined for elog. Probably wrong generator meta." % key)
        else:
            attrib[G2EMAP[key]] = value     
   
    if "{" in genmeta["file_names"] and ".." in genmeta["file_names"] and "}" in genmeta["file_names"] :
        filenames = genmeta["file_names"].replace("..","-").replace("{","[").replace("}","]")
        attrib["file_names"] = filenames.replace("[0-0]","0")

    elif ";" in genmeta["file_names"]:  
        filenames = genmeta["file_names"].split(";")
        (fprefix, nser, ftype) = filenames[0].rsplit(".",2)
        (fprefix_last, nser_last, ftype_last) = filenames[-1].rsplit(".",2)
        lser = len(nser)
        if len(filenames) != int(genmeta["number_of_files"]):
            ret = myprint( FATAL + "number_of_files ("+genmeta["number_of_files"] + \
                  ") is not consistent with files defined in file_names field.")
            return myprint( genmeta["file_names"], rc=-1 )

        attrib["file_names"] = "%s.[%d-%d].%s" % ( fprefix, int(nser), int(nser_last), ftype ) 

    attrib["Polarization"] = "e"+genmeta["polarization1"]+"."+ \
                             "p"+genmeta["polarization2"]
    attrib["Polarization"] = attrib["Polarization"].replace(" ","")
    # attrib["\\#Events"] = long(genmeta["total_number_of_events"])
    attrib["NbEvents"] = long(genmeta["total_number_of_events"])
    attrib["xsect_error"] = float(genmeta["cross_section_error_in_fb"])
    attrib["xsect"] = float(genmeta["cross_section_in_fb"])
    attrib["metaname"] = metafile.rsplit('.',1)[0]
    attrib["metapath"] = metafile
    attrib["Energy"] = int(genmeta["CM_energy_in_GeV"])

    datestr=genmeta["job_date_time"].split('-')[0]
    if len(datestr) == 8:
        attrib["JobDate"] = datestr[0:4] + "-"+datestr[4:6] + "-" + datestr[6:8]
    elif len(datestr) == 6:
        attrib["JobDate"] = "20"+datestr[0:2] + "-"+datestr[2:4] + "-" + datestr[4:6]
    else:
        ret = myprint( FATAL + "Date part of job_date_time value is neither 6 nor 8 characters. It was " + datestr)
        return myprint( "JobDate field of elog will be empty", rc = -1 )

    attrib["status"] = "OK"
    procid = genmeta["process_id"]
    attrib["process_id"] = "<a href=\"https://ild.ngt.ndu.ac.jp/CDS/mc-dbd.log/generated/metainfo-id/" + \
	                   "I%s.txt\"><span style=\"color:green\">%s</span></a>" % (procid, procid)

    logurl_orig=genmeta["logurl"]
    if genmeta["logurl"].startswith('"'):
       logurl_orig = genmeta["logurl"][1:]
    if logurl_orig.endswith('"'):
       logurl_orig = logurl_orig[:-2]

    if ";" in logurl_orig:
      ovec=[]
      for url in logurl_orig.split(";"):
         print(url)
         ovec.append("<a href=\"%s\"><span style=\"color:green\">%s</span></a>" % ( url, url) )
      attrib["logurl"] = ", ".join(ovec)
    else:
      attrib["logurl"] = "<a href=\"%s\"><span style=\"color:green\">%s</span></a>" % ( logurl_orig, logurl_orig )

    message = "<br>".join(messages)  if len(messages) > 0 else ""
       
    return {"OK":True, "Attribute":attrib, "genmeta":genmeta, 
            "message":message, "out":OUTBUF }

###################################################################
def check_genmeta(genmeta):
    ''' check genmeta data '''

    global OUTBUF
    if ISCGI:
       OUTBUF += ["Doing second step, check_genmeta ..."]
    
    primary_key = ["total_number_of_events", "CM_energy_in_GeV", "luminosity", 
		   "machine_configuration", "file_names", "fileurl", "logurl", 
		   "number_of_events_in_files", "number_of_files", "process_id", 
		   "process_names", "process_type", "cross_section_in_fb", 
		   "beam_particle1", "beam_particle2","polarization1", "polarization2"] 
    
    prodpara = {"simconfig":"v02-00-01", "recconfig":"v02-00-01", "model":"ILD_l5_o1_v02",
                "fseq":999, "prodid":12345, "jobid":99999}

    
    for k in primary_key:
        if k not in genmeta:
            ret = myprint( ERROR + "key " + k + " not found in meta file.")

    enemachine = genmeta["CM_energy_in_GeV"] + "-" + genmeta["machine_configuration"]
    if enemachine not in ["1-calib_IDR", "1-calib", "91-nobeam", "250-TDR_ws", 
                          "250-SetA", "350-TDR_ws", "500-TDR_ws", "550-Test", "1000-B1b_ws"]:
        ret = myprint( ERROR + "Energy-MachineConfiguration " + enemachine + " is not recognized." )

    process_type = genmeta["process_type"]
    if "{" in genmeta["file_names"] and ".." in genmeta["file_names"] and "}" in genmeta["file_names"]:
       files = list(braceexpand.braceexpand(genmeta["file_names"]))
    else:
       files = genmeta["file_names"].split(";")
    nb_files = int(genmeta["number_of_files"])
    if nb_files > 1:
        if len(files) != nb_files:
            ret = myprint( ERROR + "number_of_files(%d) and file_names(%d) is not consistent." % (nb_files, len(files)))
        filename = files[0]
    else:
        filename = files[0]

    fkeys = {}
    for k in filename.split("."):
        fkeys[k[0:1]] = k[1:]

    if fkeys["E"] != enemachine:
        ret = myprint( ERROR + "filename and CM_energy_in_GeV or machine_configuration is not consistent")
        ret = myprint( "CM_energy_in_GeV=" + genmeta["CM_energy_in_GeV"])
        ret = myprint( "machine_configuration=" + genmeta["machine_configuration"])
        ret = myprint( "filename=" + filename)
    else:
        ret = myprint( PASSED + "enery and machinepara check." )
        
    if fkeys["P"] != genmeta["process_names"]:
        ret = myprint( ERROR + "filename and process_names is not consistent.")
        ret = myprint( "process_names=" + genmeta["process_names"])
        ret = myprint( "filename=" + filename)
        ret = myprint( "fkeys[P]=" + fkeys["P"])
    else:
        ret = myprint( PASSED + "process_names check.")

    ePol = (genmeta["beam_particle1"] + genmeta["polarization1"]).replace(" ","")
    pPol = (genmeta["beam_particle2"] + genmeta["polarization2"]).replace(" ","")

    if ePol not in ["e1L", "e1R", "AB", "AW","e10"] or pPol not in ["E1L", "E1R", "AB", "AW","E10"]:
        ret = myprint( FATAL + "unknown beam_paraticle1/2 or polarization1/2")
        ret = myprint( "ePol=" + ePol + " pPol=" + pPol)
        ret = myprint( "beam_particle1=" + genmeta["beam_particle1"])
        ret = myprint( "beam_particle2=" + genmeta["beam_particle2"])
        ret = myprint( "polarization1=" + genmeta["polarization1"])
        return myprint( "polarization2=" + genmeta["polarization2"], rc=20)

    if genmeta["polarization1"] != fkeys["e"] or genmeta["polarization2"] != fkeys["p"]:
        ret = myprint( ERROR + "polarization1/2 not consistent with the file name")
        ret = myprint( "polarization1=" + genmeta["polairzation1"])
        ret = myprint( "polarization2=" + genmeta["polarization2"])
        ret = myprint( "filename=" + filename)
    else:
        ret = myprint( PASSED + "polarization values check.")
    
    fileurl = genmeta["fileurl"].replace("lfn:/grid","").replace("dirac:/ilc","/ilc").replace("lfn:/ilc/prod/ilc","/ilc/prod/ilc")

    if fileurl[0:len("/ilc/prod/ilc")] != "/ilc/prod/ilc":
        ret = myprint( WARNING + " fileurl does not start from neither /ilc/prod/ilc, " + \
              "lfn:/ilc/prod/ilc, lfn:/grid/ilc/prod/ilc nor dirac:/ilc")
    # if not fileurl.endswith('/'):
    #    ret = myprint( WARNING + " fileurl DOES NOT ends with /")

    tempurl = fileurl[:-1] if fileurl.endswith('/') else fileurl 
    genbase = '/'.join(tempurl.split('/')[-2:]) + '/'
    evtclass = tempurl.split('/')[-1]
    process_type = genmeta["process_type"]
    evttype = process_type

# Special condition to define EvtType
    if "_" in process_type:
       if process_type.startswith("np-"):
          evttype = process_type.split("_")[0]
       elif process_type.split("_")[0] == "2f-JER":
          evttype = "2f-JER"
    if process_type in ["500"]:
       evttype = evtclass
    if evtclass == "aa_lowpt":
       evttype = evtclass +"_"+genmeta["polarization1"]+genmeta["polarization2"]

    ret = myprint( MESSAGE + "process_type=" + colmsg("green", process_type) + \
             ", EvtClass=" + colmsg("green", evtclass) + ", Evttype=" + colmsg("green",  evttype))

#    evtclass = process_type if "_" not in process_type else process_type.split("_")[0]
#    if "_production" in evtclass:
#        print MESSAGE + "'_production' in process_type was dropped for EvtClass."
#        evtclass = evtclass.replace("_production","")

#    if evtclass[0:3] == "np-" :
#        evtclass = genmeta["process_type"].split("_")[0]
#        print MESSAGE + "process_type begin with \"np-\". Data sub-directory will be " + evtclass
#    if "_" in genmeta["process_type"]:
#        pregenm = genmeta["process_type"].split("_")[0]
#        if pregenm in ["2f-JER"]: 
#            evtclass = pregenm
#            print MESSAGE + "process_type begin with \"" + pregenm + "\". Data sub-directory will be " + evtclass

#    process_type = genmeta["process_type"]
#    gendir = enemachine + "/" + evtclass + "/"
#    if gendir != genbase:
#        print ERROR + "energy-machinepara or process_type is not consistent with last two subdirectory of fileurl."
#        print "Energy-MachinePara=" + enemachine
#        print "process_type=" + process_type
#        print "fileurl=" + genmeta["fileurl"]
#        print "Last two subdirectory level expected by Energy-MachinePara and process_type = " + gendir
#        print "Last two subdirectory of fileurl = " + genbase
#        print "**** These two subdirectory name should be equal."
#    else:
#        print PASSED + genbase + " check"

    if genmeta["process_names"] == genmeta["process_type"]:
        ret = myprint( WARNING + "process_names and process_type is same. Consider to use the different text.")

#    evttype = genmeta["process_type"]
#    if evttype == "aa_lowpt":
#        evttype = evtclass+"_"+genmeta["polarization1"]+genmeta["polarization2"]

    ret = myprint( MESSAGE + "EvtType (Sub-directory for sim/rec ) is "+pyc.c["green"]+evttype+pyc.c["end"])
    if evttype not in KNOWN_EVTTYPE:
        ret = myprint( WARNING + "New directory," + \
           pyc.c["light red"] + evttype + pyc.c["end"] + " will be created in sim/rec directory. Are you sure ?")

    prodpara = {"simconfig":"v02-00-01", "recconfig":"v02-00-01", "smodel":"ILD_l5_v02", "rmodel":"ILD_l5_o1_v02",
                "seq":1, "subseq":999, "prodid":12345, "jobdir":"099", "jobid":99999, 
                "topdir": "/ilc/prod/ilc/mc-opt-3/ild"}

    simfilefmt = "s%s.m%s.E%s.I%d.P%s.e%s.p%s.n%s.d_sim_%8.8d_%d.slcio"
    dirfmt  = "%s/%s/%s/%s/%s/%s/%s/%s"
    recfilefmt = "r%s.s%s.m%s.E%s.I%d.P%s.e%s.p%s.n%s.d_rec_%8.8d_%d.slcio"
  
    (genpref, gseq, gftype ) = filename.rsplit('.',2)
    seqfmt = "%%%d.%dd_%%d" % ( len(gseq), len(gseq) )    
    simseq = seqfmt % ( prodpara["seq"], prodpara["subseq"] )
    simfile = simfilefmt % ( prodpara["simconfig"], prodpara["smodel"], enemachine, 
                             int(genmeta["process_id"]), genmeta["process_names"], 
                             genmeta["polarization1"], genmeta["polarization2"], 
                             simseq, prodpara["prodid"], prodpara["jobid"]) 
    recfile = recfilefmt % ( prodpara["recconfig"], prodpara["simconfig"], prodpara["rmodel"],
                             enemachine, int(genmeta["process_id"]), genmeta["process_names"], 
                             genmeta["polarization1"], genmeta["polarization2"], 
                             simseq,  prodpara["prodid"], prodpara["jobid"]) 
    dtype = "sim"
    simdir  = dirfmt % ( prodpara["topdir"], dtype, enemachine, evttype, 
                         prodpara["smodel"], prodpara["simconfig"], 
                         prodpara["prodid"], prodpara["jobdir"] )
    dtype = "rec"
    recdir  = dirfmt % ( prodpara["topdir"], dtype, enemachine, evttype, 
                         prodpara["rmodel"], prodpara["recconfig"], 
                         prodpara["prodid"], prodpara["jobdir"] )
 
    ret = myprint( " ")   
    ret = myprint( "Simulation data: dir=" + str(len(simdir)) + " chars , File name=" + \
        pyc.c["light blue"]+str(len(simfile)) + pyc.c["end"] + " chars." )
    ret = myprint( "directory=" + simdir )
    ret = myprint( "sim_file=" + simfile)
    if len(simdir) > 127 or len(simfile) > 127:
       return myprint( FATAL + "character length of directory or filename exceeds 127 characters limit", rc=20)
 
    ret = myprint( "Reconstruction data: dir=" + str(len(recdir)) + " chars , File name=" + \
              pyc.c["light blue"]+str(len(recfile)) + pyc.c["end"] + " chars." )
    ret = myprint("directory=" + recdir)
    ret = myprint("rec_file=" + recfile)
    if len(recfile) > 122:
       ret = myprint( WARNING + "rec filename length may hit the limit, if rec with _nobg, for example.")
    elif len(recdir) > 125 or len(recfile) > 127:
       return myprint(FATAL + "character length of directory or filename exceeds 127 characters limit", rc=19)
    else:
       ret = myprint( PASSED + "character length of file names." )

    if ISCGI:
       OUTBUF += ["#####################",
            "<font color=\"green\">All check completed.</font>"]
       return OUTBUF



#####################################################
# tools taken from make_json.py
#####################################################

#import os
#import glob
#import pprint
#import json
#import argparse

#GENMETA_DIR=""
#_metaByID = {}
#_metaByFile = {}
_print_level=0

# ==============================================================
def genmetaDict(intxt):

    basename = os.path.basename(intxt)
    arec = {}
    arec["meta_file"] = basename
    for line in open(intxt):
      if "=" in line:
        (key,value) = line.replace('\n','').replace('\r','').split('=',1)
        arec[key] = value

    if "," in arec["file_names"]:
      arec["file_names"] = arec["file_names"].replace(",",";")
    if "," in arec["number_of_events_in_files"]:
      arec["number_of_events_in_files"] = arec["number_of_events_in_files"].replace(",",";")

    if "process_name" in arec:
      arec["process_names"] = arec["process_name"]
      if _print_level >= 2:
          print( "process_names for " + intxt + " added, because process_name is used." )

    if arec["CM_energy_in_GeV"] == "":
       (temp1, temp2) = arec["machine_configuration"].split("-",1)
       arec["CM_energy_in_GeV"] = temp1
       arec["machine_configuration"] = temp2


    if arec["fileurl"] == "":   
       evtype = arec["process_type"]
       if "_" in evtype:
         (evtype, dummy ) = arec["process_type"].split('_',1)
       arec["fileurl"] = "/ilc/prod/ilc/mc-dbd/generated/%s-%s/%s/" % \
	( arec["CM_energy_in_GeV"], arec["machine_configuration"], evtype )
       if _print_level >= 2:
           print( "fileurl was empty and added based on the convention, " + arec["fileurl"] )
    # pprint.pprint(arec)
    return arec

# ==============================================================
def load_metafile(target, metajson={"byid":{}, "byfile":{}}):
  
  for intxt in glob.glob(target):
      # print intxt
      arec = genmetaDict(intxt)
      procid = arec["process_id"]      

      if procid in metajson["byid"]:
         print( "ERROR : procid =" + str(procid) + " exists already. Neglected "+intxt )
      else:
      #   _metaByID[procid] = arec
      #   _metaByFile[arec["meta_file"]] = arec
         metajson["byid"][procid] = arec
         metajson["byfile"][arec["meta_file"]] = arec

  return metajson

# ==============================================================
def make_clean_genmeta(genmeta_dir):
  metajson = {"byid":{}, "byfile":{}}
  metajson = load_metafile(genmeta_dir+"/newMeta/*.txt", metajson)
  indirs = ["91-nobeam", "250-TDR_ws", "350-TDR_ws", "500-TDR_ws", "1000-B1b_ws"]
  for adir in indirs:
      metajson = load_metafile(genmeta_dir+"/fromDESY/genmeta/"+adir+"/*/*.txt", metajson)
  metajson=load_metafile(genmeta_dir+"/fromDESY/genmeta/1-calib/*.txt", metajson)

  metajson = load_metafile(genmeta_dir+"/byTino/*.txt", metajson)
  
  indirs = ["1-calib_IDR", "250-SetA", "91-nobeam", "500-TDR_ws", "550-Test"]
  for adir in indirs:
      metajson = load_metafile(genmeta_dir+"/mc-opt.dsk/generated/"+adir+"/*/*.txt", metajson)
      metajson = load_metafile(genmeta_dir+"/mc-2020/generatorlog/"+adir+"/*/*.txt", metajson)

  load_metafile(genmeta_dir+"/*.txt", metajson)


  return metajson

