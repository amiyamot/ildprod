
from openpyxl import Workbook
from openpyxl import load_workbook

##########################################################
def load_excel_sheets(excel_file, sheets):
    ''' Load sheets from a workbook and return as a dict object.
        Each column of first row is key of dictionary object.
        Rest row is stored as a dict object, with "keycol" value as a key
    '''
    wbout = {}
    wb = load_workbook(excel_file, data_only=True)
    print( "Reading " + excel_file )

    for sheet in sheets:
        sn = sheet["sheet"]
        keycol = sheet["keycol"]

        print( "Loading " + sn+" seet with "+keycol+ " as a key" )

        ws = wb[sn]
        ncols = ws.max_column
        nrows = ws.max_row

        keys = []
        ickey = 0
        for ic in range(0, ncols):
            colname = ws.cell(row=1, column=ic+1).value
            if colname == keycol:
                ickey = ic
            keys.append(colname)

        wbout[sn] = {}
        for ir in range(1, nrows):
            aline = {}
            for ic in range(0, ncols):                 
                aline[keys[ic]] = ws.cell(row=ir+1, column=ic+1).value

            ind = ws.cell(row=ir+1, column=ickey+1).value
            if ind in wbout[sn]:
               print( "Warning : Duplicated entry : "+str(ind)+" detected. latter entry is neglected." )
            else: 
               wbout[sn][ws.cell(row=ir+1, column=ickey+1).value] = aline

    wb.close()
    return wbout


