####################################
#
# A very first samples to create a splitted-lcio file from a stdhep file, 
# adding header informations.
#
#####################################
# import math
# import random
import re
import os
import pprint
import copy
import subprocess
import ROOT
ROOT.gROOT.SetBatch()
import pycolor
import braceexpand

pyc=pycolor.pycolor()

# ############################################################################
def get_list_of_fileseq(files):
  # Get a list of file_seq numbers from a list of filenames.
  seqlist = []
  if "{" not in files and ".." not in files and "}" not in files:
     seqlist.append(int(f.split(".")[-2]))

  else:
     expword=files.replace("..","-").split(".")[-2]
     seqstr=list(braceexpand.braceexpand(expword))
     for i in seqstr:
        seqlist.append(int(i))
  
  return seqlist

# ############################################################################
def decodeFilename( fullpath, separator="." ):
  '''

  Decode a file name to Key and Value according to the DBD file name convention.
  File name is splitted by ".", each item is decoded assuming it consits of
  1 character of key followed by key value.  Exception seen in DBD generator
  files are also handled. Only basename of fullpath is used, even if direcories
  are included in fullpath.

  :param str fullpath: File name in fullpath is decoded.
  :returns: returns a dict object containing key and its value.

  '''

  filename = os.path.basename( fullpath )
  if filename.count('_gen_'):
    ftemp    = re.sub(r'([0-9a-zA-Z])_gen_([0-9]+_[0-9]+_[0-9]+).(stdhep|slcio)', r'\1.d_gen_\2.\3', filename) # Special treatment for ILDDirac old files.
  else:
    ftemp    = re.sub(r'-(\d+).slcio', r'.j\1.slcio', filename)  # Special treatment for DBD sim files.
    ftemp    = re.sub(r'([0-9a-zA-Z])_(sim|rec|dst)_(\d+_\d+).slcio', r'\1.d_\2_\3.slcio', ftemp) # Special treatment for ILDDirac old files.

  replaceList=[ ["Gwhizard-1.95", "Gwhizard-1_95"] ]
  for replacement in replaceList:
    ftemp=ftemp.replace(replacement[0], replacement[1])

  (fbody, ftype) = ftemp.rsplit('.',1)
  ftemp = fbody + ".F" + ftype

  filemeta = {}
  for token in ftemp.split(separator) :
    conv=re.sub(r'^(\d)',r'n\1',token)

    key=conv[0:1]
    value=conv[1:]
    if key == "E":
      if value[0:1] == "0":
        value=value[1:]
    if key == "d" :  # special treatment for old file names with prodID and job number
      if value[0:1] == "_":
        dsplit = value[1:].split('_')
        value=dsplit[0]
        filemeta['t'] = dsplit[1]
        if len(dsplit) == 4 and value == 'gen':
          filemeta['n'] = '%3.3d_%3.3d' % ( int(dsplit[2]), int(dsplit[3]) )
        else:
          filemeta['j'] = dsplit[2]

    filemeta[key]=value

  return filemeta



# ############################################################################
def copyObjectParameters( obj, copyObj ):
    ''' Copies the parameters of an LCObject to another LCObject'''
    from ROOT import vector

    parameters = obj.getParameters()
    copyParameters = copyObj.parameters()
    keyVec = vector('string')()
    for intKey in parameters.getIntKeys( keyVec ):
        intVec = vector('int')()
        copyParameters.setValues( intKey, parameters.getIntVals( intKey, intVec ) )
    keyVec.clear()
    for floatKey in parameters.getFloatKeys( keyVec ):
        floatVec = vector('float')()
        copyParameters.setValues( floatKey, parameters.getFloatVals( floatKey, floatVec ) )
    keyVec.clear()
    for stringKey in parameters.getStringKeys( keyVec ):
        stringVec = vector('string')()
        copyParameters.setValues( stringKey, parameters.getStringVals( stringKey, stringVec ) )

# ############################################################################
def formatVecValues( vec ):

    if len( vec ) == 0:
        return ' [empty]'
    string = ''
    for val in vec:
        string += '%s, ' % ( val )
    return string


# #############################################################################
def printParameters( parameters ):
    from ROOT import vector
    ''' Helper method to print the content of an LCParameters object '''
    keyVec = vector( 'string' )()
    for intKey in parameters.getIntKeys( keyVec ):
        intVec = vector( 'int' )()
        parameters.getIntVals( intKey, intVec )
        print( ' parameter %s [int]: %s' % ( intKey, formatVecValues( intVec ) ) )
    keyVec.clear()
    for floatKey in parameters.getFloatKeys( keyVec ):
        floatVec = vector( 'float' )()
        parameters.getFloatVals( floatKey, floatVec )
        print( ' parameter %s [float]: %s' % ( floatKey, formatVecValues( floatVec ) ) )
    keyVec.clear()
    for stringKey in parameters.getStringKeys( keyVec ):
        stringVec = vector( 'string' )()
        parameters.getStringVals( stringKey, stringVec )
        print( ' parameter %s [string]: %s' % ( stringKey, formatVecValues( stringVec ) ) )

# ############################################################################
def Stdhep2LcioSplit( params ):
  '''
  Main function to read a stdhep file and output splitted lscio file.
  params is an dict object containing following data

  params["input_file"] = full path of input stdhep file.  
     "input_file" name format should be, prefix.XXX.stdhep 
  params["outdir"] = output directory. No "/" at the end 
  params["nb_records_per_file"] = Number of records per output file.
  params["maxread"] = number of input records. <0 to the EOF.
  params["skip_record"] = number of records to skip before output. Default=0
  params["genmeta"] = Generator meta data. dict object containing a record. Following information is mandatory.
     process_id, process_names, cross_section_in_fb, cross_section_error_in_fb, 
     number_of_events_in_files ( ";" separated numbers )
     beam_particle1,beam_particle2,polarization1,polarization2  
  '''
  # --- LCIO dependencies ---
  from pyLCIO import UTIL, EVENT, IMPL, IO, IOIMPL
  from pyLCIO.io import LcioReader, StdHepReader

  import sys
  sys.stdout = sys.stderr

  maxread = long(params["maxread"])
  outdir = params["outdir"]
  nb_records_per_file = int(params["nb_records_per_file"])
  nb_skip_records = long(params["skip_records"])
  genmeta = params["genmeta"]
  infile = str(params["input_file"])
  nbfiles = int(params["nbfiles"])
  if not os.path.exists(infile):
    print( "### ERROR : input_file does not exist : " + infile)
    exit(10)
  basename = str(os.path.basename(infile))
  (opref, nser, ftype ) = basename.rsplit('.',2)
  if nser[0:1] == "n":
     nser = nser[1:]
  fkeys = decodeFilename(basename)
  pprint.pprint(fkeys)
  gen_process_id = fkeys["I"]
  if gen_process_id != genmeta["process_id"]:
    print( "### ERROR : ProcessID " + gen_process_id + " in genmeta "+str(genmeta["process_id"]) + " does not agree.")
    exit(9)
  
  cross_section_in_fb = float(genmeta["cross_section_in_fb"])
  cross_section_error_in_fb = float(genmeta["cross_section_error_in_fb"])
  gen_process_name = genmeta["process_names"] 
  if "*" in genmeta["number_of_events_in_files"]:
    ( kevts, kfile ) = genmeta["number_of_events_in_files"].split("*")
    vec_nb_events = [long(kevts)]*long(kfile)
  else:
    vec_nb_events = genmeta["number_of_events_in_files"].split(';')
  event_number_offset = 0L
  if nser > 1:
    for i in range(0, int(nser)-1):
      event_number_offset += long(vec_nb_events[i])

  if outdir != None:
    if not os.path.exists(outdir):
      os.mkdir(outdir)
      print("Output directory, " + outdir + " was created.")
  
  # --- Do event loop and write events, splitting file
  oflist = []

  nofile = 0
  nb_records = 0L  
  nb_records_total = 0L
  print("Input file to StdHep/LcioRead="+infile+"----")
  if not os.path.exists(infile):
     print(infile+" not found")
     exit(-1)

  if ftype == "stdhep":
      reader=StdHepReader.StdHepReader(infile)   
  elif ftype == "slcio":
      reader=LcioReader.LcioReader(infile)
  else:
      print( "Unknown file type was requested. File was " + infile )
      exit(-1)

  nb_skipped = 0L
  nofile_first = -1
  writer = None
  file_event_no_first = -1
  for idx, event in enumerate(reader):
    event_number = int( event.getEventNumber() )
    # print "idx="+str(idx) + " event_number="+str(event_number)
    if maxread > 0L and idx >= maxread:  # idx starts from 0
      break
    
    if nb_skipped < nb_skip_records:
      nb_skipped += 1L
      continue
    
    nb_records += 1L
    if file_event_no_first < 0:
      file_event_no_first = event_number
      process_event_no_first = long( event_number ) + long( event_number_offset )
      nofile_first = nofile + 1
    run_number = long( event.getRunNumber() )
    if run_number == 0L:
      # run_number = long( "%s%4.4d" % ( fkeys['I'], int(nser) ) )
      run_number = long( fkeys['I'] )

    if not writer:
      if nbfiles > 1 and nofile >= nbfiles: 
         print( "nofile="+str(nofile)+" exceeds nbfiles="+str(nbfiles) )
         break

      nofile = nofile +1
      outfile = opref + ".n%3.3d_%3.3d.slcio" % ( int(nser), int(nofile) )
      if outdir != None:
        outfile = outdir + "/" + outfile

      print( "Run#:%s,Evt#:%s,Split#:%s,File=%s" % ( str(run_number), str(event_number), str(nofile), str( os.path.basename(outfile) ) ))

      writer = IOIMPL.LCFactory.getInstance().createLCWriter()
      writer.open(str(outfile), EVENT.LCIO.WRITE_NEW)

      wrun = IMPL.LCRunHeaderImpl()
      wrun.setRunNumber( long(run_number) )
      wrun.setDetectorName( event.getDetectorName() )

      writer.writeRunHeader( wrun )
      ofinfo = {}
      ofinfo["file_name"] = str( os.path.basename(outfile) )
      ofinfo["split_number"] = int(nofile)
      ofinfo["EventNumberFirst"] = event_number 
      ofinfo["GenProcessEventNumberFirst"] = long( event_number ) + long( event_number_offset ) 

            
    # printParameters( parameters )

    wevt = IMPL.LCEventImpl()
    copyObjectParameters( event, wevt )

    wevt.setRunNumber( long(run_number) )
    gen_event_number = long(event_number) + long(event_number_offset)
    wevt.setEventNumber( long(gen_event_number) )  
    wevt.setDetectorName( event.getDetectorName() )
    wevt.setTimeStamp( event.getTimeStamp() )

    wevt_params = wevt.getParameters()
    # printParameters(wevt_params)
    (energy, machine ) = fkeys['E'].split("-")

    keyVec = ROOT.vector('string')()

    beampid = { "A":22, "e1":11, "E1":-11 }
    
    wevt_params.setValue("beam_particle1", str(genmeta["beam_particle1"]))
    wevt_params.setValue("beam_particle2", str(genmeta["beam_particle2"]) )
    wevt_params.setValue("polarization1", str(genmeta["polarization1"]))
    wevt_params.setValue("polarization2", str(genmeta["polarization2"]))
    wevt_params.setValue("GenFileSerialNumber", int(nser))
    wevt_params.setValue("GenFileSplitNumber", int(nofile))

    if "Energy" not in wevt_params.getFloatKeys( keyVec ):  
        wevt_params.setValue("Event Number", long(gen_event_number))
        wevt_params.setValue("ProcessID", long(run_number))
        wevt_params.setValue("Run ID", long(run_number))
        wevt_params.setValue("Energy", float(energy) )
        wevt_params.setValue("crossSection", float(cross_section_in_fb) )
        wevt_params.setValue("crossSectionError", float(cross_section_error_in_fb) )
        wevt_params.setValue("BeamSpectrum", str(machine) )
        wevt_params.setValue("processName", str(gen_process_name))
    
        for pind in ["1", "2"]:
            polstr = str(genmeta["polarization"+pind])
            polstr0 = polstr[0:1]
            if polstr0 == "L":
               pol = -1.0 if len(polstr) == 1 else -1.0*float(polstr[1:])/100.0
            elif polstr0 == "R":
               pol = 1.0 if len(polstr) == 1 else float(polstr[1:])/100.0
            elif polstr0 == "W" or polstr0 == "B":
               pol = 0.0
            elif polstr0 == "0":
               pol = 0.0
            else:
               print( pyc.c["red"]+"ERROR"+pyc.c["end"] +
                      "polarization"+pind+" in genmeta, "+ genmeta["polarization"+pind] + " can not decode " )
               exit(-1)
            wevt_params.setValue("Pol%d" % (int(pind)-1), float(pol) )

            beam = str(genmeta["beam_particle"+pind]).replace(" ","")
            wevt_params.setValue("BeamPDG%d" % (int(pind)-1), int(beampid[beam]))

    else:
      for k in ["Event Number", "ProcessID", "Run ID"]:
          if k not in wevt_params.getIntKeys( keyVec ):
              print( pyc.c["green"] + "Warning"+pyc.c["end"] +
                     " event parameter "+k+ " not defined in the input file." )

      for k in ["crossSection", "crossSectionError"]:
          if k not in wevt_params.getFloatKeys( keyVec ):
              print( pyc.c["green"] + "Warning"+pyc.c["end"] + 
                     " event parameter "+k+ " not defined in the input file." )

      for k in ["BeamSpectrum", "processName"]:
          if k not in wevt_params.getStringKeys( keyVec ):
              print( pyc.c["green"] + "Warning"+pyc.c["end"] +
                     " event parameter "+k+ " not defined in the input file." )

    
    for colname in event.getCollectionNames():
      col = event.getCollection( colname )    
      colout = copy.deepcopy(col)
      wevt.addCollection( colout, colout.getTypeName() )
       
    writer.writeEvent( wevt )

    if nb_records >= nb_records_per_file:
      ofinfo["EventNumberLast"] = event_number
      ofinfo["NbRecordsWritten"] = nb_records
      ofinfo["GenProcessEventNumberLast"] = long(event_number) + long(event_number_offset) 
      oflist.append(ofinfo)
      nb_records_total += nb_records
      writer.flush()
      writer.close()
      # del writer
      writer = None
      nb_records = 0

  if nb_records > 0 and writer:
    ofinfo["EventNumberLast"] = event_number  
    ofinfo["NbRecordsWritten"] = nb_records
    oflist.append(ofinfo)
    pprint.pprint(ofinfo)
    nb_records_total += nb_records
    writer.flush()
    writer.close()

  ret = {"OK":True, "RunNumber":run_number, "OutDir":outdir, 
	 "FileEventNumberLast":event_number, "NbWrittenRecordTotal":nb_records_total, "OutFiles":oflist} 

  ret["FileEventNumberFirst"] = file_event_no_first
  ret["ProcessEventNumberFirst"] = process_event_no_first
  ret["ProcessEventNumberLast"] = long(event_number) + long(event_number_offset)
  ret["InputFile"] = basename
  ret["GenProcessID"] = gen_process_id
  ret["GenFileNumber"] = nser
  ret["FileSplitNumberLast"] = nofile
  ret["FileSplitNumberFirst"] = nofile_first
  ret["genmeta"] = genmeta

  return ret
  
# ##########################################################################
def GetFilename_by_nseq(ameta, iseq):
  ''' 
  Get a file name from meta data and seq number.
  seq number is the one attached to the input file names 
  and could be start from 0 or 1.
  '''
  
  seqlist = []
  if "{" not in files and ".." not in files and "}" not in files:
     seqlist.append(int(f.split(".")[-2]))

  else:
     expword=files.replace("..","-").split(".")[-2]
     seqstr=list(braceexpand.braceexpand(expword))
     for i in seqstr:
        seqlist.append(int(i))
  
  return seqlist



# ##########################################################################
def GetFilenames(ameta):
  ''' Get a list of file names from genmeta '''

  filenames = []
  files = ameta["file_names"]
  if "{" in files or ".." in files or "}" in files:
     filenames = list(braceexpand.braceexpand(files))
     print("Generated filenames from " + files)
     print("First file is "+filenames[0])
     print("Last file is  "+filenames[-1]) 
  else:
     filenames = ameta["file_names"].split(";")

  if len(filenames) != int(ameta["number_of_files"]):
     print( "Warning : Number of file names available in genmeta is not consistent with genmeta[number_of_files]" )
     print( "filenames array has to be created automatically as follows." )
     print( "file_names data in genmeta is" )
     print( ameta["file_names"] )
     fntokens = filenames[0].split('.')
     lnser = len(fntokens[-2])
     iser = int(fntokens[-2])
     nfmr = "%%%d.%dd" % ( lnser, lnser ) 
     for i in range(0, int(ameta["number_of_files"])):
        fnnew = ".".join(fntokens[:-2]) + "." + nfmr % int(iser) + "." + fntokens[-1]
        filenames.append(fnnew)
        iser += 1
     print( "Generated file names are " )
     pprint.pprint(filenames)
  
  return filenames

# ############################################################################
if __name__ == '__main__':

  import json
  import argparse
  import socket
  import os

  runhost = socket.gethostname()
  (shost, nzone, cname ) = runhost.rsplit(".",2)
  default_meta = os.environ["GENMETA_JSON"] if "GENMETA_JSON" in os.environ \
	else "/group/ilc/users/miyamoto/optprod/json/mc-dbd.log.genmeta/genmetaByID.json"

  default_base = "/group/ilc/grid/storm"
  rundomain = nzone + "." + cname
  if rundomain == "desy.de":
      default_meta = os.environ["GENMETA_JSON"] if "GENMETA_JSON" in os.environ \
        else os.environ["HOME"] + "/genmetaByID.json" 
      default_base = "/pnfs/desy.de/ilc"

  parser = argparse.ArgumentParser(description="Read stdhep file and create splitted lcio files adding header information. Input file is decided by process_id and file_seq given as arguments, unless other information is given by optional parameters.")
  parser.add_argument("proccess_id", help="Process ID of stdhep file", nargs='?', default=0)
  parser.add_argument("file_seq", help="File number of stdhep file", nargs='?', default=1)
  parser.add_argument("-n", help="number of records per file.", nargs='?', type=str, dest="nrec_per_file", default='100')
  parser.add_argument("-N", help="Number of file to produce", nargs='?', type=str, dest="nb_files", default='0')
  parser.add_argument("-m", help="Max number of records to read", nargs='?', type=str, dest="maxread", default='-1')
  parser.add_argument("-s", help="Number of records to skip", nargs='?', type=str, dest="nskip", default='0')
  parser.add_argument("-o", help="Out put directory", nargs='?', dest="outdir", default="gensplit")
  parser.add_argument("-f", help="Input file name (Full LFN)", nargs='?', dest="inputlfn", default="")
  parser.add_argument("-d", help="Input data directory", nargs='?', dest="datadir", default=None)
  parser.add_argument("-j", help="Json file of generator meta file", nargs=1, dest="genmeta_json", default=[default_meta])
  parser.add_argument("-t", help="Do test run.", dest="do_test", action="store_true", default=False)
  parser.add_argument("-l", help="A directory to output job result", nargs=1, dest="outjson", default="lciosplit.json")
  parser.add_argument("-b", help="Local base path", nargs=1, dest="local_base_path", default=[default_base])
  parser.add_argument("-v", help="Verbose mode. Printout more messages", dest="verbose", default=False, action="store")
  parser.add_argument("--prod_json", help="A file, production.json, where to get production parameter", 
                       nargs="?", action="store", dest="prod_json", default="")
  parser.add_argument("--option_file", help="A file to get optional data in gensplit step", 
                       nargs="?", action="store", dest="option_file", default="")

  args = parser.parse_args()
  params = {}
  procid = args.proccess_id
  nser = args.file_seq
  genmeta_json = args.genmeta_json[0]
  outjson = args.outjson
  localbase = args.local_base_path[0]
  nbfiles = args.nb_files
  inputlfn = args.inputlfn
  prod_json = args.prod_json
  option_file = args.option_file
  nrec_per_file = int(args.nrec_per_file)
  maxread = int(args.maxread)
  nskip = int(args.nskip)
  datadir = args.datadir
  verbose = args.verbose

  if prod_json != "":
      prodjson = json.load(open(prod_json))
      nrec_per_file = long(prodjson["__SPLIT_NW_PER_FILE"])
      maxread = long(prodjson["__SPLIT_MAX_READ"])
      nskip = long(prodjson["__SPLIT_SKIP_NEVENTS"])
      nbfiles = long(prodjson["__SPLIT_NB_FILES"])
      datadir = prodjson["__STDHEP_LOCALBASE"]     

  print("local_base_path=" + str(localbase))
  print("datadir ="+str(datadir))
  print("genmeta_json = " + genmeta_json)

  print("nrec_per_file = %d " % int(nrec_per_file))
  print("maxread = %d " % int(maxread))
  print("nskip = %d " % int(nskip))
  print("nbfiles = %d " % int(nbfiles))

  params["nbfiles"] = nbfiles 
  params["outdir"] = args.outdir if not args.do_test else "gensplit"
  params["nb_records_per_file"] = long(nrec_per_file) if not args.do_test else 10
  params["maxread"] = long(maxread) if not args.do_test else 30
  params["skip_records"] = long(nskip) if not args.do_test else 0

  if option_file != "":
      trmap = {"nw_per_file":"nb_records_per_file", "maxread":"maxread", "nskip":"skip_records",
               "nbfile":"nbfiles" }
      for line in open(option_file):
          (k,v)=line.replace("\n","").split("=",1)
          if k in trmap:
              print "Reset " + trmap[k] + "=" + str(v)
              params[trmap[k]] = v 


  genmeta_full = json.load(open(genmeta_json))

  # params["datadir"] = None
  datadir = None
  if args.do_test:
      srclfndir = "/ilc/prod/ilc/mc-dbd/generated/500-TDR_ws/6f"
      datadir = "/group/ilc/grid/storm/prod/ilc/mc-dbd/generated/500-TDR_ws/6f"
      procid = 108683
      thismeta = genmeta_full[str(procid)]
      params["genmeta"] = thismeta
      print("-- input_file name is decided as test mode.") 

  # if -f option is used ( inputlfn is defined ) use this file for input.
  elif inputlfn != "":
      localpath = inputlfn.replace("/ilc/prod/ilc/","%s/prod/ilc/" % localbase )
      localdir = os.path.dirname(localpath)
      localbase = os.path.basename(localpath)

      if verbose:
          print("inputlfn="+inputlfn)
	  print("localdir="+localdir)
          print("localbase="+localbase)

      fkeys = decodeFilename(localbase)  # Get procID and nser from the filename
      procid = fkeys["I"]
      nser = fkeys["n"]

      print(" nser is decided from fkeys[n], which is "+str(nser)+" args.nser="+str(args.file_seq))

      if procid not in genmeta_full:
          print( "### Error : ProcID %s not in %s " % ( str(procid), genmeta_json ) )
          exit(-1)

      thismeta = genmeta_full[str(procid)]
      params["genmeta"] = thismeta
      datadir = None
      srclfndir = inputlfn
      params["datadir"] = localdir
      params["srclfndir"] = os.path.dirname(inputlfn)
      params["input_file"] = localpath
  
  # if -f is not specified, input file name is decided from genmeta
  else:
      datadir = args.datadir
      print("Deciding input filenames from genmeta... nser="+str(nser))
      if procid not in genmeta_full:
          print( "### Error : ProcID %s not in %s " % ( str(procid), genmeta_json ) )
          exit(-1)
      thismeta = genmeta_full[str(procid)]
      params["genmeta"] = thismeta
  
      if datadir != None:  # when datadir is given
          print("-- datadir is given as "+datadir)
          srclfndir = datadir.replace("%s/prod/ilc/" % localbase, "/ilc/prod/ilc/") # lfn fullpath
          datadir = srclfndir.replace("lfn:/grid/","/").replace("lfn:/","/")          # local fullpath

      else: # if datadir is not given, obtain from thismeta
          proctype = thismeta["process_type"]
	  fileurl = thismeta["fileurl"] if ":" not in thismeta["fileurl"] else thismeta["fileurl"].split(":",1)[1]
	  if fileurl.startswith("/grid/ilc/prod/ilc/"):
               fileurl = fileurl.replace("/grid/ilc/prod/ilc/","/ilc/prod/ilc/") 
          print(" inputlfn is not given and datadir is not given.")
	  print(" fileurl = "+fileurl)
          #if proctype == "500":
	  if fileurl.startswith("/ilc/prod/ilc/"):
               # adatadir = thismeta["fileurl"].replace("lfn:/grid","")
	       adatadir = fileurl
               srclfndir = adatadir if not adatadir.endswith('/') else fileurl[:-1]
               datadir = adatadir.replace("/ilc/prod/ilc/", "%s/prod/ilc/" % localbase )
          elif "/ilc/user/a/amiyamot/mygen" in fileurl:
               datadir = fileurl
               srclfndir = datadir
               datadir = datadir.replace("/ilc/user/a/", "%s/user/a/" % localbase )
          else:
               if "_" in proctype:
                   (genclass, simclass ) = proctype.split('_',1)
               else:
                   genclass = proctype
    
               adatadir = "/ilc/prod/ilc/mc-dbd/generated/%s-%s/%s" % ( thismeta["CM_energy_in_GeV"],
                   thismeta["machine_configuration"], genclass )
               srclfndir = adatadir
               datadir = adatadir.replace("/ilc/prod/ilc/", "%s/prod/ilc/" % localbase )
    
      filenames = GetFilenames( thismeta )
      if verbose:
         print("--- Following files are found from genmeta. First 10 files max. are printed.")
         lfiles = len(filenames) if len(filenames) < 10 else 10
         pprint.pprint(filenames[0:lfiles])

      fuse = ""
      for f in filenames:
         if int(f.split(".")[-2].replace("n","")) == int(nser):
            fuse = f
      if fuse == "":
         print("ERROR For nser="+str(nser)+" no matching input files were found.")
         exit(-1)

      temp = datadir + '/' + str(fuse)

      if verbose:
          print("datadir="+datadir)
          print("fuse="+fuse)
          print("temp="+temp)
          print("srclfndir="+srclfndir)
   
      params["datadir"] = datadir
      params["srclfndir"] = srclfndir
      if temp.startswith("/ilc/prod/ilc/"):
          temp = temp.replace("/ilc/prod/ilc/","/group/ilc/grid/storm/prod/ilc/")
      params["input_file"] = temp.replace("//","/")
  
  # ################## check file status at KEK tape system by ghils command ######
  if "/hsm" in params["input_file"]:
      cmd = "/usr/local/bin/ghils %s" % params["input_file"]
      cmdout = subprocess.check_output(cmd, shell=True, stderr=subprocess.STDOUT)
      print(cmd)
      print(cmdout)

  # pprint.pprint(params)

  #################################################################################
  # ##  CALLs main part of Stdhep2LcioSplit
  #################################################################################
  
  print("")
  print("=========================== Calls Stdhep2LcioSplit main ================================")
  print("input_file="+params["input_file"])
  if not os.path.exists(params["input_file"]):
     print("Stdhep2LcioSplit failed because input file does NOT EXIST.")
     exit(15) 

  ret = Stdhep2LcioSplit( params )

  #################################################################################
  ret["params"] = {}
  for key, value in params.items():
    if key != "genmeta":
      ret["params"][key] = value

  json.dump(ret, open(outjson,"w"))

  if verbose:
     pprint.pprint(ret)


