#####################################
#
# simple script to create lcio files with single particle
# events - modify as needed
# @author F.Gaede, DESY
# @date 1/07/2014
#
# initialize environment:
#  export PYTHONPATH=${LCIO}/src/python:${ROOTSYS}/lib
#
#####################################
import math
import random
from array import array
import os

# --- LCIO dependencies ---
from pyLCIO import UTIL, EVENT, IMPL, IO, IOIMPL

#---- number of events per momentum bin -----
nevt = 100000
# nevt_per_file=10000
nev_per_file_default = 10000
datadir = "single_data_randomP/"
os.mkdir(datadir)

random.seed()


#========== particle properties ===================

nano_meter=1.0e-7
cent_meter=1.0e-2

pdgs = {}
pdgs[13] = {"charge":-1, "mass":0.1056583745, "nev_per_file":10000, "momentum_range":[0.2, 150.0]}
pdgs[11] = {"charge":-1, "mass":0.0005109989461, "nev_per_file":10000, "momentum_range":[0.2, 150.0]}
pdgs[211] = {"charge":1, "mass":0.1056583745, "nev_per_file":10000, "momentum_range":[0.2, 150.0]}
pdgs[321] = {"charge":1, "mass":0.493677, "nev_per_file":10000, "momentum_range":[0.2, 150.0]}
pdgs[2212] = {"charge":1, "mass":0.938272081, "nev_per_file":10000, "momentum_range":[0.2, 150.0]}

genstat  = 1

decayLen_default = 1.e32 
#=================================================

#================================================
both_charge = True
sign_word = "pm"

for pdgkey, pinfo in pdgs.items():

    mass = pinfo["mass"]
    charge = pinfo["charge"]
    pdg = pdgkey

    prange = pinfo["momentum_range"]
    pminlog = math.log10(prange[0])
    pmaxlog = math.log10(prange[1])
 
    nevt_per_file = pinfo["nev_per_file"]
 
    print( "pdg="+str(pdg)+" p from "+str(prange[0])+" to " + str(prange[1])+ " nevt_per_file="+str(nevt_per_file))
    nfile = 0
    if both_charge:
       charge = -charge   # Swap sign first to start from the specified value
       pdg = -pdg

    for j in range(0, nevt):  
#--------------------------------------------
        plog = pminlog + random.random()*( pmaxlog - pminlog )
        p = math.pow(10.0, plog)             

        if both_charge:
            charge = -charge
            pdg = -pdg

        if j%nevt_per_file == 0 : 
            nfile += 1
            if nfile > 1:
                wrt.close() 
  
            # outfile = datadir + "mcparticles_PDG"+str(pdg)+"_MOM%3.3d" % int(p) +"GeV.n%3.3d.slcio" % nfile
            outfile = datadir + "mcparticles_PDG"+sign_word+str(pdg)+"_RandomP" +".n%3.3d.slcio" % nfile
            # WARNING, assumes integer momentum!!!
            wrt = IOIMPL.LCFactory.getInstance().createLCWriter( )
            wrt.open( outfile , EVENT.LCIO.WRITE_NEW ) 
            print( " opened outfile: " , outfile )

# write a RunHeader
            run = IMPL.LCRunHeaderImpl() 
            run.setRunNumber( 0 ) 
            run.parameters().setValue("Generator","${lcgeo}_DIR/examples/lcio_particle_guni_randomP.py")
            run.parameters().setValue("PDG", pdg )
            run.parameters().setValue("Charge", charge )
            run.parameters().setValue("Mass", mass )
            run.parameters().setValue("Momentum_Minimum", prange[0])
            run.parameters().setValue("Momentum_Maximum", prange[1])
            run.parameters().setValue("Momentum_Distribution", "logarithmic")
            run.parameters().setValue("Both_Charge", both_charge)
            wrt.writeRunHeader( run ) 
        
        col = IMPL.LCCollectionVec( EVENT.LCIO.MCPARTICLE ) 
        evt = IMPL.LCEventImpl() 

        evt.setEventNumber( j ) 

        evt.addCollection( col , "MCParticle" )

        phi =  random.random() * math.pi * 2.
        
        theta = math.acos ( 2*random.random() - 1 )

        # print phi, theta
        
        energy   = math.sqrt( mass*mass  + p * p ) 
        
        px = p * math.cos( phi ) * math.sin( theta ) 
        py = p * math.sin( phi ) * math.sin( theta )
        pz = p * math.cos( theta ) 
        
        momentum  = array('f',[ px, py, pz ] )  

        decayLen = decayLen_default
        epx = decayLen * math.cos( phi ) * math.sin( theta ) 
        epy = decayLen * math.sin( phi ) * math.sin( theta )
        epz = decayLen * math.cos( theta ) 

        endpoint = array('d',[ epx, epy, epz ] )  
        

#--------------- create MCParticle -------------------
        
        mcp = IMPL.MCParticleImpl() 

        mcp.setGeneratorStatus( genstat ) 
        mcp.setMass( mass )
        mcp.setPDG( pdg ) 
        mcp.setMomentum( momentum )
        mcp.setCharge( charge ) 

        if( decayLen < 1.e9 ) :   # arbitrary ...
            mcp.setEndpoint( endpoint ) 


#-------------------------------------------------------

      

#-------------------------------------------------------


        col.addElement( mcp )

        wrt.writeEvent( evt ) 


    wrt.close() 


#
#  longer format: - use ".hepevt"
#

#
#    int ISTHEP;   // status code
#    int IDHEP;    // PDG code
#    int JMOHEP1;  // first mother
#    int JMOHEP2;  // last mother
#    int JDAHEP1;  // first daughter
#    int JDAHEP2;  // last daughter
#    double PHEP1; // px in GeV/c
#    double PHEP2; // py in GeV/c
#    double PHEP3; // pz in GeV/c
#    double PHEP4; // energy in GeV
#    double PHEP5; // mass in GeV/c**2
#    double VHEP1; // x vertex position in mm
#    double VHEP2; // y vertex position in mm
#    double VHEP3; // z vertex position in mm
#    double VHEP4; // production time in mm/c
#
#    inputFile >> ISTHEP >> IDHEP 
#    >> JMOHEP1 >> JMOHEP2
#    >> JDAHEP1 >> JDAHEP2
#    >> PHEP1 >> PHEP2 >> PHEP3 
#    >> PHEP4 >> PHEP5
#    >> VHEP1 >> VHEP2 >> VHEP3
#    >> VHEP4;
